using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Collections;

namespace Common
{
    /// <summary>
    /// 数据库备份，还原操作工具类
    /// </summary>
    public class BackUpSystem
    {
        /// <summary>
        ///  因为DataReader独占连接，本程序需要两个Connection,并且连接的数据库是master,所以要初始化新的连接字符串
        /// </summary>
        public static string connectionString = System.Configuration.ConfigurationManager.AppSettings["DBmaster"].ToString();
        public static string DBName = System.Configuration.ConfigurationManager.AppSettings["DBName"].ToString();
        //public static string DBpath = System.Configuration.ConfigurationManager.AppSettings["DBpath"].ToString();
        //public static SqlConnection sc = new SqlConnection(connectionString);

        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static string BackUp(string path,DateTime now)
        {
            string newname = now.ToString("yyyy年MM月dd日HH时mm分ss秒") + ".bak";
            //StringBuilder sb = new StringBuilder();
            //sb.Append(page.Server.MapPath("") + @"\");
            //sb.Append(DBpath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path += "/" + newname;
            string sql = "BACKUP DATABASE @DBName to DISK =@DISK";
            SqlParameter[] sp ={
            new SqlParameter("@DBName", SqlDbType.NVarChar,50),
            new SqlParameter("@DISK", SqlDbType.NVarChar,200)};
            sp[0].Value = DBName;
            sp[1].Value = path;
            ExecuteNonQuery(connectionString, CommandType.Text, sql, sp);
            return newname;
        }

        
        /// <summary>
        /// 根据时间自动备份整个数据库
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        //public static string BackUpByday(string path)
        //{
        //    string date = DbHelperSQL.GetSingle(" select getdate()").ToString();
        //    string newname = Convert.ToDateTime(date).ToString("yyyy年MM月dd日") + ".bak";
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append(path + @"\");
        //    sb.Append(DBpath);
        //    if (!Directory.Exists(sb.ToString()))
        //    {
        //        Directory.CreateDirectory(sb.ToString());
        //    }
        //    sb.Append(@"\");
        //    sb.Append(newname);
        //    if (File.Exists(sb.ToString()))
        //    {
        //        return "";
        //    }
        //    string sql = "BACKUP DATABASE @DBName to DISK =@DISK";
        //    try
        //    {
        //        SqlParameter[] sp ={
        //        new SqlParameter("@DBName", SqlDbType.NVarChar,50),
        //        new SqlParameter("@DISK", SqlDbType.NVarChar,200)};
        //        sp[0].Value = DBName;
        //        sp[1].Value = sb.ToString();
        //        SqlHelper.ExecuteNonQuery(sc, CommandType.Text, sql, sp);
        //        return newname;
        //    }
        //    catch (Exception ex)
        //    {
        //        ArrayList al = new ArrayList();
        //        al.Add("--------------------------数据备份错误---------------------------------");
        //        al.Add(ex.ToString());
        //        //ErrLog.WriteLog(page,al);
        //        return null;
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pathBack">还原文件的路径</param>
        /// <param name="strDBName">数据库名</param>
        /// <returns></returns>
        public static bool SQLBack(string pathBack)
        {
            KillSPID(DBName);
            bool IsBack = false;

            if (!pathBack.EndsWith(".bak"))
            {
                throw new ValidateException("请选择正确的备份设备！");
            }
            string logicalName = pathBack.Substring(pathBack.LastIndexOf('\\') + 1).TrimEnd(new char[] { 'b', 'a', 'k', '.' });
            string cmdText = "use master restore database " + DBName + " from disk = '" + pathBack + "' WITH   REPLACE use " + DBName;
            SqlParameter[] sp = { };
            ExecuteNonQuery(connectionString, CommandType.Text, cmdText, sp);
            IsBack = true;

            return IsBack;
        }


        /// <summary>
        /// 关闭当前连接数据库的连接进程
        /// </summary>
        /// <param name="DBName">要关闭进程的数据库名称</param>
        public static void KillSPID(string DBName)
        {
            string strDBName = DBName;
            string strSQL = String.Empty, strSQLKill = String.Empty;
            try
            {

                //读取连接当前数据库的进程
                strSQL = "select spid from master..sysprocesses where dbid=db_id('" + strDBName + "')";
                SqlParameter[] sp = { };
                using (SqlDataReader mydr = ExecuteReader(connectionString, CommandType.Text, strSQL, sp))
                {
                    //开取杀进程的数据连接
                    while (mydr.Read())
                    {
                        strSQLKill = "kill " + mydr["spid"].ToString();
                        SqlParameter[] sps = { };
                        ExecuteNonQuery(connectionString, CommandType.Text, strSQLKill, sps);
                    }
                    mydr.Close();
                }
            }
            catch (Exception ex)
            {
                ArrayList al = new ArrayList();
                al.Add("--------------------------清除数据库" + DBName + "进程错误错误---------------------------------");
                al.Add(ex.ToString());
                //ErrLog.WriteLog(page, al);
            }
        }

    
        /// <summary>
        /// 删除备份
        /// </summary>
        /// <param name="Xpath"></param>
        /// <returns></returns>
        public static bool DelBackUp(string Xpath)
        {
            File.Delete(Xpath);
            return true;
        }


        private static int ExecuteNonQuery(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {

            SqlCommand cmd = new SqlCommand();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                int val = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                return val;
            }
        }

        /// <summary>
        /// Prepare a command for execution
        /// </summary>
        /// <param name="cmd">SqlCommand object</param>
        /// <param name="conn">SqlConnection object</param>
        /// <param name="trans">SqlTransaction object</param>
        /// <param name="cmdType">Cmd type e.g. stored procedure or text</param>
        /// <param name="cmdText">Command text, e.g. Select * from Products</param>
        /// <param name="cmdParms">SqlParameters to use in the command</param>
        private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            cmd.Connection = conn;
            cmd.CommandText = cmdText;

            if (trans != null)
                cmd.Transaction = trans;

            cmd.CommandType = cmdType;

            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }

        /// <summary>
        /// Execute a SqlCommand that returns a resultset against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  SqlDataReader r = ExecuteReader(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">a valid connection string for a SqlConnection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of SqlParamters used to execute the command</param>
        /// <returns>A SqlDataReader containing the results</returns>
        private static SqlDataReader ExecuteReader(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(connectionString);

            // we use a try/catch here because if the method throws an exception we want to 
            // close the connection throw code, because no datareader will exist, hence the 
            // commandBehaviour.CloseConnection will not work
            try
            {
                PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
                return rdr;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

    }
}
