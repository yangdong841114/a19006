﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 列表响应对象
    /// </summary>
    public class ResponseDtoPage<T> : ResponseData
    {
        public ResponseDtoPage()
        {
            base.status = "success";
        }

        public ResponseDtoPage(string status)
        {
            base.status = status;
        }

        //分页结果对象
        public PageResult<T> result;
    }
}
