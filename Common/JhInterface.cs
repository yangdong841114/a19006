﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Xfrog.Net;

namespace Common
{
    /// <summary>
    /// 聚合数据接口
    /// </summary>
    public static class JhInterface
    {

        public static readonly string appkey = ConfigurationManager.AppSettings["JH_APPKEY"];
        public static readonly string ipappkey = ConfigurationManager.AppSettings["JH_IP_APPID"];
        public static readonly string openId = ConfigurationManager.AppSettings["JH_OPENID"];
        public static readonly string logKey = ConfigurationManager.AppSettings["JH_LOG_APPKEY"];
        public static readonly string SEND_KEY = ConfigurationManager.AppSettings["SEND_KEY"];
        public static readonly string SEND_URL = ConfigurationManager.AppSettings["SEND_URL"];

        /// <summary>
        /// 手机充值
        /// </summary>
        /// <param name="phone">电话</param>
        /// <param name="money">充值金额</param>
        /// <param name="orderId">订单号</param>
        /// <returns></returns>
        public static string Recharge(string phone, string money, string orderId)
        {

            string url7 = "http://op.juhe.cn/ofpay/mobile/onlineorder";

            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("phoneno", phone); //手机号码
            parameters7.Add("cardnum", money); //充值金额,目前可选：5、10、20、30、50、100、300
            parameters7.Add("orderid", orderId); //商家订单号，8-32位字母数字组合
            parameters7.Add("key", appkey);//你申请的key
            string sign = openId + appkey + phone + money + orderId;
            sign = DESEncrypt.md5(sign);
            parameters7.Add("sign", sign); //校验值，md5(OpenID+key+phoneno+cardnum+orderid)，OpenID在个人中心查询

            string result7 = HttpSend.sendPost(url7, parameters7, "get");

            JsonObject newObj7 = new JsonObject(result7);
            String errorCode7 = newObj7["error_code"].Value;

            if (errorCode7 == "0")
            {
                return "success";
            }
            else
            {
                throw new ValidateException(newObj7["error_code"].Value + ":" + newObj7["reason"].Value);
            }
        }
        public static readonly string POC_secretKey = ConfigurationManager.AppSettings["POC_secretKey"];
        public static readonly string POC_signCode = ConfigurationManager.AppSettings["POC_signCode"];
        public static readonly string POC_testUrl = ConfigurationManager.AppSettings["POC_testUrl"];
        public static readonly string POC_Url = ConfigurationManager.AppSettings["POC_Url"];
        public static readonly string istest = ConfigurationManager.AppSettings["POC_IsTest"];
        public static string pocGateway(string orderId, string projectId, string address, string value)
        {
            string url7 = POC_Url;
            string url7test = POC_testUrl;
            string secretKey = POC_secretKey;
            var parameters7 = new Dictionary<string, string>();
            POCJson jpoc = new POCJson();
            string signcode = POC_signCode;
            string data = "{\"orderId\":\"" + orderId + "\",\"projectId\":\"" + projectId + "\",\"address\":\"" + address + "\",\"value\":\"" + value + "\"}";
            jpoc.address = address;
            jpoc.orderId = orderId;
            jpoc.secretKey = secretKey;
            jpoc.value = value;
            string sign = "{\"orderId\":\"" + orderId + "\",\"projectId\":\"" + projectId + "\",\"address\":\"" + address + "\",\"value\":\"" + value + "\"}" + signcode;
            parameters7.Add("data", data);
            string temp = sign;
            sign = DESEncrypt.md5(sign);
            parameters7.Add("sign", sign); //校验值
            string result7 = "";
            if (istest != "test")
                result7 = HttpSend.sendPost(url7, parameters7, "post");
            else
                result7 = HttpSend.sendPost(url7test, parameters7, "post");
            string request = "请求数据：" + " orderId=" + orderId + " address=" + address + "value=" + value + "secretKey=" + secretKey + "sign=" + sign;
            if (result7.IndexOf("\"code\":0") != -1 || result7.IndexOf("\"code\":5") != -1)
            {
                return request + " success" + ":" + result7;
            }
            else
            {
                return request + " lose" + ":" + result7;
            }
        }

        public static string pocGateway(string orderID, string xmbh, string sign, string address, string value, string dxtype)
        {
            string url7 = SEND_URL;
            string secretKey_A19006 = SEND_KEY;
            string veryfy_A19006 = DESEncrypt.md5("poc" + orderID.ToString() + secretKey_A19006);
            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("orderID", orderID);
            parameters7.Add("xmbh", xmbh);
            parameters7.Add("address", address);
            parameters7.Add("value", value);
            parameters7.Add("dxtype", dxtype);
            parameters7.Add("sign", veryfy_A19006); //校验值
            string result7 = "";
            result7 = HttpSend.sendPost(url7, parameters7, "post");
            return result7;
        }

        /// <summary>
        /// 获取物流商信息
        /// </summary>
        /// <returns></returns>
        public static List<Detail> GetLogisticList()
        {
            string url7 = "http://v.juhe.cn/exp/com";

            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("key", logKey);//你申请的key

            string result7 = HttpSend.sendPost(url7, parameters7, "get");

            LogResult m = Newtonsoft.Json.JsonConvert.DeserializeObject<LogResult>(result7);

            if (m.error_code == "0")
            {
                return m.result;
            }
            else
            {
                throw new ValidateException(m.reason);
            }
        }

        /// <summary>
        /// 查询物流信息
        /// </summary>
        /// <param name="wlno">物流商编码</param>
        /// <param name="logNo">物流单号</param>
        /// <returns></returns>
        public static LogisticResult GetLogisticMsg(string wlno, string logNo)
        {
            string url7 = "http://v.juhe.cn/exp/index";

            var parameters7 = new Dictionary<string, string>();
            parameters7.Add("key", logKey);//你申请的key
            parameters7.Add("com", wlno);
            parameters7.Add("no", logNo);
            parameters7.Add("dtype", "json");

            string result7 = HttpSend.sendPost(url7, parameters7, "get");

            LogisticJson m = Newtonsoft.Json.JsonConvert.DeserializeObject<LogisticJson>(result7);

            if (m.error_code == "0")
            {
                return m.result;
            }
            else
            {
                throw new ValidateException(m.reason);
            }
        }
    }

    public partial class Detail
    {
        public string no { get; set; }

        public string com { get; set; }
    }


    public partial class LogResult
    {
        public string error_code { get; set; }

        public string resultcode { get; set; }

        public string reason { get; set; }

        public List<Detail> result { get; set; }
    }
    class POCJson
    {
        public string orderId { get; set; }
        public string address { get; set; }
        public string secretKey { get; set; }
        public string value { get; set; }
    }
}
