﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 查询参数类
    /// </summary>
    public class DbParameterItem
    {
        //参数化名称，如： where id = @id,此处保存@后面点名字 id
        public string Name { get; set; }

        //参数化值
        public object Value { get; set; }

        //查询条件操作符：ConstUtil.EQ ...等，引用ConstUtil中常量
        public string op { get; set; }

        //数据类型
        public DbType dtype { get; set; }


        //public System.Type SysType { get; set; }

        public DbParameterItem() { }


        public DbParameterItem(string name, string op, object value)
        {
            this.Name = name;
            this.op = op;
            this.Value = value;
        }

        public DbParameterItem(string name, DbType tp, object value)
        {
            this.Name = name;
            this.dtype = tp;
            this.Value = value;
        }
    }
}
