﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 响应对象，默认为SUCCESS；
    /// </summary>
    public class ResponseData
    {
        public ResponseData()
        {
            this.status = "success";
        }

        public ResponseData(string status)
        {
            this.status = status;
        }

        //描述信息
        public string msg { get; set; }
        //其他信息
        public string other { get; set; }

        //状态，success代表此次请求成功，fail代表此次请求失败
        public string status { get; set; }

        /// <summary>
        /// 状态改为成功
        /// </summary>
        public void Success()
        {
            this.status = "success";
        }

        /// <summary>
        /// 状态改为失败
        /// </summary>
        public void Fail()
        {
            this.status = "fail";
        }
    }
}
