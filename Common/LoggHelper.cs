﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{

    public static class LoggHelper
    {
        public static ILog logError = LogManager.GetLogger("ErrorLog");
        public static ILog logInfor = LogManager.GetLogger("InforLog");

        /// <summary>
        /// 记录错误日志
        /// </summary>
        public static void WriteLog(string infor, Exception ex)
        {
            logError.Error(infor, ex);
        }
        /// <summary>
        /// 记录普通日志
        /// </summary>
        public static void WriteLog(string infor)
        {
            logInfor.Info(infor);
        }
    }
}
