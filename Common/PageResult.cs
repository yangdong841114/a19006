﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 分页结果类,对用前端easyui中 datagrid 插件
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageResult<T>
    {
        public int total = 0;       //总条数

        public List<T> rows;        //查询结果

        public List<T> footer;      //表格底部行（一般为合计）
    }
}
