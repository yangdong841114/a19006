﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html lang="zh">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><%:ViewData["sitename"]%></title>
    <link href="/Content/css/zui2.css" rel="stylesheet" type="text/css" />
    <link href="/Content/css/style.css" rel="stylesheet" type="text/css" />
    <script src="/Content/js/jquery-3.2.1.min.js" id="main"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
    <script type="text/javascript" src="/Content/js/jquery.flexslider-min.js"></script>
</head>
<body>
    <div class="header">
        <div class="pull-left">
            <img class="lf" src="/Content/images/logo.png" />
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="mainnav">
    </div>
    <div class="">
        <div class="center-block tipsbox">

            <div class="tipscontbox" style="height:280px;">
                <div class="tipsiconbox">
                    <i class="icon icon-user"></i>
                </div>
                <form class="form-horizontal" id="cgForm">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon" >登录密码</span>
                            <input type="password" class="form-control" id="pass1" placeholder="请录入新的登录密码">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon" >安全密码</span>
                            <input type="password" class="form-control" id="pass2" placeholder="请录入新的安全密码">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon" >交易密码</span>
                            <input type="password" class="form-control" id="pass3" placeholder="请录入新的交易密码">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12" >
                        <button type="button" id="zfBtn" class="btn btn-warning ">提交修改</button>
                </div>
                 </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="ifooter">
        <div class="">
            <p><%:ViewData["copyright"]%></p>

            <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $(".form-control").each(function () {
                var d = $(this);
                d.on("focus", function () {
                    d.removeClass("inputError");
                    if (d.popover) {
                        d.popover("destroy");
                    }
                })
            });
            

            //回车键绑定
            document.onkeydown = function (event) {
                var e = event || window.event || arguments.callee.caller.arguments[0];
                if (e && e.keyCode == 13) {
                    $('#zfBtn').trigger("click");
                }
            };

            $("#zfBtn").on("click", function () {
                var checked = true;
                if ($("#pass1").val() == 0) {
                    checked = false;
                    $("#pass1").addClass("inputError");
                }
                if ($("#pass2").val() == 0) {
                    checked = false;
                    $("#pass2").addClass("inputError");
                }
                if ($("#pass3").val() == 0) {
                    checked = false;
                    $("#pass3").addClass("inputError");
                }

                if (checked) {
                    var data = { pass1: $("#pass1").val(), pass2: $("#pass2").val(), pass3: $("#pass3").val() };
                    $.ajax({
                        url: "/ForgetPass/ChangePss",
                        type: "POST",
                        data: data,
                        success: function (result) {
                            if (result.status == "fail") {
                                if (result.msg == "jump") {
                                    location.href = "/ForgetPass/Index";
                                } else {
                                    var msgbox = new $.zui.Messager('提示消息：' + result.msg, {
                                        type: 'danger',
                                        icon: 'warning-sign',
                                        placement: 'center',
                                        parent: 'body',
                                        close: true
                                    });
                                    msgbox.show();
                                }
                            } else {
                                alert("重置密码成功,请重新登录");
                                location.href = "/Home/Index";
                            }
                        }
                    });
                }
            })
        });
    </script>
</body>
</html>
