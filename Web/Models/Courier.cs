﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Courier
    {
        public string com { get; set; }
        public string no { get; set; }
    }
    public class CourierInfo
    {
        public string company { get; set; }
        public string com { get; set; }
        public string no { get; set; }
        public string status { get; set; }
        public List<CourierInfoDetailed> list { get; set; }
        
    }
    public class CourierInfoDetailed
    {
        public string datetime { get; set; }
        public string remark { get; set; }
        public string zone { get; set; }
    }
}