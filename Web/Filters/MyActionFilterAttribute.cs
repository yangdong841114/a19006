﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Filters
{
    public class MyActionFilterAttribute : ActionFilterAttribute
    {

        private const string HAS_ADMIN_LOGIN = "HAS_ADMIN_LOGIN";               //需要后台登录才能访问
        private const string HAS_APP_ADMIN_LOGIN = "HAS_APP_ADMIN_LOGIN";       //需要手机端后台登录才能访问
        private const string HAS_USER_LOGIN = "HAS_USER_LOGIN";                 //需要前台登录才能访问
        private const string HAS_APP_USER_LOGIN = "HAS_APP_USER_LOGIN";         //需要手机端前台登录才能访问
        private const string HAS_ADMIN_PERMISSION = "HAS_ADMIN_PERMISSION";     //需要后台登录且有后台对应权限才能访问
        private const string HAS_USER_PERMISSION = "HAS_USER_PERMISSION";       //需要前台登录且有前台对应权限才能访问

        /// <summary>  
        /// 在Action方法之前 调用  
        /// </summary>  
        /// <param name="filterContext"></param>  
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string strController = filterContext.RouteData.Values["controller"].ToString();
            string strAction = filterContext.RouteData.Values["action"].ToString();
            string url = filterContext.HttpContext.Request.RawUrl;
            bool isJsFile = url.EndsWith(".js"); //是否js文件

            string result = null;

            //通过设置JS_Permission参数，在StaticFileController内决定返回的JS
            filterContext.HttpContext.Session["JS_Permission"] = null;

            string pms = null; //是否需要校验
            if (isJsFile)
            {
                pms = IsFilterJsUrl(url); //JS文件是否需要权限
            }
            else
            {
                pms = IsFilterControllerUrl(url); //Controller是否需要权限
            }

            //1.HAS_ADMIN_LOGIN 需要后台登录
            //2.HAS_USER_LOGIN 需要前台台登录
            //3.HAS_ADMIN_PERMISSION  需要后台登录且有后台对应权限才能访问
            //4.HAS_USER_PERMISSION  需要前台登录且有前台对应权限才能访问
            if (pms != null)
            {
                object login = null;

                //需要后台已登录
                if (pms == HAS_ADMIN_LOGIN)
                {
                    login = filterContext.HttpContext.Session["LoginUser"];
                    if (login == null) { result = "NotAdminLogin"; }
                }
                //需要前台已登录
                else if (pms == HAS_USER_LOGIN)
                {
                    login = filterContext.HttpContext.Session["MemberUser"];
                    if (login == null) { result = "NotUserLogin"; }
                }
                //需要后台权限
                else if (pms == HAS_ADMIN_PERMISSION)
                {
                    Member m = (Member)filterContext.HttpContext.Session["LoginUser"]; //后台登录用户
                    //首先需保持后台登录有效
                    if (m == null) { result = "NotAdminLogin"; }
                    else
                    {
                        //判断是JS文件请求，还是Controller请求
                        if (isJsFile)
                        {
                            //截取js对应的权限路径
                            string jsPath = url.Substring(1, url.LastIndexOf(".") - 1);
                            //检查是否拥有js权限
                            if (m.permission == null || !m.permission.ContainsKey(jsPath))
                            {
                                //通过设置JS_Permission参数，在StaticFileController内决定返回的JS
                                filterContext.HttpContext.Session["JS_Permission"] = "NotPermission";
                            }
                        }
                        else
                        {
                            string controllerPath = "Admin/" + strController; //Controller权限路径
                            //检查是否拥有Controller权限
                            if (m.permission == null || !m.permission.ContainsKey(controllerPath)) { result = "NotPermission"; }
                        }
                    }
                }
                //需要前台权限
                else if (pms == HAS_USER_PERMISSION)
                {
                    Member m = (Member)filterContext.HttpContext.Session["MemberUser"]; //前台登录用户
                    //首先需保持前台登录有效
                    if (m == null) { result = "NotUserLogin"; }
                    else
                    {
                        //进行权限验证
                        //判断是JS文件请求，还是Controller请求
                        if (isJsFile)
                        {
                            //截取js对应的权限路径
                            string jsPath = url.Substring(1, url.LastIndexOf(".") - 1);
                            //检查是否拥有js权限
                            if (m.permission == null || !m.permission.ContainsKey(jsPath))
                            {
                                //通过设置JS_Permission参数，在StaticFileController内决定返回的JS
                                filterContext.HttpContext.Session["JS_Permission"] = "NotPermission";
                            }
                        }
                        else
                        {
                            string controllerPath = "User/" + strController; //Controller权限路径
                            //检查是否拥有Controller权限
                            if (m.permission == null || !m.permission.ContainsKey(controllerPath)) { result = "NotPermission"; }
                        }
                    }
                }
            }
            //result有值代表校验不通过
            if (result != null)
            {
                filterContext.HttpContext.Response.Write(result);
                filterContext.HttpContext.Response.End();
            }
            else
            {
                base.OnActionExecuting(filterContext);
            }
        }

        /// <summary>  
        /// 在  Action方法之后 调用  
        /// </summary>  
        /// <param name="filterContext"></param>  
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }


        //判断Controller请求是否需要登录、权限过滤
        private string IsFilterControllerUrl(string url)
        {
            if (url.StartsWith("/User/UserWeb")) { return HAS_USER_LOGIN; }
            else if (url.StartsWith("/Admin/ManageWeb") || url.StartsWith("/Admin/Ueditor")) { return HAS_ADMIN_LOGIN; }
            else if (url.StartsWith("/Admin/")) { return HAS_ADMIN_PERMISSION; }
            else if (url.StartsWith("/User/")) { return HAS_USER_PERMISSION; }
            return null;
        }

        //判断js文件是否需要登录、权限过滤、密码校验（交易密码、安全密码）
        private string IsFilterJsUrl(string url)
        {
            if (url.StartsWith("/User/"))
            {
                //前台以下js访问只需要登录
                if (url.EndsWith("router.js") || url.EndsWith("main.js") || url.EndsWith("index.js") || url.EndsWith("NoPermission.js") || url.EndsWith("NotFound.js")
                    || url.EndsWith("Password2.js") || url.EndsWith("Password3.js"))
                {
                    return HAS_USER_LOGIN;
                    //前台以下js访问需要权限
                }
                else
                {
                    return HAS_USER_PERMISSION;
                }

            }
            else if (url.StartsWith("/Admin/"))
            {
                //后台以下js访问只需要登录
                if (url.EndsWith("router.js") || url.EndsWith("index.js") || url.EndsWith("NoPermission.js") || url.EndsWith("NotFound.js")
                    || url.EndsWith("Password2.js") || url.EndsWith("Password3.js"))
                {
                    return HAS_ADMIN_LOGIN;
                    //后台以下js访问需要权限
                }
                else
                {
                    return HAS_ADMIN_PERMISSION;
                }
            }
            return null;
        }
    }
}