
define(['text!errorCurrencyDetails.html', 'jquery', 'j_easyui', 'datetimepicker'], function (errorCurrency, $) {

    var controller = function (userId) {
        //设置标题
        $("#center").panel("setTitle", "详细数据");
        appView.html(errorCurrency);
        
        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: 'POC地址', width: '150' }]],
            columns: [[

                { field: 'epointsSf', title: '释放数量', width: '150' },
                { field: 'epointsFh', title: '分红数量', width: '150' },

                { field: 'sf', title: '实发', width: '100' },
                { field: 'jstime', title: '结算日期', width: '130' },
                { field: 'ff', title: '发放状态', width: '100' },
                { field: 'mulx', title: '业务摘要', width: '250' },
                { field: 'apiLog', title: 'API日志', width: '850' },
            ]],
            url: "Charge/GetListPageErrorCurrencyDetails?userId=" + userId + ""
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["jstime"] = utils.changeDateFormat(data.rows[i]["jstime"]);
                    data.rows[i]["ff"] = data.rows[i]["ff"] == 1 ? "已发" : "未发";
                    data.rows[i]["yf"] = data.rows[i]["yf"].toFixed(2);
                    data.rows[i]["epointsSf"] = data.rows[i]["epointsSf"].toFixed(2);
                    data.rows[i]["epointsFh"] = data.rows[i]["epointsFh"].toFixed(2);
                    data.rows[i]["fee3"] = data.rows[i]["fee3"].toFixed(2);
                    data.rows[i]["sf"] = data.rows[i]["sf"].toFixed(2);
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }


        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});