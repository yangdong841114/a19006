
define(['text!CurrencyUserDetail.html', 'jquery', 'j_easyui', 'datetimepicker'], function (CurrencyUserDetail, $) {

    var controller = function (agm) {
        //设置标题
        $("#center").panel("setTitle", "会员收益明细");
        if (agm) {
            appView.html(CurrencyUserDetail);

            var dt = agm.substring(0, 10);
            var uid = agm.substring(10, agm.length);
          

            //初始select
            var cList = cacheList["rLevel"];
            if (cList && cList.length > 0) {
                $("#LockType").empty();
                $("#LockType").append("<option value='0'>--全部档次--</option>");
                for (var i = 0; i < cList.length; i++) {
                    $("#LockType").append("<option value='" + cList[i].id + "'>" + cList[i].name + "</option>");
                }
            }

            //初始化日期选择框
            $(".form-date").datetimepicker(
            {
                language: "zh-CN",
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

            //初始化表格
            var grid = utils.newGrid("dg", {
                frozenColumns: [[{ field: 'userId', title: 'POC地址', width: '150' }]],
                columns: [[
                  
                    { field: 'epointsSf', title: '释放数量', width: '150' },
                    { field: 'epointsFh', title: '分红数量', width: '150' },
                  
                    { field: 'sf', title: '实发', width: '100' },
                    { field: 'jstime', title: '结算日期', width: '130' },
                    { field: 'ff', title: '发放状态', width: '100' },
                    { field: 'mulx', title: '业务摘要', width: '250' },
                    { field: 'apiLog', title: 'API日志', width: '850' },
                ]],
                url: "Currency/GetDetailListPage?addDate=" + dt + "&uid=" + uid
            }, null, function (data) {
                if (data && data.rows) {
                    for (var i = 0; i < data.rows.length; i++) {
                        data.rows[i]["jstime"] = utils.changeDateFormat(data.rows[i]["jstime"]);
                        data.rows[i]["ff"] = data.rows[i]["ff"] == 1 ? "已发" : "未发";
                        data.rows[i]["yf"] = data.rows[i]["yf"].toFixed(2);
                        data.rows[i]["epointsSf"] = data.rows[i]["epointsSf"].toFixed(2);
                        data.rows[i]["epointsFh"] = data.rows[i]["epointsFh"].toFixed(2);
                        data.rows[i]["fee3"] = data.rows[i]["fee3"].toFixed(2);
                        data.rows[i]["sf"] = data.rows[i]["sf"].toFixed(2);
                    }
                }
                return data;
            })

            //查询grid
            queryGrid = function () {
                var objs = $("#QueryForm").serializeObject();
                grid.datagrid("options").queryParams = objs;
                grid.datagrid("reload");
            }

            //查询按钮
            $("#submit").on("click", function () {
                queryGrid();
            })
        }

      


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});