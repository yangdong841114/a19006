
define(['text!CurrencySum.html', 'jquery', 'j_easyui', 'datetimepicker'], function (errorCurrency, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "下发");
        appView.html(errorCurrency);
        details = function (userId) {
            alert(userId);
        }
        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'addTime', title: '生成日期', width: '200' }]],
            showFooter: true,
            columns: [[
                { field: 'sumPoc', title: '下发POC总数', width: '200' },
                { field: 'isStatus', title: '下发状态', width: '100' },
                {
                    field: '下发', title: '下发', width: '200', align: 'center', formatter: function (val, row, index) {
                        return row.isStatus == "未下发" ? "<a id='xf_" + index + "' onclick='pocxf(" + index + ");' dataId='" + row.addTime + "' class='gridFildEdit' >下发</a>&nbsp;&nbsp;" : "";
                    }
                },
                {
                    field: '下发明细', title: '下发明细', width: '200', align: 'center', formatter: function (val, row, index) {
                        return "<a href='#CurrencySumDetails/" + row.addTime + "' dataId='" + row.addTime + "' class='gridFildEdit' >下发明细</a>&nbsp;&nbsp;";
                    }
                }
            ]],
            url: "Charge/GetListPageCurrencySum"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]).substr(0, 10);
                    data.rows[i]["isStatus"] = data.rows[i]["isStatus"] == "0" ? "未下发" : "已下发";
                }

            }
            return data;
        }, function () {

            ////行确认按钮
            //$(".gridFildEdit").each(function (i, dom) {
            //    dom.onclick = function () {
            //        var dataId = $(dom).attr("dataId");
            //        window.open('https://etherscan.io/tx/' + dataId);
            //    }
            //});


        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        pocxf = function (index) {
            var addTime = $("#xf_" + index).attr("dataId");
            var data = { "addTime": addTime };
            utils.AjaxPost("/Home/AutoCurrencyToPoc", data, function (result) { grid.datagrid("reload"); });
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});