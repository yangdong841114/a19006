
define(['text!errorCurrency.html', 'jquery', 'j_easyui', 'datetimepicker'], function (errorCurrency, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "财务审计");
        appView.html(errorCurrency);
        details = function (userId) {
            alert(userId);
        }
        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: 'userId', width: '300' }]],
            showFooter: true,
            columns: [[
                { field: 'pocAddress', title: '钱包地址', width: '300' },
                { field: 'sumSc', title: '锁仓总数', width: '100' },
                { field: 'sumSf', title: '释放总数', width: '100' },
                { field: 'sumFh', title: '分红总数', width: '100' },
                { field: 'maxFh', title: '分红上限', width: '100' },
                { field: 'addTime', title: '审计时间', width: '130' },
                { field: 'remarks', title: '业务摘要', width: '200' },
                {
                    field: '查看详情', title: '查看详情', width: '200', align: 'center', formatter: function (val, row, index) {
                        return "<a href='#errorCurrencyDetails/" + row.userId + "' dataId='" + row.userId + "' class='gridFildEdit' >查看详情</a>&nbsp;&nbsp;";
                    }
                }
            ]],
            url: "Charge/GetListPageErrorCurrency"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }

            }
            return data;
        }, function () {

            ////行确认按钮
            //$(".gridFildEdit").each(function (i, dom) {
            //    dom.onclick = function () {
            //        var dataId = $(dom).attr("dataId");
            //        window.open('https://etherscan.io/tx/' + dataId);
            //    }
            //});


        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }


        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});