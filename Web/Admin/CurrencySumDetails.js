
define(['text!CurrencySumDetails.html', 'jquery', 'j_easyui', 'datetimepicker'], function (errorCurrency, $) {

    var controller = function (addTime) {
        //设置标题
        $("#center").panel("setTitle", "详细数据");
        appView.html(errorCurrency);

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'id', title: 'id', width: '50' }]],
            columns: [[
                { field: 'userId', title: 'POC地址', width: '300' },
                { field: 'addTime', title: '生成时间', width: '100' },
                { field: 'sumPoc', title: '发放POC总数', width: '100' },
                { field: 'isStatus', title: '发放状态', width: '100' },
                { field: 'logs', title: 'api日志', width: '500' },
                {
                    field: '审核发放', title: '审核发放', width: '200', align: 'center', formatter: function (val, row, index) {
                        return row.isAdmin == 1 ? "<a id='xf_" + index + "' onclick='pocxf(" + index + ");' dataValue='" + row.id + "' dataId='" + row.addTime + "' class='gridFildEdit' >审核发放</a>&nbsp;&nbsp;" : "";
                    }
                }
            ]],
            url: "Charge/GetListPageCurrencySumDetails?addTime=" + addTime + ""
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]).substr(0, 10);
                    data.rows[i]["isStatus"] = data.rows[i]["isStatus"] == 1 ? "已发" : data.rows[i]["isStatus"] == 2 ? "已结算" : "未发";

                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }
        pocxf = function (index) {
            var addTime = $("#xf_" + index).attr("dataId");
            var id = $("#xf_" + index).attr("dataValue");
            var data = { "addTime": addTime, "id": id };
            utils.AjaxPost("/Home/AutoCurrencyToUserPoc", data, function (result) { grid.datagrid("reload"); });
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});