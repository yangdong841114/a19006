﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using Web.Filters;

namespace Web.Common.Controllers
{
    public class CommonController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public IResourceBLL resourceBLL { get; set; }

        public IMemberAccountBLL accBLL { get; set; }

        public IBaseSetBLL setBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public IMobileNoticeBLL mobileNoticeBLL { get; set; }

        /// <summary>
        /// 后台删除消息提醒记录
        /// </summary>
        /// <param name="url"></param>
        /// <param name="toUid"></param>
        /// <returns></returns>
        public ActionResult DeleteMsg(string url)
        {
            msgBLL.Delete(url, 0);
            return null;
        }

        /// <summary>
        /// 前台删除消息提醒记录
        /// </summary>
        /// <param name="url"></param>
        /// <param name="toUid"></param>
        /// <returns></returns>
        public ActionResult DeleteBMsg(string url)
        {
            Member m = (Member)Session["MemberUser"];
            msgBLL.Delete(url, m.id.Value);
            return null;
        }

        /// <summary>
        /// 获取站点信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSetMsg()
        {
            ResponseDtoData response = new ResponseDtoData(); //响应对象
            BaseSet set = setBLL.GetModel();
            BaseSet rt = new BaseSet();
            rt.sitename = set.sitename;
            rt.copyright = set.copyright;
            rt.WebCode = set.WebCode;
            response.result = rt;
            response.Success();
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// List权限列表转Map
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetPermissionMap(List<Resource> list)
        {
            Dictionary<string, string> di = new Dictionary<string, string>();
            if (list != null && list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    Resource r = list[i];
                    if (!ValidateUtils.CheckNull(r.curl))
                    {
                        if (!di.ContainsKey(r.curl))
                        {
                            di.Add(r.curl, r.id);
                        }
                    }
                }
            }
            return di;
        }

        /// <summary>
        /// PC后台登录
        /// 1.检查用户、密码、验证码
        /// 2.获取用户权限
        /// </summary>
        /// <param name="mb">会员信息：帐号、密码</param>
        /// <param name="yCode">验证码</param>
        /// <returns>登录信息</returns>
        public JsonResult ManageLogin(Member mb, string yCode)
        {
            ResponseDtoData response = new ResponseDtoData(); //响应对象
            ////获取session中的验证码
            //string sessionCode = Session["ManageLoginCode"] == null ? "-1" : Session["ManageLoginCode"].ToString();
            ////每个验证码只使用一次
            //Session["ManageLoginCode"] = null;
            ////匹配验证码
            //if (yCode.ToUpper() == sessionCode.ToUpper())
            //{
            try
            {
                string ip = GetIP(); //获取IP
                mb.ipAddress = ip;
                //ip登陆限制
                Member m = memberBLL.GetModelByUserId(mb.userId);
                if (m != null)
                {
                    var blackList = System.Web.Configuration.WebConfigurationManager.AppSettings["Blacklist"].ToString();
                    var arrBlackList = blackList.Split(',');
                    if (Array.IndexOf(arrBlackList, ip) == -1)
                    {
                        var msgResult = mobileNoticeBLL.SendMessage(m.phone, string.Format("异常IP：{0}正试图用账号：{1}入侵A19006后台，请及时上线处置", ip, mb.userId));
                        throw new ValidateException("登陆IP受限");
                    }
                    //List<string> loginField = Session["loginField"] == null ? new List<string>() : (List<string>)Session["loginField"];
                    //if (m.password != DESEncrypt.EncryptDES(mb.password, ConstUtil.SALT)) /* && m.userId.ToLower() == "admin"*/
                    //{
                    //    loginField.Add(m.userId.ToString());
                    //    Session["loginField"] = loginField;
                    //}
                    //var userLoginFieldCounts = loginField == null ? 0 : loginField.Where(p => p == m.userId).Count();
                    //if (userLoginFieldCounts >= 3)
                    //{
                    //    var msgResult = mobileNoticeBLL.SendMessage(m.phone, string.Format("IP：{0}用账号：ADMIN密码连续输错3次，有暴力破解A19006风险，请及时上线处置", ip));
                    //    throw new ValidateException("密码错误次数过多");
                    //}
                }

                ResponseDtoData rdd = memberBLL.loginUser(mb, 1);
                if (rdd.status == "fail")
                {
                    throw new ValidateException(rdd.msg);
                }
                else
                {
                    Member mm = (Member)rdd.result;
                    mm.ipAddress = ip;
                    //获取用户后台菜单权限
                    List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, "100");
                    Dictionary<string, string> di = GetPermissionMap(list);
                    mm.permission = di;
                    Session["LoginUser"] = mm;
                    Session.Timeout = 5;//设置过期时间五分钟
                }
            }
            catch (ValidateException ve)
            {
                response.status = "fail";
                response.msg = ve.Message;
            }
            catch (Exception ex)
            {
                response.status = "fail";
                response.msg = "登录失败，请联系管理员";
            }
            //}
            //else
            //{
            //    response.status = "fail";
            //    response.msg = "验证码错误";
            //}

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取客户端IP地址
        /// </summary>
        /// <returns>若失败则返回回送地址</returns>
        private string GetIP()
        {
            //如果客户端使用了代理服务器，则利用HTTP_X_FORWARDED_FOR找到客户端IP地址
            string userHostAddress = null;
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                userHostAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString().Split(',')[0].Trim();
            }
            //否则直接读取REMOTE_ADDR获取客户端IP地址
            if (string.IsNullOrEmpty(userHostAddress))
            {
                userHostAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            //前两者均失败，则利用Request.UserHostAddress属性获取IP地址，但此时无法确定该IP是客户端IP还是代理IP
            if (string.IsNullOrEmpty(userHostAddress))
            {
                userHostAddress = Request.UserHostAddress;
            }
            //最后判断获取是否成功，并检查IP地址的格式（检查其格式非常重要）
            if (!string.IsNullOrEmpty(userHostAddress) && IsIP(userHostAddress))
            {
                return userHostAddress;
            }
            return "127.0.0.1";
        }

        /// <summary>
        /// 检查IP地址格式
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(string ip)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }
    }
}
