﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using WechatAPI.Api;
using WechatAPI.Mod;
using WechatAPI.Lib;



namespace Web.Common.Controllers
{
    /// <summary>
    /// 支付回调Controller
    /// </summary>
    public class PayBackController : Controller
    {
        public IChargeBLL chargeBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 微信支付回调
        /// </summary>
        /// <returns></returns>
        public ActionResult WxBack()
        {
            ResultNotify result = new ResultNotify(Request,Response);
            WxPayData notifyData = result.GetNotifyData();

            //检查支付结果中transaction_id是否存在
            if (!notifyData.IsSet("transaction_id"))
            {
                //若transaction_id不存在，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "支付结果中微信订单号不存在");
                Log.Error(this.GetType().ToString(), "The Pay result is error : " + res.ToXml());
                Response.Write(res.ToXml());
                Response.End();
            }

            string transaction_id = notifyData.GetValue("transaction_id").ToString();

            //查询订单，判断订单真实性
            if (!result.QueryOrder(transaction_id))
            {
                //若订单查询失败，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "订单查询失败");
                Log.Error(this.GetType().ToString(), "Order query failure : " + res.ToXml());
                Response.Write(res.ToXml());
                Response.End();
            }
            //查询订单成功
            else
            {
                WxPayData res = new WxPayData();
                string out_trade_no = notifyData.GetValue("out_trade_no").ToString();  //商户订单号
                string attach = notifyData.GetValue("attach").ToString();   //附加数据商家数据包

                if (string.IsNullOrEmpty(attach))
                {
                    res.SetValue("return_code", "FAIL");
                    res.SetValue("return_msg", "订单更新失败");
                    Log.Error(this.GetType().ToString(), "Order query failure : " + res.ToXml());
                    Response.Write(res.ToXml());
                    Response.End();
                }
                else
                {
                    //是否已经写入成功
                    List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("bankCard", ConstUtil.EQ, out_trade_no)).Result();
                    int count = chargeBLL.GetCount("select count(1) from Charge where typeId = " + ConstUtil.CHARGE_WX, param);
                    if (count == 0)
                    {

                        Member current = memberBLL.GetModelById(int.Parse(attach.Split('|')[0]));  //实体

                        //保存充值记录
                        DateTime now = DateTime.Now;
                        Charge model = new Charge();
                        model.uid = current.id.Value;
                        model.userId = current.userId;
                        model.epoints = double.Parse(attach.Split('|')[1]);
                        model.ispay = 2;
                        model.passTime = now;
                        model.addTime = now;
                        model.typeId = ConstUtil.CHARGE_WX;
                        model.bankTime = now.ToString("yyyy-MM-dd HH:mm:ss");
                        model.toBank = "微信";
                        model.fromBank = "微信";
                        model.bankUser = "";
                        model.bankCard = out_trade_no;  //商户订单号
                        chargeBLL.SavePayCharge(model, current);

                        //消息提醒
                        SystemMsg msg = new SystemMsg();
                        msg.isRead = 0;
                        msg.toUid = 0;
                        msg.url = "#Charge";
                        msg.msg = "您有新的充值申请";
                        msgBLL.Save(msg);

                        res.SetValue("return_code", "SUCCESS");
                        res.SetValue("return_msg", "OK");
                        Log.Info(this.GetType().ToString(), "order query success : " + res.ToXml());
                        Response.Write(res.ToXml());
                        Response.End();
                    }
                    else
                    {
                        res.SetValue("return_code", "FAIL");
                        res.SetValue("return_msg", "订单更新失败");
                        Log.Error(this.GetType().ToString(), "Order query failure : " + res.ToXml());
                        Response.Write(res.ToXml());
                        Response.End();
                    }
                }
            }

            return null;
        }
    }
}
