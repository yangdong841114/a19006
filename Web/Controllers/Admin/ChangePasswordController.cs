﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 修改密码Controller
    /// </summary>
    public class ChangePasswordController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="oldPass">原密码</param>
        /// <param name="newPass"新密码></param>
        /// <param name="flag">密码类型：1：登录密码，2：安全密码，3：交易密码</param>
        /// <returns></returns>
        public JsonResult ChangePassword(string oldPass,string newPass,int flag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                int c = memberBLL.UpdatePassword(mb.id.Value, oldPass, newPass, flag);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作出错，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
