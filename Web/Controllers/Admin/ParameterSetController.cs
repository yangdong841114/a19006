﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using Web.Models;

namespace Web.Admin.Controllers
{
    public class ParameterSetController : Controller
    {
        public IParamSetBLL paramBLL { get; set; }

        public IMobileNoticeBLL mobileNoticeBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 初始化页面，读取已有参数
        /// </summary>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoMap<string, ParameterSet> response = new ResponseDtoMap<string, ParameterSet>();
            response.map = paramBLL.GetAllToDictionary();
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="list">参数列表</param>
        /// <returns></returns>
        public JsonResult Save(List<ParameterSet> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                paramBLL.UpdateList(list);
                response.status = "success";
                Member m = (Member)Session["LoginUser"];
                var msgResult = mobileNoticeBLL.SendMessage(m.phone, string.Format("您正在修改A19006参数，如不是本人操作，请及时上线处置"));
            }
            catch (ValidateException ex){response.msg = ex.Message;}
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
