﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    public class ManageWebController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public IResourceBLL resourceBLL { get; set; }

        public IDataDictionaryBLL dataDictionaryBLL { get; set; }

        public IBaseSetBLL setBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public IAreaBLL areaBLL { get; set; }
        public IParamSetBLL paramBLL { get; set; }


        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMenu(string parentId)
        {
            ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["LoginUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, parentId);
            response.list = list;

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取基础数据
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public JsonResult GetBaseData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                BaseSet set = setBLL.GetModel(); // 基础设置
                Dictionary<string, List<DataDictionary>> result = dataDictionaryBLL.GetAllToDictionary(); //数据字典
                //List<SystemMsg> mlist = msgBLL.GetList(0); //通知提醒
                response.Success();
                Member cur = (Member)Session["LoginUser"];
                //di.Add("notic", mlist);
                di.Add("id", cur.id);
                di.Add("userId", cur.userId);
                di.Add("baseSet", set);
                di.Add("cacheData", result);
                di.Add("area", areaBLL.GetTreeModelList()); //省市区数据
                response.map = di;
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载基础数据失败"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLockTotal()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("pocLockZjfped1", "pocLockZjfped2", "pocLockZjfped3", "pocLockZjfped4", "pocLockZjfped5", "pocLockZjfped6", "pocLockZjfped7");
                double pocLockZjfped1 = Convert.ToDouble(param["pocLockZjfped1"].paramValue) * 10000;
                double pocLockZjfped2 = Convert.ToDouble(param["pocLockZjfped2"].paramValue) * 10000;
                double pocLockZjfped3 = Convert.ToDouble(param["pocLockZjfped3"].paramValue) * 10000;
                double pocLockZjfped4 = Convert.ToDouble(param["pocLockZjfped4"].paramValue) * 10000;
                double pocLockZjfped5 = Convert.ToDouble(param["pocLockZjfped5"].paramValue) * 10000;
                double pocLockZjfped6 = Convert.ToDouble(param["pocLockZjfped6"].paramValue) * 10000;
                double pocLockZjfped7 = Convert.ToDouble(param["pocLockZjfped7"].paramValue) * 10000;

                Dictionary<string, object> di = new Dictionary<string, object>();
                string html_LockTotal = "";
                DataTable dt_DataDictionary = memberBLL.GetDataTable("select * from DataDictionary where type='rLevel' and parentId>0 order by id desc");
                html_LockTotal += @"<thead>
            <tr>
                <th width='150' style='vertical-align:middle;'>档次</th>
               <th width='100' style='vertical-align:middle;'>锁仓笔数</th>
               <th width='150' style='vertical-align:middle;'>锁仓POC</th>
               <th width='150' style='vertical-align:middle;'>总额度</th>
               <th width='150' style='vertical-align:middle;'>剩余额度</th>
            </tr>
        </thead><tbody>";
                int total_pocLockScbs = 0;
                double total_pocLockScpoc = 0;
                double total_pocLockZjfped = 0;
                double total_pocLockZjfped_sy = 0;
                for (int i = 0; i < dt_DataDictionary.Rows.Count; i++)
                {
                    html_LockTotal += "<tr><td>" + dt_DataDictionary.Rows[i]["name"].ToString() + "</td><td>"
                        + dt_DataDictionary.Rows[i]["pocLockScbs"].ToString() + "</td><td>"
                        + dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString() + "</td>";
                    total_pocLockScbs += int.Parse(dt_DataDictionary.Rows[i]["pocLockScbs"].ToString());
                    total_pocLockScpoc += double.Parse(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString());
                    if (dt_DataDictionary.Rows[i]["id"].ToString() == "87")
                    {
                        total_pocLockZjfped += pocLockZjfped1;
                        html_LockTotal += " <td>" + pocLockZjfped1 + "</td>";
                        html_LockTotal += " <td>" + (pocLockZjfped1 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString())) + "</td>";
                        total_pocLockZjfped_sy += (pocLockZjfped1 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString()));
                    }
                    if (dt_DataDictionary.Rows[i]["id"].ToString() == "86")
                    {
                        total_pocLockZjfped += pocLockZjfped2;
                        html_LockTotal += " <td>" + pocLockZjfped2 + "</td>";
                        html_LockTotal += " <td>" + (pocLockZjfped2 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString())) + "</td>";
                        total_pocLockZjfped_sy += (pocLockZjfped2 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString()));
                    }
                    if (dt_DataDictionary.Rows[i]["id"].ToString() == "44")
                    {
                        total_pocLockZjfped += pocLockZjfped3;
                        html_LockTotal += " <td>" + pocLockZjfped3 + "</td>";
                        html_LockTotal += " <td>" + (pocLockZjfped3 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString())) + "</td>";
                        total_pocLockZjfped_sy += (pocLockZjfped3 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString()));
                    }
                    if (dt_DataDictionary.Rows[i]["id"].ToString() == "43")
                    {
                        total_pocLockZjfped += pocLockZjfped4;
                        html_LockTotal += " <td>" + pocLockZjfped4 + "</td>";
                        html_LockTotal += " <td>" + (pocLockZjfped4 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString())) + "</td>";
                        total_pocLockZjfped_sy += (pocLockZjfped4 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString()));
                    }
                    if (dt_DataDictionary.Rows[i]["id"].ToString() == "42")
                    {
                        total_pocLockZjfped += pocLockZjfped5;
                        html_LockTotal += " <td>" + pocLockZjfped5 + "</td>";
                        html_LockTotal += " <td>" + (pocLockZjfped5 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString())) + "</td>";
                        total_pocLockZjfped_sy += (pocLockZjfped5 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString()));
                    }
                    if (dt_DataDictionary.Rows[i]["id"].ToString() == "41")
                    {
                        total_pocLockZjfped += pocLockZjfped6;
                        html_LockTotal += " <td>" + pocLockZjfped6 + "</td>";
                        html_LockTotal += " <td>" + (pocLockZjfped6 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString())) + "</td>";
                        total_pocLockZjfped_sy += (pocLockZjfped6 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString()));
                    }
                    if (dt_DataDictionary.Rows[i]["id"].ToString() == "40")
                    {
                        total_pocLockZjfped += pocLockZjfped7;
                        html_LockTotal += " <td>" + pocLockZjfped7 + "</td>";
                        html_LockTotal += " <td>" + (pocLockZjfped7 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString())) + "</td>";
                        total_pocLockZjfped_sy += (pocLockZjfped7 - Convert.ToDouble(dt_DataDictionary.Rows[i]["pocLockScpoc"].ToString()));
                    }
                    html_LockTotal += "</tr>";
                }
                html_LockTotal += "<tr><td>合计</td><td>" + total_pocLockScbs + "</td><td>" + total_pocLockScpoc + "</td><td>" + total_pocLockZjfped + "</td><td>" + total_pocLockZjfped_sy + "</td></tr>";
                html_LockTotal += " </tbody>";
                di.Add("html_LockTotal", html_LockTotal);
                response.Success();
                response.map = di;
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载基础数据失败"; }
            return Json(response, JsonRequestBehavior.AllowGet);

        }
    }
}
