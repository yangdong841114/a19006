﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 充值查询Controller
    /// </summary>
    public class ChargeController : Controller
    {
        public IChargeBLL chargeBLL { get; set; }
        public ILockRecordBLL lockreBLL { get; set; }
        public ICurrencyErrorBLL currencyErrorBLL { get; set; }
        public ICurrencyBLL cyBll { get; set; }
        public ICurrencySumsBLL currencySumsBLL { get; set; }
        public ICurrencySumDetailsBLL currencySumDetailsBLL { get; set; }

        public JsonResult GetListPageScjl(LockRecord model)
        {
            model.flag = 1;
            var key = System.Web.Configuration.WebConfigurationManager.AppSettings["DataBaseKey"];
            PageResult<LockRecord> page = lockreBLL.GetListPage(model, key);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageZfjl(LockRecord model)
        {
            model.flag = -1;
            var key = System.Web.Configuration.WebConfigurationManager.AppSettings["DataBaseKey"];
            PageResult<LockRecord> page = lockreBLL.GetListPage(model, key);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageErrorCurrency(CurrencyError model)
        {
            PageResult<CurrencyError> page = currencyErrorBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageCurrencySum(CurrencySums model)
        {
            PageResult<CurrencySums> page = currencySumsBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageErrorCurrencyDetails(Currency model)
        {
            PageResult<Currency> result = cyBll.GetErrorDetailListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageCurrencySumDetails(CurrencySumDetails model)
        {
            PageResult<CurrencySumDetails> result = currencySumDetailsBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
