﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 奖金查询Controller
    /// </summary>
    public class CurrencyController : Controller
    {
        public ICurrencyBLL cyBll { get; set; }

        /// <summary>
        /// 分页查询（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetByUserSumCurrency(CurrencySum model)
        {
            var key = System.Web.Configuration.WebConfigurationManager.AppSettings["DataBaseKey"];
            PageResult<CurrencySum> result = cyBll.GetByUserSumCurrency(model, key);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询奖金明细记录（按某个会员)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetDetailListPage(Currency model)
        {
            var key = System.Web.Configuration.WebConfigurationManager.AppSettings["DataBaseKey"];
            PageResult<Currency> result = cyBll.GetDetailListPage(model, key);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }
    }
}
