﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 已开通会员Controller
    /// </summary>
    public class MemberPassedController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        /// <summary>
        /// 分页查询已开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            //model.isPay = 1;
            var key = System.Web.Configuration.WebConfigurationManager.AppSettings["DataBaseKey"];
            string fields = string.Format("id,replace(dbo.nylnz(userId,'{0}'),substring(dbo.nylnz(userId,'{0}'),LEN(dbo.nylnz(userId,'{0}'))/3,LEN(dbo.nylnz(userId,'{0}'))/3),'********') as userId,userName,pocLockScbs1,pocLockScpoc1,pocLockScbs2,pocLockScpoc2,pocLockScbs3,pocLockScpoc3,pocLockScbs4,pocLockScpoc4,pocLockScbs5,pocLockScpoc5,pocLockScbs6,pocLockScpoc6,pocLockScbs7,pocLockScpoc7", key);
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields, key);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }
    }
}
