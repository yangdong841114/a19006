
define(['text!UShoppingCart.html', 'jquery', 'j_easyui'], function (UShoppingCart, $) {

    var controller = function (name) {

        $("#title").html("购物车");


        //计算总金额
        calTotalSumt = function () {
            var total = 0;
            $(".txtamount").each(function () {
                var dataId = $(this).attr("dataId"); //id 
                total = total + (parseFloat($("#fxPrice" + dataId).html()) * parseFloat(this.value));
            });
            $("#submitMoney").html(total);
        }

        var g = /^\d+(\.{0,1}\d+){0,1}$/;

        var indexChecked = [0, 0, 0];

        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#provinceName").val(data[0].value);
            $("#cityName").val(data[1].value);
            if (data.length > 2) {
                $("#areaName").val(data[2].value);
            }
        }

        utils.AjaxPostNotLoadding("/User/UShoppingCart/GetList", {}, function (result) {
            appView.html(UShoppingCart);
            if (result.status == "success") {
                var map = result.map;

                //省市区选择
                var proSet = utils.InitMobileSelect('#provinceName', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#cityName', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#areaName', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                //默认收货信息
                var memberInfo = map.memberInfo;
                $("#receiptName").val(memberInfo.userName);
                $("#phone").val(memberInfo.phone);
                $("#address").val(memberInfo.address);
                $("#provinceName").val(memberInfo.province);
                $("#cityName").val(memberInfo.city);
                $("#areaName").val(memberInfo.area);

                var list = map.list; //购物车信息
                if (list && list.length > 0) {
                    var cont = '';
                    for (var i = 0; i < list.length; i++) {
                        var pro = list[i];
                        var checked = pro.isCheck == 1 ? '' : 'checked="checked"';
                        cont += '<dl id="div' + pro.id + '"><dt><img src="' + pro.imgUrl + '" /></dt>' +
                               '<dd><div class="cartcomminfo"><h2>' + pro.productName + '</h2>' +
                               '<h3 id="fxPrice' + pro.id + '">' + pro.price + '</h3><dl><dt>购买数量</dt>' +
                               '<dd><button class="btnminus" dataId="' + pro.id + '"><i class="fa fa-minus"></i></button>' +
                               '<input type="text" class="txtamount" id="buyNum' + pro.id + '" dataId="' + pro.id + '" value="' + pro.num + '" dataValue="' + pro.num + '" />' +
                               '<button class="btnplus" dataId="' + pro.id + '"><i class="fa fa-plus"></i></button></dd>' +
                               '</dl><dl><dt>总金额</dt><dd><span id="totalPrice' + pro.id + '">¥' + pro.totalPrice + '</span></dd>' +
                               '</dl></div><button class="delbtn" dataId="' + pro.id + '"><i class="fa fa-trash-o"></i><br />删除</button></dd></dl>';
                        //cont += '<div class="row" id="div' + pro.id + '"><div class="col-sm-12"><table ><tr><td>商品编码：</td><td>' + pro.productCode + '</td><td style="padding-left:50px;">商品名称：</td>' +
                        //       '<td>' + pro.productName + '</td></tr></table></div><div class="col-sm-1" style="padding-top:50px;text-align:center;">'+
                        //       '<input style="height:20px;width:20px; cursor:pointer;" dataId="' + pro.id + '" ' + checked + ' type="checkbox" class="ppppCheckBox" /></div>' +
                        //       '<div class="col-sm-2" style="padding-right:0px;padding-top:5px;">' +
                        //       '<img data-toggle="lightbox"  src="' + pro.imgUrl + '" data-image="' + pro.imgUrl + '" class="img-thumbnail" alt="" style="width:225px;height:130px"></div>' +
                        //       '<div class="col-sm-6" style="padding-top:10px;"><table class="table table-borderless"><tr><td align="right" style="padding-top:15px;">市场价：</td>' +
                        //       '<td id="fxPrice' + pro.id + '" style="color: red; padding-top: 15px;">' + pro.price + '</td><td colspan="4" align="right">' +
                        //       '<button type="button" class="btn btn-danger delBtn" dataId="' + pro.id + '"> 删除 </button></td></tr>' +
                        //       '<tr><td align="right" style="padding-top:15px;">购买数量：</td>' +
                        //       '<td align="right"><button type="button" class="btn btn-primary subBtn" dataId="' + pro.id + '"> - </button></td><td width="80px;">' +
                        //       '<input type="text" class="form-control numtext" id="buyNum' + pro.id + '" dataId="' + pro.id + '" dataValue="' + pro.num + '" style="width:80px;" value="' + pro.num + '" /></td>' +
                        //       '<td align="left"><button type="button" class="btn btn-primary addBtn" dataId="' + pro.id + '"> + </button></td>' +
                        //       '<td align="right" style="padding-top:15px;">总金额：</td>' +
                        //       '<td align="left" id="totalPrice' + pro.id + '" style="color: red; padding-top: 15px;">' + pro.totalPrice + '</td>' +
                        //       '</tr></table></div></div><hr/>';
                    }
                    $("#content").html(cont);

                    ////图片控件
                    //$(".img-thumbnail").each(function (index, ele) {
                    //    $(this).lightbox();
                    //});

                    //计算总金额
                    calTotalSumt();

                    ////全选按钮
                    //$("#checkAll").on("click", function () {
                    //    var checked = $("#checkAll")[0].checked;
                    //    var hasBox = false;
                    //    $(".ppppCheckBox").each(function () {
                    //        this.checked = checked;
                    //        hasBox = true;
                    //    });
                    //    if (hasBox) {
                    //        var isChecked = checked ? 2 : 1;
                    //        utils.AjaxPost("UShoppingCart/SaveCheckAll", { isChecked: isChecked }, function (result) {
                    //            if (result.status == "success") {
                    //                calTotalSumt();
                    //            }
                    //        });
                    //    }
                    //});

                    //输入框取消按钮
                    $(".erase").each(function () {
                        var dom = $(this);
                        dom.bind("click", function () {
                            var prev = dom.prev();
                            if (prev[0].id == "provinceName" || prev[0].id == "cityName" || prev[0].id == "area") {
                                $("#provinceName").val("");
                                $("#cityName").val("");
                                $("#areaName").val("");
                            }
                            dom.prev().val("");
                        });
                    });

                    //隐藏提示框
                    $(".hideprompt").click(function () {
                        utils.showOrHiddenPromp();

                    });

                    //清空购物车
                    $("#clearBtn").on("click", function () {
                        $("#propTitle").html("您确定要清空购物车吗？");
                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () {
                            sureClearCart();
                        });
                        utils.showOrHiddenPromp();
                        //var hasBox = false;
                        //$(".ppppCheckBox").each(function () {
                        //    this.checked = checked;
                        //    hasBox = true;
                        //});
                        //if (hasBox) {
                        //}
                    });

                    //清空购物车
                    sureClearCart = function () {
                        utils.AjaxPost("/User/UShoppingCart/DeleteAll", {}, function (result) {
                            if (result.status == "success") {
                                utils.showOrHiddenPromp();
                                $("#content").remove()
                                calTotalSumt();
                                utils.showSuccessMsg("清空成功！");
                            }
                        });
                    }

                    //删除按钮
                    $(".delbtn").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId");
                            //utils.confirm("确定把该商品从购物车删除吗？", function () {
                            utils.AjaxPost("/User/UShoppingCart/Delete", { id: dataId }, function (result) {
                                if (result.status == "success") {
                                    $("#div" + dataId).remove()
                                    calTotalSumt();
                                    utils.showSuccessMsg("删除成功！");
                                }
                            });
                            //});
                        })
                    });

                    //减少数量按钮
                    $(".btnminus").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var old = numDom.val();
                            if (!g.test(old)) return; //不是数字
                            if (old <= 1) return;
                            var num = old - 1;
                            var data = { id: dataId, num: num };
                            utils.AjaxPostNotLoadding("/User/UShoppingCart/Update", data, function (result) {
                                if (result.status == "success") {
                                    var price = $("#fxPrice" + dataId).html();
                                    numDom.val(num);
                                    numDom.attr("dataValue", num);
                                    $("#totalPrice" + dataId).html(price * num);
                                    calTotalSumt();
                                }
                            });
                        })
                    });

                    //增加数量按钮
                    $(".btnplus").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var old = numDom.val();
                            if (!g.test(old)) return; //不是数字
                            if (old <= 0) return;
                            var num = parseInt(old) + 1;
                            var data = { id: dataId, num: num };
                            utils.AjaxPostNotLoadding("/User/UShoppingCart/Update", data, function (result) {
                                if (result.status == "success") {
                                    var price = $("#fxPrice" + dataId).html();
                                    numDom.val(num)
                                    numDom.attr("dataValue", num);
                                    $("#totalPrice" + dataId).html(price * num);
                                    calTotalSumt();
                                }
                            });
                        })
                    });

                    //改变数量离开焦点
                    $(".txtamount").each(function () {
                        var dom = $(this);
                        dom.on("blur", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var num = numDom.val();
                            //如果是错误格式或小于等于0，自动改回来
                            if (!g.test(num) || parseInt(num) <= 0) {
                                numDom.val(numDom.attr("dataValue"));
                            } else {
                                var data = { id: dataId, num: num };
                                utils.AjaxPostNotLoadding("/User/UShoppingCart/Update", data, function (result) {
                                    if (result.status == "success") {
                                        var price = $("#fxPrice" + dataId).html();
                                        $("#buyNum" + dataId).val(num)
                                        $("#buyNum" + dataId).attr("dataValue", num);
                                        $("#totalPrice" + dataId).html(price * num);
                                        calTotalSumt();
                                    }
                                });
                            }
                        })
                    });

                    ////CheckBox点击事件
                    //$(".ppppCheckBox").each(function () {
                    //    var dom = $(this);
                    //    var dataId = dom.attr("dataId"); //id 
                    //    dom.on("click", function () {
                    //        var isChecked = dom[0].checked ? 2 : 1;
                    //        var data = { id: dataId, isChecked: isChecked };
                    //        utils.AjaxPost("UShoppingCart/SaveChecked", data, function (result) {
                    //            if (result.status == "success") {

                    //                calTotalSumt();
                    //            }
                    //        });
                    //    });
                    //});

                    //提交订单
                    $("#saveBtn").on("click", function () {
                        var isChecked = true;
                        if ($("#receiptName").val() == 0) {
                            utils.showErrMsg("请填写收货人");
                        } else if ($("#phone").val() == 0) {
                            utils.showErrMsg("请填写联系电话");
                        } else if ($("#provinceName").val() == 0) {
                            utils.showErrMsg("请选择省");
                        } else if ($("#cityName").val() == 0) {
                            utils.showErrMsg("请选择市");
                        } else if ($("#address").val() == 0) {
                            utils.showErrMsg("请填详细地址");
                        } else {
                            $("#propTitle").html("您确定提交订单吗？");
                            $("#sureBtn").unbind();
                            $("#sureBtn").bind("click", function () {
                                sureSubmitOrder();
                            });
                            utils.showOrHiddenPromp();
                        }
                    });

                    sureSubmitOrder = function(){
                        var data = {
                            receiptName: $("#receiptName").val(), phone: $("#phone").val(), address: $("#address").val(),
                            provinceName: $("#provinceName").val(), cityName: $("#cityName").val(), areaName: $("#areaName").val(),
                        };
                        utils.AjaxPost("/User/UShoppingCart/SaveCheckAll", { isChecked: 2 }, function (result) {
                            if (result.status == "success") {
                                utils.AjaxPost("/User/UShoppingCart/SaveOrder", data, function (result) {
                                    if (result.status == "success") {
                                        utils.showOrHiddenPromp();
                                        utils.showSuccessMsg("提交成功！");
                                        location.href = "#UOrder";
                                    } else {
                                        utils.showErrMsg(result.msg);
                                    }
                                });
                            }
                        });
                    }
                }

            } else {
                utils.showErrMsg(result.msg);
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});