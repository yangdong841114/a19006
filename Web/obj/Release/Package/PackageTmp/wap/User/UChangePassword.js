
define(['text!UChangePassword.html', 'jquery'], function (UChangePassword, $) {

    var controller = function (name) {

        var tab2Init = false;
        var tab3Init = false;

        //修改活动样式
        changeClass = function (index) {
            $("#litab1").removeClass("active");
            $("#litab2").removeClass("active");
            $("#litab3").removeClass("active");
            document.getElementById("passDiv1").style.display = "none";
            document.getElementById("passDiv2").style.display = "none";
            document.getElementById("passDiv3").style.display = "none";
            document.getElementById("passDiv" + index).style.display = "block";
            $("#litab" + index).addClass("active");
        }

        //保存数据
        saveData = function (oldPass, newPass, flag, cls) {
            var data = { oldPass: oldPass, newPass: newPass, flag: flag };
            utils.AjaxPost("/User/UChangePassword/ChangePassword", data, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg("修改成功！");
                    //清空表单
                    $("." + cls).each(function (index, ele) {
                        $(this).val("");
                    });
                }
            });
        }

        //选项卡切换
        changeTab = function (index) {
            changeClass(index);
            if (index == 1) {
                $("#title").html("修改登录密码");
            } else if (index == 2) {
                $("#title").html("修改安全密码");
                if (!tab2Init) {
                    //绑定修改安全密码按钮
                    $("#changBtn2").on('click', function () {
                        if ($("#oldPassOpen").val() == 0) {
                            utils.showErrMsg("旧安全密码不能为空");
                        } else if ($("#passOpen").val() == 0) {
                            utils.showErrMsg("新安全密码不能为空");
                        } else if ($("#passOpen").val() != $("#passOpenRe").val()) {
                            utils.showErrMsg("确认新密码与新安全密码不一致");
                        } else {
                            saveData($("#oldPassOpen").val(), $("#passOpen").val(), 2, "pss2");
                        }
                    });
                    tab2Init = true;
                }
            } else {
                $("#title").html("修改交易密码");
                if (!tab3Init) {
                    //绑定修改交易密码按钮
                    $("#changBtn3").on('click', function () {
                        if ($("#oldThreepass").val() == 0) {
                            utils.showErrMsg("旧交易密码不能为空");
                        } else if ($("#threepass").val() == 0) {
                            utils.showErrMsg("新交易密码不能为空");
                        } else if ($("#threepass").val() != $("#threepassRe").val()) {
                            utils.showErrMsg("确认新密码与新交易密码不一致");
                        } else {
                            saveData($("#oldThreepass").val(), $("#threepass").val(), 3, "pss3");
                        }
                    });
                    tab3Init = true;
                }
            }
        }

        //设置标题
        $("#title").html("修改登录密码")
        appView.html(UChangePassword);


        utils.CancelBtnBind();

        
        //修改登录密码
        $("#changBtn1").on('click', function () {
            if ($("#oldPassword").val() == 0) {
                utils.showErrMsg("旧登录密码不能为空");
            } else if ($("#password").val() == 0) {
                utils.showErrMsg("新登录密码不能为空");
            } else if ($("#password").val() != $("#passwordRe").val()) {
                utils.showErrMsg("确认新密码与新登录密码不一致");
            } else {
                saveData($("#oldPassword").val(), $("#password").val(), 1, "pss1");
            }
        });

        

        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});