
define(['text!ProductDetail.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (ProductDetail, $) {

    var controller = function (id) {

        if (!id || id == 0) {
            utils.showErrMsg("加载失败！未找到商品");
        } else {
            utils.AjaxPostNotLoadding("UProductShop/GetModel", {id:id}, function (result) {
                appView.html(ProductDetail);
                if (result.status == "success") {
                    var dto = result.result;
                    $("#productCode").html(dto.productCode);
                    $("#fxPrice").html(dto.fxPrice);
                    $("#productName").html(dto.productName);
                    $("#imgUrl").attr("src", dto.imgUrl);
                    $("#productId").val(dto.id);
                    $("#imgUrl").attr("data-image", dto.imgUrl);
                    $("#cont").html(dto.cont);
                    $("#imgUrl").lightbox();

                    $("#buyNum").on("focus", function () {
                        var current = $("#buyNum");
                        var parent = current.parent();
                        parent.removeClass("has-error");
                        utils.destoryPopover(current);
                    })

                    $("#buyNum").on("blur", function () {
                        var g = /^\d+(\.{0,1}\d+){0,1}$/;
                        var current = $("#buyNum");
                        if (g.test(current.val())) {
                            $("#total").val(current.val() * dto.fxPrice);
                        } else {
                            $("#total").val("");
                        }
                    })

                    //放入购物车
                    $("#saveBtn").on('click', function () {
                        var g = /^\d+(\.{0,1}\d+){0,1}$/;
                        if (!g.test($("#buyNum").val())) {
                            var current = $("#buyNum");
                            var parent = current.parent();
                            parent.addClass("has-error");
                            utils.showPopover(current, "购物数量格式错误", "popover-danger");
                        } else {
                            var data = { productId: $("#productId").val(), num: $("#buyNum").val() };
                            utils.AjaxPost("UShoppingCart/Save", data, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("加入成功！");
                                }
                            });
                        }
                    });

                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }


        controller.onRouteChange = function () {
        };
    };

    return controller;
});