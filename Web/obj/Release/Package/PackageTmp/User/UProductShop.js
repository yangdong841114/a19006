
define(['text!UProductShop.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UProductShop, $) {

    var controller = function (name) {

        var count = 16; //每页条数
        var queryModel = { page: 1, rows: count };
        var nowPage = 1;

        utils.AjaxPostNotLoadding("UProductShop/GetListPage", queryModel, function (result) {
            appView.html(UProductShop);
            if (result) {
                var rows = result.rows;
                
                if (rows && rows.length > 0) {
                    //显示内容
                    createContent(rows);
                    createPager(nowPage, result.total);
                }
            }

            //条件查询
            $("#queryProductBtn").bind('click', function () {
                if ($("#productName").val() == 0) {
                    queryModel["productName"] = "";
                } else {
                    queryModel["productName"] = $("#productName").val();
                }
                queryData(1);
            })
        });

        

        //查询数据
        queryData = function (newPage) {
            queryModel["page"] = newPage;
            utils.AjaxPost("UProductShop/GetListPage", queryModel, function (result) {
                if (result) {
                    var rows = result.rows;
                    if (rows && rows.length > 0) {
                        //显示内容
                        createContent(rows);
                        createPager(newPage, result.total);
                    }
                }
            });
        }

        //生成内容
        createContent = function (rows) {
            var cont = "";
            //显示内容
            for (var i = 1; i <= rows.length; i++) {
                if (i == 1 || i % 4 == 0) {
                    cont += '<div class="form-group">';
                }
                cont += '<div class="col-sm-3"><a href="#ProductDetail/' + rows[i - 1].id + '"><img data-toggle="lightbox" src="' + rows[i - 1].imgUrl + '"  class="img-thumbnail" alt="" style="width:225px;height:130px"></a>'
                cont += '<div class="col-sm-12" style="font-weight:bold;padding-top:3px;">' + rows[i - 1].productName + '</div>';
                cont += '<div class="col-sm-6" style="padding-top:3px;">价格：<font style="color:red;">' + rows[i - 1].fxPrice + '</font></div>';
                cont += '<div class="col-sm-6" style="padding-top:3px;text-align:right;"><a href="#ProductDetail/' + rows[i - 1].id + '">查看详情</a></div>';
                cont += '</div>';

                if (i > 1 && i % 4 == 0) {
                    cont += '</div>';
                }
            }
            $("#ffform").empty();
            $("#ffform").html(cont);
        }


        //生成分页页码
        createPager = function(newPage,total){
            var pageSize = Math.ceil(total / count);
            nowPage = newPage;

            //页码下拉框解除改变事件
            $("#pageSelect").unbind("change", changeFunction);

            //页码下拉框
            $("#pageSelect").empty();
            for (var i = 1; i <= pageSize; i++) {
                var val = "";
                if (i == nowPage) {
                    val = "<option value='" + i + "' selected='true'>" + i + "</option>";
                } else {
                    val = "<option value='" + i + "'>" + i + "</option>";
                }
                $("#pageSelect").append(val);
            }

            //页码下拉框改变事件
            $("#pageSelect").bind("change", changeFunction);

            //页码显示区
            var cont = '';
            if (nowPage <= 1) {
                cont += '<li class="previous"><a href="javascript:void(0);">第一页</a></li>';
                cont += '<li><a href="javascript:void(0);">上一页</a></li>';
            } else {
                cont += '<li class="previous"><a href="javascript:void(0);" onclick="queryData(1)">第一页</a></li>';
                cont += '<li><a href="javascript:void(0);" onclick="queryData(' + (nowPage - 1) + ')">上一页</a></li>';
            }
            if (nowPage >= pageSize) {
                cont += '<li><a href="javascript:void(0);">下一页</a></li>';
                cont += '<li class="previous"><a href="javascript:void(0);">最后页</a></li>';
            } else {
                cont += '<li><a href="javascript:void(0);" onclick="queryData(' + (nowPage + 1) + ')">下一页</a></li>';
                cont += '<li class="previous"><a href="javascript:void(0);" onclick="queryData(' + pageSize + ')">最后页</a></li>';
            }
            $("#pageUL").empty();
            $("#pageUL").html(cont);
            var pageMsg = "每页&nbsp;" + count + "&nbsp;条，共&nbsp;" + total + "&nbsp;条记录，当前第&nbsp;" + nowPage + "/" + pageSize + "&nbsp;页";
            $("#pageMsg").empty();
            $("#pageMsg").html(pageMsg);

        }
        
        //下拉框改变事件
        changeFunction = function () {
            var val = $("#pageSelect").val();
            if (val != 0 && val != nowPage) {
                queryData(val);
            }
        }

        controller.onRouteChange = function () {
        };
    };

    return controller;
});