
define(['text!OperateLog.html', 'jquery', 'j_easyui', 'datetimepicker'], function (OperateLog, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "操作日志");
        appView.html(OperateLog);

        //初始化日期
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化业务类型下拉框
        $("#typeId").append("<option value='0'>全部</option>");
        utils.AjaxPostNotLoadding("/Admin/OperateLog/GetListType", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                for (var i = 0; i < result.rows.length; i++) {
                    $("#typeId").append("<option value='" + result.rows[i].recordName + "'>" + result.rows[i].recordName + "</option>");
                }
            }
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '操作人', width: '10%' }]],
            columns: [[
             { field: 'userName', title: '姓名', width: '10%' },
             { field: 'addTime', title: '操作时间', width: '13%' },
             { field: 'ipAddress', title: 'IP', width: '13%' },
             { field: 'recordName', title: '业务类型', width: '15%' },
             { field: 'mulx', title: '业务摘要', width: '38%' },
            ]],
            url: "OperateLog/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});