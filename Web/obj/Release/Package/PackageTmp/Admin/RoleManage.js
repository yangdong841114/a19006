
define(['text!RoleManage.html', 'jquery', 'j_easyui'], function (RoleManage, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "角色管理");
        appView.html(RoleManage);

        //初始化角色增改弹出框
        $('#dlg').dialog({
            title: '编辑记录',
            width: 500,
            height: 300,
            cache: false,
            modal: true
        })

        //初始化添加角色会员关系弹出框
        $('#dlg2').dialog({
            title: '添加角色会员关系',
            width: 500,
            height: 600,
            cache: false,
            modal: true
        })

        //初始化分配菜单弹出框
        $('#dlg3').dialog({
            title: '分配菜单',
            width: 500,
            height: 600,
            cache: false,
            modal: true
        })

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        /***************************************** 选择会员列表表格 START **********************************************************************************/

        //初始化选择会员列表表格
        var dialogGrid = utils.newGrid("dialogGird", {
            singleSelect: false,
            title: '选择会员',
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '49%' }]],
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'userName', title: '会员名称', width: '49%' },
            ]],
        })

        //查询选择会员列表表格
        selectMemberQuery = function (roleId,roleName,roleType) {
            var param = { "id": roleId, "isAdmin": roleType == 1 ? 0 : 1 };
            var gridPanel = dialogGrid.datagrid("getPanel");//先获取panel对象 
            $("#mRoleId").val(roleId);
            gridPanel.panel('setTitle', "请选择分配为【" + roleName + "】角色的" + (roleType == 1 ? "前台" : "后台") + "会员");
            dialogGrid.datagrid("options").url = "RoleManage/GetNotRoleMemberPage"
            dialogGrid.datagrid("options").queryParams = param;
            dialogGrid.datagrid("reload");
        }

        //分配角色会员关系保存按钮
        $("#saveRmBtn").bind("click", function () {
            var rows = dialogGrid.datagrid("getChecked");
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择要分配的会员！"); }
            else {
                var roleId = $("#mRoleId").val();
                var data = {};
                for (var i = 0; i < rows.length; i++) {
                    data["list[" + i + "].id"] = roleId;
                    data["list[" + i + "].memberId"] = rows[i].id;
                }
                utils.AjaxPost("RoleManage/SaveRm", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        //关闭弹出框
                        $('#dlg2').dialog("close");
                        //重刷角色会员分配关系表grid
                        rmQueryGrid();
                    }
                });
            }
            return false;
        })

        /***************************************** 选择会员列表表格 END **********************************************************************************/

        /***************************************** 菜单tree START **********************************************************************************/

        //ztree设置
        var treesetting = {
            data: {
                simpleData: {
                    enable: true,
                    idKey: "id", 	              // id编号字段名
                    pIdKey: "parentResourceId",   // 父id编号字段名
                    rootPId: null
                },
                key: {
                    name: "resourceName"    //显示名称字段名
                }
            },
            check: {
                autoCheckTrigger: true,
                enable: true,
                chkStyle: "checkbox"
            },
            view: {
                showLine: false,            //不显示连接线
                dblClickExpand: false       //禁用双击展开
            },
            callback: {
                onClick: function (e, treeId, node) {
                    if (node.isParent) {
                        resourceTree.expandNode(node);
                    }
                }
            }
        };

        var beforeMenu = null, afterMenu = null; //前端菜单、后台菜单

        var resourceTree = undefined; //ztree对象

        //打开分配菜单弹出框
        openSelectMenuDialog = function (roleId, roleType,roleName) {
            //加载角色已有菜单
            utils.AjaxPostNotLoadding("RoleManage/GetCheckedMenus", { roleId: roleId }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg("加载角色已分配菜单失败，"+result.msg);
                } else {
                    var menus = roleType == 1 ? beforeMenu : afterMenu;
                    $("#menuRoleId").val(roleId);
                    //生成树
                    reInitTree(menus, result.list);
                    //打开窗口
                    $('#dlg3').dialog({ title: '角色【' + roleName + '】分配菜单' }).dialog("open");
                }
            });
        }

        //重新生成树
        reInitTree = function (menus,checkedMenus) {
            //先销毁树
            if (resourceTree) {
                resourceTree.destroy();
            }
            //var newMenus = $.map(menus, function (n) { return n; });
            var newMenus = $.extend(true, [], menus);
            //选中已分配的菜单
            if (checkedMenus && checkedMenus.length > 0) {
                for (var i = 0; i < checkedMenus.length; i++) {
                    var checkNode = checkedMenus[i];
                    for (var j = 0; j < newMenus.length; j++) {
                        var node = newMenus[j];
                        if (checkNode.id == node.id) {
                            newMenus[j]["checked"] = true;
                            break;
                        }
                    }
                }
            }
            //生成树
            resourceTree = $.fn.zTree.init($("#roleMenuTree"), treesetting, newMenus);

            //查找根节点并自动展开
            var rootNodes = resourceTree.getNodesByFilter(function (node) {
                if (node.parentResourceId == null || node.parentResourceId == 0) {
                    return true;
                }
            });
            if (rootNodes && rootNodes.length > 0) {
                for (var i = 0; i < rootNodes.length; i++) {
                    resourceTree.expandNode(rootNodes[i], true, false, true);
                }
            }
        }

        //加载系统前台菜单和后台菜单
        utils.AjaxPostNotLoadding("RoleManage/GetMenuMap", null, function (result) {
            if (result) {
                beforeMenu = result.before;
                afterMenu = result.after;
            }
        });

        //分配菜单保存按钮
        $("#saveMenuBtn").bind("click", function () {
            var nodes = resourceTree.getCheckedNodes(true);
            if (!nodes || nodes.length == 0) {
                utils.showErrMsg("您还未勾选任何菜单，不能保存");
            } else {
                var roleId = $("#menuRoleId").val();
                var data = {};
                for (var i = 0; i < nodes.length; i++) {
                    data["list[" + i + "].id"] = roleId;
                    data["list[" + i + "].resourceId"] = nodes[i].id;
                }
                utils.AjaxPost("RoleManage/SaveRoleResorce", data, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg(result.msg);
                        //关闭弹出框
                        $('#dlg3').dialog("close");
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            }
            return false;
        })

        /***************************************** 菜单tree END **********************************************************************************/

        /***************************************** 角色列表表格 START **********************************************************************************/

        //初始化角色列表表格
        var grid = utils.newGrid("dg", {
            title: '角色列表',
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'id', title: '角色ID', width: '10%' },
             { field: 'roleName', title: '角色名称', width: '15%' },
             { field: 'roleTypeName', title: '角色类型', width: '15%' },
             { field: 'remark', title: '描述', width: '43%' }
            ]],
            onClickRow: function (index, data) {
                memberGrid.datagrid("options").url = "RoleManage/GetRoleMemberPage";
                memberGrid.datagrid("options").queryParams = { id: data.id };
                memberGrid.datagrid("reload");
                //utils.AjaxPost("RoleManage/GetRoleMemberPage", {id:data.id}, function (result) {
                //    if (result.status == "success") {
                //        utils.showSuccessMsg("发货成功");
                //        $('#dlg').dialog("close");
                //        queryGrid();
                //    } else {
                //        utils.showErrMsg(result.msg);
                //    }
                //});
                //memberGrid.datagrid("loadData", data.items);
            },
            url: "RoleManage/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["roleTypeName"] = data.rows[i].roleType == 1 ? "前台角色" : "后台角色";
                }
            }
            return data;
        })

        //清除错误样式
        $(".form-control").each(function () {
            var dom = $(this);
            dom.on("focus", function () {
                dom.removeClass("inputError");
            })
        });

        //角色编辑弹出框保存按钮
        $("#saveBtn").bind("click", function () {
            var checked = true;
            $(".form-control").each(function () {
                var dom = $(this);
                if (dom.val() == 0 && dom[0].id != "remark") {
                    dom.addClass("inputError");
                    checked = false;
                }
            });
            if (checked) {
                var data = { roleType: $("#roleType").val(), roleName: $("#roleName").val(), id: $("#id").val(), remark: $("#remark").val() };
                utils.AjaxPost("RoleManage/SaveOrUpdate", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        //关闭弹出框
                        $('#dlg').dialog("close");
                        //重刷grid
                        queryGrid();
                    }
                });
            }
            return false;
        })

        //角色列表编辑数据
        $("#gridEditBtn").linkbutton({
            onClick: function () {
                var obj = grid.datagrid("getSelected");
                if (obj == null) { utils.showErrMsg("请选择需要编辑的行！"); }
                else {
                    //清空表单数据
                    $('#editModalForm').form('reset');
                    //根据选中的行记录加载数据
                    $("#id").val(obj.id);
                    $("#roleType").val(obj.roleType);
                    $("#roleName").val(obj.roleName);
                    $("#remark").val(obj.remark);
                    $('#dlg').dialog({ title: '编辑角色' }).dialog("open");

                }
            }
        });

        //角色列表添加数据
        $("#gridAddBtn").linkbutton({
            onClick: function () {
                //清空表单数据
                $('#editModalForm').form('reset');
                //清空id（编辑、添加使用同一个表单，所以这里需要清除）
                $("#id").val('');
                $('#dlg').dialog({ title: '添加角色' }).dialog("open");
            }
        });

        //角色列表添加角色会员分配关系
        $("#mgAddBtn").linkbutton({
            onClick: function () {
                var obj = grid.datagrid("getSelected");
                if (obj == null) { utils.showErrMsg("请选择需要分配关系的角色！"); }
                else {

                    selectMemberQuery(obj.id,obj.roleName,obj.roleType);
                    $('#dlg2').dialog({ title: '添加角色会员关系' }).dialog("open")
                }
            }
        });

        //角色列表分配菜单
        $("#gridMenuBtn").linkbutton({
            onClick: function () {
                var obj = grid.datagrid("getSelected");
                if (obj == null) { utils.showErrMsg("请选择需要分配菜单的角色！"); }
                else {
                    openSelectMenuDialog(obj.id,obj.roleType,obj.roleName);
                }
            }
        });

        //角色列表删除记录
        $("#gridDelBtn").linkbutton({
            onClick: function () {
                var obj = grid.datagrid("getSelected");
                if (obj == null) { utils.showErrMsg("请选择需要删除的行！"); }
                else {
                    utils.confirm("您确定要删除吗？", function () {
                        utils.AjaxPost("RoleManage/Delete", { id: obj.id }, function (result) {
                            if (result.msg == "success") {
                                utils.showSuccessMsg("删除成功");
                                //重刷grid
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                }
            }
        });

        //查询角色grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            if (objs) {
                for (name in objs) {
                    if (name == "roleName2") { objs["roleName"] = objs[name]; }
                    else if (name == "roleType2") { objs["roleType"] = objs[name]; }
                }
            }
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //角色grid查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        /***************************************** 角色列表表格 END **********************************************************************************/

        /***************************************** 角色会员分配关系表 START **********************************************************************************/

        //角色会员分配关系表删除记录
        $("#mgDelBtn").linkbutton({
            onClick: function () {
                var rows = memberGrid.datagrid("getChecked");
                if (rows == null ||　rows.length==0) { utils.showErrMsg("请选择需要删除的行！"); }
                else {
                    utils.confirm("您确定要删除吗？", function () {
                        var data = {};
                        for (var i = 0; i < rows.length; i++) {
                            data["list[" + i + "]"] = rows[i].rmId;
                        }

                        utils.AjaxPost("RoleManage/DeleteRm", data, function (result) {
                            if (result.msg == "success") {
                                utils.showSuccessMsg("删除成功");
                                //重刷grid
                                rmQueryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                }
            }
        });

        //初始化角色会员分配关系表表格
        var memberGrid = utils.newGrid("memberGrid", {
            singleSelect:false,
            title: '角色会员分配关系表',
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'id', title: '角色ID', width: '15%' },
             { field: 'roleName', title: '角色名称', width: '21%' },
             { field: 'roleType', title: '角色类型', width: '21%' },
             { field: 'userId', title: '会员编号', width: '21%' },
             { field: 'userName', title: '会员名称', width: '21%' },
            ]],
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["roleType"] = data.rows[i].roleType == 1 ? "前台角色" : "后台角色";
                }
            }
            return data;
        })

        //查询角色会员分配关系表grid
        rmQueryGrid = function () {
            var objs = $("#RoleMemberQueryForm").serializeObject();
            if (objs) {
                for (name in objs) {
                    if (name == "roleName3") { objs["roleName"] = objs[name]; }
                    else if (name == "roleType3") { objs["roleType"] = objs[name]; }
                    else if (name == "userId3") { objs["userId"] = objs[name]; }
                    else if (name == "userName3") { objs["userName"] = objs[name]; }
                }
            }
            memberGrid.datagrid("options").queryParams = objs;
            memberGrid.datagrid("reload");
        }

        //角色会员分配关系表grid查询按钮
        $("#mgQueryBtn").on("click", function () {
            rmQueryGrid();
        })

        /***************************************** 角色会员分配关系表 END **********************************************************************************/


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
            $('#dlg2').dialog("destroy");
            $('#dlg3').dialog("destroy");
        };
    };

    return controller;
});