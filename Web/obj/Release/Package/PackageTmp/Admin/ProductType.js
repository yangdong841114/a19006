
define(['text!ProductType.html', 'jquery', 'j_easyui', 'datetimepicker'], function (ProductType, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "类目设置");
        appView.html(ProductType);

        //初始化表单区域
        $('#dlg').dialog({
            title: '类目设置',
            width: 400,
            height: 150,
            cache: false,
            modal: true
        });

        //获取父类
        //utils.AjaxPostNotLoadding("ProductType/GetDefaultData", {}, function (result) {
        //    if (result.status == "fail") {
        //        utils.showErrMsg(result.msg);
        //    } else {
        //        var map = result.map;
        //        var mlist = map.mlist;
        //        //初始化下拉框
        //        $("#parent").empty();
        //        $("#parent").append("<option value='0' dataName = '无'>--无父级类别--</option>");
        //        if (mlist != null && mlist.length > 0) {
        //            for (var i = 0; i < mlist.length; i++) {
        //                var vo = mlist[i];
        //                $("#parent").append("<option value='" + vo.id + "' dataName = '" + vo.name + "'>" + vo.name + "</option>");
        //            }
        //        }
        //    }
        //});

       

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //保存按钮
        $("#saveBtn").bind("click", function () {
            if ($("#name").val() == 0) {
                utils.showErrMsg("请输入名称");
            }
           else {
                var  parentNmaePara= $("#parent").find("option:selected").attr("dataName");
                var parentIdPara = $("#parent").find("option:selected").attr("value");
                utils.AjaxPost("ProductType/SaveByUeditor", { name: $("#name").val(), parentId: $("#parentId").val(), parentNmae: $("#parentNmae").val(), grade: $("#grade").val(), id: $("#id").val() == 0 ? "" : $("#id").val() }, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("操作成功");
                        $('#dlg').dialog("close");
                        queryGrid();
                        SmqueryGrid();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            }
        })

        //打开明细
        openDetailDialog = function (row) {
            $("#id").val(row.id);
            $("#parentId").val(row.parentId);
            $("#parentNmae").val(row.parentNmae);
            $("#grade").val(row.grade);
            $("#name").val(row.name);
            $('#dlg').dialog({ title: '修改类目' }).dialog("open");
        }
        SmopenDetailDialog = function (row) {
            $("#id").val(row.id);
            $("#parentId").val(row.parentId);
            $("#parentNmae").val(row.parentNmae);
            $("#grade").val(row.grade);
            $("#name").val(row.name);
            $('#dlg').dialog({ title: '修改类目' }).dialog("open");
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             { field: 'name', title: '一级类目名称', width: '55%' },
             {
                 field: '_deleteRecord', title: '操作', width: '40%', formatter: function (val, row, index) {
                     var cont = "";
                   
                     cont += '<a href="javascript:void(0);" rowIndex ="' + index + '" class="recordEdit" >编辑</a>&nbsp;&nbsp;' +
                            '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >删除</a>';
                     return cont;
                 }
             }
            ]],
            onClickRow: function (index, data) {
                $("#Sm_parentId").val(data.id);
                $("#Sm_parentNmae").val(data.name);
                SmqueryGrid();
                document.getElementById("div_sm").style.display = "block";
            },
            toolbar: [{
                text:"新增一级类目",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $("#parentId").val("0");
                    $("#parentNmae").val("");
                    $("#grade").val("1");
                    $("#name").val("");
                    $("#id").val("");
                    $('#dlg').dialog({ title: '类目设置' }).dialog("open");
                }
            }
            ],
            url: "ProductType/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                   // data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {
            //点击编辑按钮编辑
            $(".recordEdit").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = grid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    openDetailDialog(row);
                })
            })

            //删除
            $(".recordDelete").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("您确定要删除该记录？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("ProductType/Delete", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg(result.msg);
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    var name = name == "S_productTypeName" ? "name" : name;
                    queryObj[name] = val;
                }
            });
            grid.datagrid("options").queryParams = queryObj;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })



        var Smgrid = utils.newGrid("Smdg", {
            columns: [[
             { field: 'name', title: '二级类目名称', width: '55%' },
             {
                 field: '_deleteRecord', title: '操作', width: '40%', formatter: function (val, row, index) {
                     var cont = "";

                     cont += '<a href="javascript:void(0);" rowIndex ="' + index + '" class="SmrecordEdit" >编辑</a>&nbsp;&nbsp;' +
                            '<a href="javascript:void(0);" dataId ="' + row.id + '" class="SmrecordDelete" >删除</a>';
                     return cont;
                 }
             }
            ]],
            toolbar: [{
                text: "新增二级类目",
                iconCls: 'icon-add',
                handler: function () {
                    $("#parentId").val($("#Sm_parentId").val());
                    $("#parentNmae").val($("#Sm_parentNmae").val());
                    $("#grade").val("2");
                    $("#name").val("");
                    $("#id").val("");
                    $('#dlg').dialog({ title: '类目设置' }).dialog("open");
                }
            }
            ],
            url: "ProductType/SmGetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    // data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {
            //点击编辑按钮编辑
            $(".SmrecordEdit").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = Smgrid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    SmopenDetailDialog(row);
                })
            })

            //删除
            $(".SmrecordDelete").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("您确定要删除该记录？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("ProductType/Delete", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg(result.msg);
                                SmqueryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })
        })

        //查询grid
        SmqueryGrid = function () {
            var objs = $("#SmQueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    var name = name == "Sm_productTypeName" ? "name" : name;
                    name = name == "Sm_parentId" ? "parentId" : name;
                    queryObj[name] = val;
                }
            });
            Smgrid.datagrid("options").queryParams = queryObj;
            Smgrid.datagrid("reload");
        }

        //查询按钮
        $("#SmqueryBtn").on("click", function () {
            SmqueryGrid();
        })



        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});