
define(['text!MemberPassed.html', 'jquery', 'j_easyui', 'datetimepicker'], function (MemberPassed, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "用户列表");
        appView.html(MemberPassed);

       //显示总计
        utils.AjaxPostNotLoadding("ManageWeb/GetLockTotal", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
                return;
            }
            var html_LockTotal = result.map.html_LockTotal;
            $("#dgtotal").html(html_LockTotal);
        });
       

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[{ field: 'userId', title: 'POC钱包地址', width: '200' }]],
            columns: [
             [
             { "title": "POC创始团队", "colspan": 2 },
             { "title": "POC技术团队", "colspan": 2 },
             { "title": "超级节点", "colspan": 2 },
             { "title": "高级节点", "colspan": 2 },
             { "title": "忠诚节点", "colspan": 2 },
             { "title": "守护者", "colspan": 2 },
             { "title": "个体用户", "colspan": 2 }
             ]
             ,
             [
             { field: 'pocLockScbs1', title: '锁仓笔数', width: '80', "rowspan": 1 },
             { field: 'pocLockScpoc1', title: '锁仓POC', width: '80', "rowspan": 1 },
             
             { field: 'pocLockScbs2', title: '锁仓笔数', width: '80', "rowspan": 1 },
             { field: 'pocLockScpoc2', title: '锁仓POC', width: '80', "rowspan": 1 },

              { field: 'pocLockScbs3', title: '锁仓笔数', width: '80', "rowspan": 1 },
             { field: 'pocLockScpoc3', title: '锁仓POC', width: '80', "rowspan": 1 },

              { field: 'pocLockScbs4', title: '锁仓笔数', width: '80', "rowspan": 1 },
             { field: 'pocLockScpoc4', title: '锁仓POC', width: '80', "rowspan": 1 },

              { field: 'pocLockScbs5', title: '锁仓笔数', width: '80', "rowspan": 1 },
             { field: 'pocLockScpoc5', title: '锁仓POC', width: '80', "rowspan": 1 },

              { field: 'pocLockScbs6', title: '锁仓笔数', width: '80', "rowspan": 1 },
             { field: 'pocLockScpoc6', title: '锁仓POC', width: '80', "rowspan": 1 },

              { field: 'pocLockScbs7', title: '锁仓笔数', width: '80', "rowspan": 1 },
             { field: 'pocLockScpoc7', title: '锁仓POC', width: '80', "rowspan": 1 }
             ]
            ],

            url: "MemberPassed/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["passTime"] = utils.changeDateFormat(data.rows[i]["passTime"]);
                   
                }
            }
            return data;
        }, function () {
          
          
          
        })

       

       

       

       


        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
        
        };
    };

    return controller;
});