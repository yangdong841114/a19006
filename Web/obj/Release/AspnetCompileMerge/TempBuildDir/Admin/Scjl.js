
define(['text!Scjl.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Scjl, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "锁仓记录");
        appView.html(Scjl);

        //初始select
        var cList = cacheList["rLevel"];
        if (cList && cList.length > 0) {
            $("#LockType").empty();
            $("#LockType").append("<option value='0'>--全部档次--</option>");
            for (var i = 0; i < cList.length; i++) {
                $("#LockType").append("<option value='" + cList[i].id + "'>" + cList[i].name + "</option>");
            }
        }

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: 'POC钱包地址', width: '200' }]],
            showFooter: true,
            columns: [[
                { field: 'OrderId', title: 'OrderId', width: '150' },
                { field: 'LockTypeName', title: '档次', width: '100' },
                { field: 'pocValue', title: '锁仓POC', width: '120' },
                { field: 'getTime', title: '日期', width: '130' },
                {
                    field: 'HASH', title: 'HASH', width: '200', align: 'center', formatter: function (val, row, index) {
                      
                        return '<a href="javascript:void(0);" dataId="' + row.HASH + '" class="gridFildEdit" >' + row.HASH + '</a>&nbsp;&nbsp;';
                      
                    }
                }
            ]],
            url: "Charge/GetListPageScjl"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["getTime"] = utils.changeDateFormat(data.rows[i]["getTime"]);
                    data.rows[i]["flag"] = data.rows[i].flag == 1 ? "有锁仓档次" : "无锁仓档次";
                }
               
            }
            return data;
        }, function () {
           
            //行确认按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    window.open('https://etherscan.io/tx/' + dataId);
                }
            });
            
           
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }


        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});