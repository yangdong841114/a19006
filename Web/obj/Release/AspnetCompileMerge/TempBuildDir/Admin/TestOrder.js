
define(['text!TestOrder.html', 'jquery', 'j_easyui', 'zui'], function (TestOrder, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "测试订单录入");
        appView.html(TestOrder);


       
        
    

       

        //保存start
        $("#saveBtn").on('click', function () {
            var fs = $("#registerForm").serializeObject();
         
            utils.AjaxPost("/Home/CallbackReceivePoc", fs, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                    }
                });
            
        }); ////保存end

        //保存start
        $("#sdBonusBtn").on('click', function () {
          

            utils.AjaxPost("/Home/SdBonus", {}, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg(result.msg);
                }
            });

        }); ////保存end
        

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#regContent').dialog("destroy");
        };
    };

    return controller;
});