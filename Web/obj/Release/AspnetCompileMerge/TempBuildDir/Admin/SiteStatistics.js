﻿
define(['text!SiteStatistics.html', 'jquery'], function (SiteStatistics, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "前台设置");


        utils.AjaxPostNotLoadding("Website/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(SiteStatistics);

                var dto = result.result;
                $("#WebCode").val(dto.WebCode);
                $("#WebCode").on("focus", function () {
                    $("#WebCode").removeClass("inputError");
                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#WebCode").val();
                    if (val == null || val == undefined|| val== "") {
                        $("#WebCode").addClass("inputError");
                    } else {
                        var data = { id: 1, WebCode: val };
                        utils.AjaxPost("Website/Save", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }

                })
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});