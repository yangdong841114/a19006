
'use strict';

(function (win) {
    //配置baseUrl
    //var baseUrl = document.getElementById('main').getAttribute('data-baseurl');

    /*
     * 文件依赖
     */
    var config = {
        waitSeconds: 30, //加载js超时时间，30秒
        //baseUrl: baseUrl,           //依赖相对路径
        map: {
            '*': {
                'css': '../Content/js/css.min'
            }
        },
        paths: {                    //如果某个前缀的依赖不是按照baseUrl拼接这么简单，就需要在这里指出
            director: '../Content/js/director.min',
            jquery: '../Content/js/jquery-3.2.1.min',
            underscore: '../Content/js/underscore',
            zui: '../Content/js/zui_admin',
            datetimepicker: '../Content/js/datetimepicker.min',
            j_easyui: '../Content/easyui/jquery.easyui.min',
            ztree: '../Content/ztree/js/jquery.ztree.all.min',
            ueditor_config: '../Content/ueditor/ueditor.config',
            ueditor: '../Content/ueditor/ueditor.all.min',
            ZeroClipboard: '../Content/ueditor/third-party/zeroclipboard/ZeroClipboard.min',
            quill: '../Content/quill/quill',
            text: '../Content/js/text'             //用于requirejs导入html类型的依赖
        },
        shim: {                     //引入没有使用requirejs模块写法的类库。
            underscore: {
                exports: '_'
            },
            jquery: {
                exports: '$'
            },
            director: {
                exports: 'Router'
            },
            ztree: {
                exports: 'ztree',
                deps: ['jquery',
                    'css!../Content/ztree/css/zTreeStyle/zTreeStyleAdmin.css'
                ]
            },
            j_easyui: {
                exports: 'j_easyui',
                deps: ['jquery',
                    'css!../Content/easyui/themes/material/easyui.css',
                    'css!../Content/easyui/themes/icon.css',
                ]
            },
            zui: {
                exports: 'zui',
                deps: ['jquery', 'css!../Content/css/zuiAdmin.css']
            },
            datetimepicker: {
                exports: 'datetimepicker',
                deps: ['jquery', 'zui', 'css!../Content/css/datetimepicker.min.css']
            },
            ueditor_config: {
                exports: 'ueditor_config',
            },
            ZeroClipboard: {
                exports: 'ZeroClipboard',
            },
            quill: {
                exports: 'quill',
                deps: ['jquery', 'css!../Content/quill/quill.css']
            },
            ueditor: {
                exports: 'ueditor',
                deps: ['ueditor_config',  'css!../Content/ueditor/themes/iframe.css']
            },
        }
    };

    require.config(config);
    require(['jquery', 'router', '../Content/js/CommonUtil', 'ZeroClipboard', 'quill', 'j_easyui', 'ztree', 'zui'], function ($, router, util, ZeroClipboard,quill) {
        $("#pppppxxx").panel({});
        win.appView = $('#center');      //用于各个模块控制视图变化
        win.$ = $;                          //暴露必要的全局变量，没必要拘泥于requirejs的强制模块化
        win._ = _;
        win.Quill = quill;
        win.utils = util;
        win.cacheMap = null;
        win.cacheList = null;
        win.pass2 = undefined;
        win.pass3 = undefined;
        win.menuPass = undefined;
        window['ZeroClipboard'] = ZeroClipboard;
        win.areaData = null;

        $("#exitSystem").on("click", function () {
            util.confirm("确定要退出系统吗？", function () {
                util.AjaxPost("/Home/ExitB", {}, function (result) {
                    if (result.status == "success") {
                        location.href = "/ManageLogin.html";
                    }
                });
            });
        });

        //注册404路由
        router.toUrl("NoPermission", "NoPermission.js");
        //注册无权限路由
        router.toUrl("NotFound", "NotFound.js");
        //注册安全密码路由
        router.toUrl("Password2/:page", "Password2.js");
        //注册交易密码路由
        router.toUrl("Password3/:page", "Password3.js");
        router.toUrl("SiteStatistics", "SiteStatistics.js");
        router.toUrl("SetCourier", "SetCourier.js");
        router.configure({
            //未匹配到URL
            notfound: function () { location.href = '#NotFound';},
            before: function (a, b, c) {
                util.DestoryAllPopover();
                win.appView.empty();
                win.appView.html("<table width=\"100%\"><tr>"+
                "<td align=\"center\" style=\"padding-top:15%;\"><i class=\"icon icon-spin icon-spinner-snake\" style=\"font-size:150px;\"></i><br/>" +
                    "<h3>加载中......</h3></td></tr></table>");

                //获取请求地址
                var hash = window.location.hash;
                hash = hash.substring(1, hash.length);
                var param = undefined;
                if (hash.indexOf("/") != -1) {
                    param = hash.substring(hash.indexOf("/") + 1, hash.length);
                    hash = hash.substring(0, hash.indexOf("/"));
                }
                //是否需要校验安全密码
                if (win.menuPass && win.menuPass[hash] == 2 && !win.pass2 && hash.indexOf("Password2") == -1 && hash.indexOf("Password3") == -1) {
                    var url = "#Password2/" + hash;
                    if (param) { url = url + "." + param; }
                    location.href = url;
                    return false;
                }
                //是否需要校验交易密码
                else if (win.menuPass && win.menuPass[hash] == 3 && !win.pass3 && hash.indexOf("Password2") == -1 && hash.indexOf("Password3") == -1) {
                    var url = "#Password3/" + hash;
                    if (param) { url = url + "." + param; }
                    location.href = url;
                    return false;
                }
            },
        })

        //ztree设置
        var setting = {
            data: {
                simpleData: {
                    enable : true,
                    idKey : "id", 	              // id编号字段名
                    pIdKey: "parentResourceId",   // 父id编号字段名
                    rootPId : null
                },
                key: {
                    name: "resourceName"    //显示名称字段名
                }
            },
            view: {
                showLine: false,            //不显示连接线
                dblClickExpand :false       //禁用双击展开
            },
            callback: {
                onClick: function (e, treeId, treeNode) {
                    if (treeNode.isParent) {
                        zTree.expandNode(treeNode);
                    } else {
                        var url = (treeNode.curl + "").replace("Admin/", "");
                        location.href = "#" + url;
                    }
                }
            }
        };

        var zTree = null;
        var zNodes = [];
        util.AjaxPostNotLoadding("ManageWeb/GetBaseData", {}, function (result) {
            if (result.status == "fail") {
                util.showErrMsg(result.msg);
                return;
            }
            var baseSet = result.map.baseSet;

            //省市区数据
            win.areaMap = result.map["area"];
            if (areaMap) {
                win.areaData = areaMap[0];
                for (var i = 0; i < win.areaData.length; i++) {
                    var dto = win.areaData[i];
                    if (areaMap[dto.id]) {
                        dto["childs"] = areaMap[dto.id];
                        for (var j = 0; j < dto["childs"].length; j++) {
                            var child = dto["childs"][j];
                            if (areaMap[child.id]) {
                                child["childs"] = areaMap[child.id];
                            }
                        }
                    }
                }
            }

            //消息提醒
            var notic = result.map.notic;
            if (notic && notic.length > 0) {
                var html = "<table>";
                for (var i = 0; i < notic.length;i++){
                    var o = notic[i];
                    html += "<tr><td><a href='"+o.url+"'>"+o.msg+"</a></td></tr>";
                }
                html += "</table>";
                $.messager.show({
                    title: '消息提醒',
                    msg: html,
                    height:200,
                    timeout: 60000,
                    showType: 'slide'
                });
                $("#newMsgAudio")[0].play();
            }

            //网站名称
            $(document).attr("title", baseSet.sitename + "-后台管理");

            var cacheData = result.map.cacheData;
            win.cacheList = cacheData; //list形式缓存
            //map形式缓存
            if (cacheData != null) {
                win.cacheMap = {};
                for (name in cacheData) {
                    win.cacheMap[name] = {};
                    var list = cacheData[name];
                    if (list != null && list.length > 0) {
                        for (var i = 0; i < list.length; i++) {
                            var obj = list[i];
                            win.cacheMap[name][obj.id] = obj.name;
                        }
                    }
                }
            }

            //初始化菜单start
            util.AjaxPostNotLoadding("ManageWeb/GetMenu", { parentId: '100' }, function (result) {
                if (result.status == "success") {
                    win.menuPass = {};
                    for (var i = 0; i < result.list.length; i++) {

                        var node = result.list[i];
                        if (node.isMenu == "N") {
                            var url = (node.curl + "").replace("Admin/", "");
                            win.menuPass[url] = node.pass;
                            //注册路由
                            router.toUrl(url, url + ".js");
                            router.toUrl(url + "/:id", url + ".js");
                            router.toUrl(url + "/:id1/:id2", url + ".js");
                        }
                        if (node.isShow == 0) {
                            zNodes.push(node);
                        }
                    }
                    zTree = $.fn.zTree.init($("#menuTree"), setting, zNodes);
                }
            });
            //初始化菜单end
        })

    });


})(window);
