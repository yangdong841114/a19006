
define(['text!Currency.html', 'jquery'], function (Currency, $) {

    var controller = function (name) {

        //切换选项卡
        changeTab = function (index) {
            $("#tabLi1").removeClass("active");
            $("#tabLi2").removeClass("active");
            $("#tabDiv1").css("display", "none");
            $("#tabDiv2").css("display", "none");
            $("#tabDiv" + index).css("display", "block");
            $("#tabLi" + index).addClass("active");
        }

        //设置标题
        $("#title").html("奖金查询")
        appView.html(Currency);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //跳转到获奖明细界面
        toCurrenctyList = function (addDate) {
            location.href = '#CurrencyUser/' + addDate;
        }

        //获取汇总的默认数据
        setTotalDefault = function () {
            return '<li><div class="lead" style="background:#21a1eb;"><h5>所有奖项合计</h5><span>0</span></div>' +
                                  '<dl><dt>报单费</dt><dd>0</dd></dl><dl><dt>直推奖</dt><dd>0</dd></dl>' +
                                  '<dl><dt>层碰奖</dt><dd>0</dd></dl><dl><dt>对碰奖</dt><dd>0</dd></dl>' +
                                  '<dl><dt>管理奖</dt><dd>0</dd></dl><dl><dt>见点奖</dt><dd>0</dd></dl>' +
                                  '<dl><dt>级差奖</dt><dd>0</dd></dl><dl><dt>应发</dt><dd>0</dd></dl>' +
                                  '<dl><dt>所得税</dt><dd>0</dd></dl><dl><dt>管理费</dt><dd>0</dd></dl>' +
                                  '<dl><dt>复消账户</dt><dd>0</dd></dl><dl><dt>实发</dt><dd>0</dd></dl>' +
                                  '</div></li>';
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#CurrencyDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Currency/GetAllSumCurrency", param, me,
                    function (rows, footers) {
                        var footer = footers[0];
                        var totleHtml = "";
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addDate"] = utils.changeDateFormat(rows[i]["addDate"], 'date');
                            rows[i]["cat1"] = rows[i]["cat1"].toFixed(2);
                            rows[i]["cat2"] = rows[i]["cat2"].toFixed(2);
                            rows[i]["cat3"] = rows[i]["cat3"].toFixed(2);
                            rows[i]["cat4"] = rows[i]["cat4"].toFixed(2);
                            rows[i]["cat5"] = rows[i]["cat5"].toFixed(2);
                            rows[i]["cat6"] = rows[i]["cat6"].toFixed(2);
                            rows[i]["cat7"] = rows[i]["cat7"].toFixed(2);
                            rows[i]["yf"] = rows[i]["yf"].toFixed(2);
                            rows[i]["fee1"] = rows[i]["fee1"].toFixed(2);
                            rows[i]["fee2"] = rows[i]["fee2"].toFixed(2);
                            rows[i]["fee3"] = rows[i]["fee3"].toFixed(2);
                            rows[i]["sf"] = rows[i]["sf"].toFixed(2);

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addDate + '</time><span class="sum">+' + dto.sf + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo"><div class="btnbox"><ul class="tga1">' +
                            '<li><button class="sdelbtn" onclick=\'toCurrenctyList("' + dto.addDate + '")\'>获奖明细</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>结算日期</dt><dd>' + dto.addDate + '</dd></dl><dl><dt>报单费</dt><dd>' + dto.cat1 + '</dd></dl>' +
                            '<dl><dt>直推奖</dt><dd>' + dto.cat2 + '</dd></dl><dl><dt>层碰奖</dt><dd>' + dto.cat3 + '</dd></dl>' +
                            '<dl><dt>对碰奖</dt><dd>' + dto.cat4 + '</dd></dl><dl><dt>管理奖</dt><dd>' + dto.cat5 + '</dd></dl>' +
                            '<dl><dt>见点奖</dt><dd>' + dto.cat6 + '</dd></dl><dl><dt>级差奖</dt><dd>' + dto.cat7 + '</dd></dl>' +
                            '<dl><dt>应发</dt><dd>' + dto.yf + '</dd></dl><dl><dt>所得税</dt><dd>' + dto.fee1 + '</dd></dl>' +
                            '<dl><dt>管理费</dt><dd>' + dto.fee2 + '</dd></dl><dl><dt>复消账户</dt><dd>' + dto.fee3 + '</dd></dl>' +
                            '<dl><dt>实发</dt><dd>' + dto.sf + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#CurrencyItemList").append(html);
                        totleHtml += '<li><div class="lead" style="background:#21a1eb;"><h5>所有奖项合计</h5><span>' + footer.yf + '</span></div>' +
                                  '<dl><dt>报单费</dt><dd>' + footer.cat1 + '</dd></dl><dl><dt>直推奖</dt><dd>' + footer.cat2 + '</dd></dl>' +
                                  '<dl><dt>层碰奖</dt><dd>' + footer.cat3 + '</dd></dl><dl><dt>对碰奖</dt><dd>' + footer.cat4 + '</dd></dl>' +
                                  '<dl><dt>管理奖</dt><dd>' + footer.cat5 + '</dd></dl><dl><dt>见点奖</dt><dd>' + footer.cat6 + '</dd></dl>' +
                                  '<dl><dt>级差奖</dt><dd>' + footer.cat7 + '</dd></dl><dl><dt>应发</dt><dd>' + footer.yf + '</dd></dl>' +
                                  '<dl><dt>所得税</dt><dd>' + footer.fee1 + '</dd></dl><dl><dt>管理费</dt><dd>' + footer.fee2 + '</dd></dl>' +
                                  '<dl><dt>复消账户</dt><dd>' + footer.fee3 + '</dd></dl><dl><dt>实发</dt><dd>' + footer.sf + '</dd></dl>' +
                                  '</div></li>';
                        $("#footerUl").html(totleHtml);
                    }, function () {
                        $("#footerUl").html(setTotalDefault());
                        $("#CurrencyItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#CurrencyItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});