
define(['text!MemberAdmin.html', 'jquery'], function (MemberAdmin, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("管理员设置")
        var atp = "N";

        //清空数据按钮
        clearData = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    dom.prev().val("");
                });
            });
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = null;

        //删除
        deleteAdmin = function (id) {
            $("#prompTitle").html("确定删除该管理员吗？");
            $("#prompCont").empty();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberAdmin/Delete", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //编辑
        editAdmin = function (id, userId, userName) {
            $("#prompTitle").html("编辑管理员");
            var html = "";
            html += '<input type="hidden" id="id" value="' + id + '"/>' +
            '<dl><dt>管理员编号</dt><dd><input type="text" required="required" class="entrytxt" id="userId" value="' + userId + '" placeholder="请输入管理员编号" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>姓名</dt><dd><input type="text" required="required" class="entrytxt" id="userName" value="' + userName + '" placeholder="请输入姓名" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
            if (atp != "N") {
                html += '<dl><dt>登录密码</dt><dd><input type="password" required="required" class="entrytxt" id="password" placeholder="请输入登录密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                '<dl><dt>确认登录密码</dt><dd><input type="password" required="required" class="entrytxt" id="passwordRe" placeholder="请输入确认登录密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                '<dl><dt>安全密码</dt><dd><input type="password" required="required" class="entrytxt" id="passOpen" placeholder="请输入安全密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                '<dl><dt>确认安全密码</dt><dd><input type="password" required="required" class="entrytxt" id="passOpenRe" placeholder="请输入确认安全密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                '<dl><dt>交易密码</dt><dd><input type="password" required="required" class="entrytxt" id="threepass" placeholder="请输入交易密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                '<dl><dt>确认交易密码</dt><dd><input type="password" required="required" class="entrytxt" id="threepassRe" placeholder="请输入确认交易密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
            }
            $("#prompCont").empty();
            $("#prompCont").html(html);
            clearData();
            $("#sureBtn").html("确定提交")
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#userId").val() == 0) {
                    utils.showErrMsg("请输入管理员编号");
                } else if ($("#userName").val() == 0) {
                    utils.showErrMsg("请输入姓名");
                } else if (atp != "N" && $("#password").val()!=0 && $("#password").val() != $("#passwordRe").val()) {
                    utils.showErrMsg("登录密码与确认登录密码不一致");
                } else if (atp != "N" && $("#passOpen").val() != 0 && $("#passOpen").val() != $("#passOpenRe").val()) {
                    utils.showErrMsg("安全密码与确认安全密码不一致");
                } else if (atp != "N" && $("#threepass").val() != 0 && $("#threepass").val() != $("#threepassRe").val()) {
                    utils.showErrMsg("交易密码与确认交易密码不一致");
                } else {
                    var data = {id:$("#id").val(), userId: $("#userId").val(), userName: $("#userName").val(), password: $("#password").val(), passOpen: $("#passOpen").val(), threepass: $("#threepass").val() }
                    utils.AjaxPost("/Admin/MemberAdmin/SaveOrUpdate", data, function (result) {
                        utils.showOrHiddenPromp();
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg(result.msg);
                            searchMethod();
                        }
                    });
                }
            })
            utils.showOrHiddenPromp();
        }

        //初始化加载数据
        utils.AjaxPostNotLoadding("/Admin/MemberAdmin/GetAdminType", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                atp = result.msg;
                appView.html(MemberAdmin);

                //清空查询条件按钮
                $("#clearQueryBtn").bind("click", function () {
                    utils.clearQueryParam();
                })

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //添加按钮
                $("#addAdminBtn").bind("click", function () {
                    $("#prompTitle").html("添加管理员");
                    var html = "";
                    html += '<input type="hidden" id="id"/>' +
                    '<dl><dt>管理员编号</dt><dd><input type="text" required="required" class="entrytxt" id="userId" placeholder="请输入管理员编号" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                    '<dl><dt>姓名</dt><dd><input type="text" required="required" class="entrytxt" id="userName" placeholder="请输入姓名" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                    '<dl><dt>登录密码</dt><dd><input type="password" required="required" class="entrytxt" id="password" placeholder="请输入登录密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                    '<dl><dt>确认登录密码</dt><dd><input type="password" required="required" class="entrytxt" id="passwordRe" placeholder="请输入确认登录密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                    '<dl><dt>安全密码</dt><dd><input type="password" required="required" class="entrytxt" id="passOpen" placeholder="请输入安全密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                    '<dl><dt>确认安全密码</dt><dd><input type="password" required="required" class="entrytxt" id="passOpenRe" placeholder="请输入确认安全密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                    '<dl><dt>交易密码</dt><dd><input type="password" required="required" class="entrytxt" id="threepass" placeholder="请输入交易密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                    '<dl><dt>确认交易密码</dt><dd><input type="password" required="required" class="entrytxt" id="threepassRe" placeholder="请输入确认交易密码" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
                    $("#prompCont").empty();
                    $("#prompCont").html(html);
                    clearData();
                    $("#sureBtn").html("确定提交")
                    $("#sureBtn").unbind();
                    $("#sureBtn").bind("click", function () {
                        if ($("#userId").val() == 0) {
                            utils.showErrMsg("请输入管理员编号");
                        } else if ($("#userName").val() == 0) {
                            utils.showErrMsg("请输入姓名");
                        } else if ($("#password").val() == 0) {
                            utils.showErrMsg("请输入登录密码");
                        } else if ($("#password").val() != $("#passwordRe").val()) {
                            utils.showErrMsg("登录密码与确认登录密码不一致");
                        } else if ($("#passOpen").val() == 0) {
                            utils.showErrMsg("请输入安全密码");
                        } else if ($("#passOpen").val() != $("#passOpenRe").val()) {
                            utils.showErrMsg("安全密码与确认安全密码不一致");
                        } else if ($("#threepass").val() == 0) {
                            utils.showErrMsg("请输入交易密码");
                        } else if ($("#threepass").val() != $("#threepassRe").val()) {
                            utils.showErrMsg("交易密码与确认交易密码不一致");
                        } else {
                            var data = { userId: $("#userId").val(), userName: $("#userName").val(), password: $("#password").val(), passOpen: $("#passOpen").val(), threepass: $("#threepass").val() }
                            utils.AjaxPost("/Admin/MemberAdmin/SaveOrUpdate", data, function (result) {
                                utils.showOrHiddenPromp();
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg(result.msg);
                                    searchMethod();
                                }
                            });
                        }
                    })
                    utils.showOrHiddenPromp();
                })

                dropload = $('#MemberAdmindatalist').dropload({
                    scrollArea: window,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData("/Admin/MemberAdmin/GetListPage", param, me,
                            function (rows, footers) {
                                var html = "";
                                for (var i = 0; i < rows.length; i++) {
                                    rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                                    var dto = rows[i];
                                    dto.nickName = dto.nickName ? dto.nickName : "";
                                    html += '<li>' +
                                            '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                                    html += '<i class="fa fa-angle-right"></i></div>' +
                                    '<div class="allinfo">' +
                                    '<div class="btnbox"><ul class="tga2">' +
                                    '<li><button class="seditbtn" onclick=\'editAdmin(' + dto.id + ',"' + dto.userId + '","' + dto.userName + '")\'>编辑</button></li>' +
                                    '<li><button class="sdelbtn" onclick=\'deleteAdmin(' + dto.id + ')\'>删除</button></li>' +
                                    '</ul></div>' +
                                    '<dl><dt>管理员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                    '<dl><dt>注册日期</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>拥有角色</dt><dd>' + dto.nickName + '</dd></dl>' +
                                    '</div></li>';
                                }
                                $("#MemberAdminitemList").append(html);
                            }, function () {
                                $("#MemberAdminitemList").append('<p class="dropload-noData">暂无数据</p>');
                            });
                    }
                });

                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    $("#MemberAdminitemList").empty();
                    param["userId"] = $("#userId2").val();
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }

                //查询按钮
                $("#searchBtn").on("click", function () {
                    searchMethod();
                })

            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});