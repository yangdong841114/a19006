
define(['text!UMemberInfo.html', 'jquery'], function (UMemberInfo, $) {

    var controller = function (memberId) {
       
        //设置标题
        $("#title").html("会员资料")
        var pmap = null;

        //设置表单默认数据
        setDefaultFormValue = function (dto) {
            $("#id").val(dto.id);
            $("span").each(function (index, ele) {
                if (dto[this.id]) {
                   $(this).html(dto[this.id]);
                }
            });
            $("p").each(function (index, ele) {
                if (dto[this.id]) {
                    if (this.id == "uLevel") {
                        $(this).html(cacheMap["ulevel"][dto[this.id + ""]]);
                    }
                    else {
                        $(this).html(dto[this.id]);
                    }

                }
            });
            $("input").each(function (index, ele) {
                if (dto[this.id]) {
                  $(this).val(dto[this.id]);
                }
            });
            $("#treePlace").html(dto["treePlace"] == 0 ? "左区" : "右区");
        }
        var data = {};
        if (memberId && memberId > 0) { data["memberId"] = memberId; }

        var inputs = undefined;

        var indexChecked = [0, 0, 0];

        //打开时确认选中数据
        initPosition = function(e){
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //加载会员信息
        utils.AjaxPostNotLoadding("/User/UMemberInfo/GetModel", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMemberInfo);

                //初始化银行帐号下拉框
                var bankLit = cacheList["UserBank"];
                utils.InitMobileSelect('#bankName', '开户行', bankLit, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                    $("#bankName").val(data[0].name);
                });

                //省市区选择
                var proSet = utils.InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                var dto = result.result;
                //初始表单默认值
                setDefaultFormValue(dto);

                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                            $("#province").val("");
                            $("#city").val("");
                            $("#area").val("");
                        }
                        dom.prev().val("");
                    });
                });


                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var checked = true;
                    //数据校验
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (jdom.attr("emptymsg") && jdom.val() == 0) {
                            utils.showErrMsg(jdom.attr("emptymsg"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                        if (jdom.attr("id") == 'bankCard') {
                            var card = jdom.val();
                            var reg = /^(\d{16,19})$/g;
                            if (!reg.test(jdom.val())) {
                                utils.showErrMsg("银行卡号位数必须为16-19位！");
                                jdom.focus();
                                checked = false;
                                return false;
                            }
                        }
                    });
                    if (checked) {
                        var fs = {};
                        $("#lemeinfo input").each(function () {
                            fs[this.id] = $(this).val();
                        });
                        utils.AjaxPost("/User/UMemberInfo/UpdateMember", fs, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});