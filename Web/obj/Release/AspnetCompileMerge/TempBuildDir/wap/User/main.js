
define(['text!main.html', 'jquery'], function (main, $) {

    var controller = function (name) {
        ////设置标题
        //$("#center").panel("setTitle","会员注册");


        ShowTakeCash = function () {
            if ($("#bankName").val() == "" || $("#bankCard").val() == "" || $("#bankUser").val() == "" || $("#bankAddress").val() == "") {
                document.getElementById("waitThing_Txdz").style.display = "block";
                document.getElementById("btnbox").style.display = "none";
                $("#prompTitle").html("提示信息");
                utils.showOrHiddenPromp();
            } else {
                location.href = "#UTakeCash";
            }
        }
        //截取字符串
        sub = function (str, n) {
            var r = /[^\x00-\xff]/g;
            if (str.replace(r, "mm").length <= n) { return str; }
            var m = Math.floor(n / 2);
            for (var i = m; i < str.length; i++) {
                if (str.substr(0, i).replace(r, "mm").length >= n) {
                    return str.substr(0, i) + "...";
                }
            }
            return str;
        }

        //初始化滚动图片
        InitHImgs = function () {
            $(".main_visual").hover(function () {
                $("#btn_prev,#btn_next").fadeIn()
            }, function () {
                $("#btn_prev,#btn_next").fadeOut()
            });

            $dragBln = false;

            $(".main_image").touchSlider({
                flexible: true,
                speed: 400,
                btn_prev: $("#btn_prev"),
                btn_next: $("#btn_next"),
                paging: $(".flicking_con a"),
                counter: function (e) {
                    $(".flicking_con a").removeClass("on").eq(e.current - 1).addClass("on");
                }
            });

            $(".main_image").bind("mousedown", function () {
                $dragBln = false;
            });

            $(".main_image").bind("dragstart", function () {
                $dragBln = true;
            });

            $(".main_image a").click(function () {
                if ($dragBln) {
                    return false;
                }
            });

            timer = setInterval(function () {
                $("#btn_next").click();
            }, 10000);

            $(".main_visual").hover(function () {
                clearInterval(timer);
            }, function () {
                timer = setInterval(function () {
                    $("#btn_next").click();
                }, 10000);
            });

            $(".main_image").bind("touchstart", function () {
                clearInterval(timer);
            }).bind("touchend", function () {
                timer = setInterval(function () {
                    $("#btn_next").click();
                }, 10000);
            });
        }

        utils.AjaxPostNotLoadding("/User/UserWeb/GetMainData", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //退出按钮
                $("#exitSystem").bind('click', function () {
                    document.getElementById("waitThing_Txdz").style.display = "none";
                    document.getElementById("btnbox").style.display = "block";
                    $("#prompTitle").html("您确定退出登录吗？");
                    $("#sureBtn").unbind();
                    $("#sureBtn").bind("click", function () {
                        location.href = "/Home/exitUserLogin";
                    });
                    utils.showOrHiddenPromp();
                });

                var map = result.map;
                if (map) {
                    var user = map.user;
                    var account = map.account;
                    $("#agentDz").html(account.agentDz.toFixed(2));        //电子币
                    $("#agentJj").html(account.agentJj.toFixed(2));        //奖金币
                    $("#agentGw").html(account.agentGw.toFixed(2));        //购物币
                    $("#agentFt").html(account.agentFt.toFixed(2));        //复投币
                    $("#bankName").val(user.bankName);
                    $("#bankCard").val(user.bankCard);
                    $("#bankUser").val(user.bankUser);
                    $("#bankAddress").val(user.bankAddress);
                    //$("#agentTotal").html(account.agentTotal.toFixed(2));  //累计奖金
                    $("#userId").html(user.userId);
                    //$("#userId2").html(user.userId);
                    $("#uLevel").html(cacheMap["ulevel"][user.uLevel]);
                    //$("#rLevel").html(cacheMap["rLevel"][user.rLevel]);
                    var baseset = map.baseSet;
                    //广告图
                    var banners = map.banners;
                    if (banners && banners.length > 0) {
                        var banHtml = "";
                        for (var i = 0; i < banners.length; i++) {
                            banHtml += '<li><img src="' + banners[i].imgUrl + '" /></li>';
                        }
                        $("#slides").html(banHtml);
                    }


                    //文章
                    var newsList = map.newsList;
                    var hh = "";
                    if (newsList && newsList.length > 0) {
                        for (var i = 0; i < newsList.length; i++) {
                            var n = newsList[i];
                            hh += '<li><a href="#UArticDetail/' + n.id + '">' + sub(n.title, 43) + '</a><time>' + utils.changeDateFormat(n.addTime) + '</time></li>';
                            //hh += '<li><a href="#UArticleDetail/'+n.id+'" title="'+ n.title +'">' + sub(n.title,43) + '</a><span>' + utils.changeDateFormat(n.addTime) + '</span></li>';
                        }
                        $("#newsItem").html(hh);
                    }

                    ////推广链接
                    //var siteUrl = map.siteUrl;
                    //$("#siteUrl").attr("href", siteUrl);
                    //$("#siteUrl").html(siteUrl);

                    ////复制链接
                    //var clip = new ZeroClipboard(document.getElementById("copyUrl"));

                    ////二维码
                    //var qrcode = map.qrcode;
                    //$("#qrcode").attr("src", "/UpLoad/qrcode/" + qrcode);
                }

                //滚动广告图
                InitHImgs();


            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});