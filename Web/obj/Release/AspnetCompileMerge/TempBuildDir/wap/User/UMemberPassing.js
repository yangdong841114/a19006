
define(['text!UMemberPassing.html', 'jquery'], function (UMemberPassing, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("待开通会员")
        appView.html(UMemberPassing);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //编辑会员
        toEditMember = function (uid) {
            location.href = "#UMemberInfo/" + uid;
        };

        //显示提示框
        showPromptbg = function (uid, dkhybh, dkhymc, dkzcrq, dkhyjb, dkzcje, title, btnName, callback) {
            $("#uid").val(uid);
            $("#dkhybh").html(dkhybh);
            $("#dkhymc").html(dkhymc);
            $("#dkzcrq").html(dkzcrq);
            $("#dkhyjb").html(dkhyjb);
            $("#dkzcje").html(dkzcje);
            $("#prompTitle").html(title);
            $("#sureBtn").html(btnName);
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                callback(uid);
            });
            utils.showOrHiddenPromp();
        };

        //开通会员
        openMember = function (uid) {
            utils.AjaxPost("/User/UMemberPassing/OpenMember", { id: uid, flag: 0 }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showOrHiddenPromp();
                    utils.showSuccessMsg(result.msg);
                    searchMethod();
                }
            });
        }

        //确认开通会员
        confirmOpenMember = function (uid, dkhybh, dkhymc, dkzcrq, dkhyjb, dkzcje) {
            showPromptbg(uid, dkhybh, dkhymc, dkzcrq, dkhyjb, dkzcje, "开通会员确认", "确定开通", openMember);
        };

        //删除会员
        deleteMember = function (uid) {
            utils.AjaxPost("/User/UMemberPassing/Delete", { id: uid }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showOrHiddenPromp();
                    utils.showSuccessMsg(result.msg);
                    searchMethod();
                }
            });
        }

        //确认删除会员
        confirmDelMember = function (uid, dkhybh, dkhymc, dkzcrq, dkhyjb, dkzcje) {
            showPromptbg(uid, dkhybh, dkhymc, dkzcrq, dkhyjb, dkzcje, "删除会员确认", "确定删除", deleteMember);
        };

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UMemberPassingdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UMemberPassing/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["passTime"] = utils.changeDateFormat(rows[i]["passTime"]);
                            rows[i].uLevel = cacheMap["ulevel"][rows[i].uLevel];
                            rows[i].isLock = rows[i].isLock == 0 ? "否" : "是";

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '注册金额：<span class="sum">' + dto.regMoney + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga3">' +
                            '<li><button class="seditbtn" onclick=\'toEditMember('+dto.id+')\'>编辑</button></li>' +
                            '<li><button class="smallbtn" onclick=\'confirmOpenMember(' + dto.id + ',"' + dto.userId + '","' + dto.userName + '","' + dto.addTime + '","' + dto.uLevel + '","' + dto.regMoney + '")\'>开通</button></li>' +
                            '<li><button class="sdelbtn" onclick=\'confirmDelMember(' + dto.id + ',"' + dto.userId + '","' + dto.userName + '","' + dto.addTime + '","' + dto.uLevel + '","' + dto.regMoney + '")\'>删除</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>注册日期</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl>' +
                            '<dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl><dl><dt>接点人编号</dt><dd>' + dto.fatherName + '</dd></dl>' +
                            '<dl><dt>会员级别</dt><dd>' + dto.uLevel + '</dd></dl><dl><dt>注册金额</dt><dd>' + dto.regMoney + '</dd></dl>' +
                            '<dl><dt>联系电话</dt><dd>' + dto.phone + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#UMemberPassingitemList").append(html);
                    }, function () {
                        $("#UMemberPassingitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UMemberPassingitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["userId"] = $("#userId").val();
            param["userName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })


        controller.onRouteChange = function () {
        };
    };

    return controller;
});