
define(['text!UEpMyBuyer.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UEpMyBuyer, $) {

    var controller = function (name) {

        var statusDto = { 0: "待买家付款", 1: "待卖家收款", 2: "已完成", 3: "已取消" };

        appView.html(UEpMyBuyer);

        //导出
        if (LoginUser && LoginUser == "system") {
            document.getElementById("export").style.display = "block";
        }

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            if (ipt.id == "batchPayBtn") {
                if (!LoginUser || LoginUser != "system") {
                    $("#batchPayBtn").attr("display", "none");
                } else {
                    $(ipt).linkbutton({});
                }
            } else {
                $(ipt).linkbutton({});
            }

        });

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化下拉框
        $("#flag").empty();
        $("#flag").append("<option value='-1'>--全部--</option>");
        $("#flag").append("<option value='0'>待买家付款</option>");
        $("#flag").append("<option value='1'>待卖家收款</option>");
        $("#flag").append("<option value='2'>已完成</option>");
        $("#flag").append("<option value='3'>已取消</option>");

        //批量付款按钮
        $("#batchPayBtn").bind("click", function () {
            utils.confirm("您确定要付款吗？", function () { 
                var rows = grid.datagrid("getSelections");
                if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要付款的购买记录！"); }
                else {
                    var data = {};
                    var checked = true;
                    for (var i = 0; i < rows.length; i++) {
                        var row = rows[i];
                        if (row.flag != 0) {
                            checked = false;
                            utils.showErrMsg("购买记录【" + row.number + "】状态不是【待买家付款】！");
                            return;
                        }
                        data["list[" + i + "].id"] = row.id;
                    }
                    if (checked) {
                        utils.AjaxPost("UEpMyBuyer/BatchPayMoney", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("批量付款成功");
                                //重刷grid
                                queryGrid();

                            }
                        });
                    }
                }
            })
        })


        //初始化表格
        var grid = utils.newGrid("dg", {
            title: '我的买单',
            showFooter: true,
            singleSelect: false,
            columns: [[
                { field: 'ck', title: '文本', checkbox: true, },
                { field: 'number', title: '买入单号', width: '170' },
                { field: 'buyNum', title: '买入数量', width: '70' },
                { field: 'addTime', title: '购买日期', width: '125' },
                { field: 'snumber', title: '挂卖单号', width: '170' },
                { field: 'payMoney', title: '应付金额', width: '70' },
                { field: 'status', title: '状态', width: '80' },
                { field: 'suserId', title: '卖家编号', width: '100' },
                { field: 'phone', title: '手机', width: '100' },
                { field: 'qq', title: 'QQ', width: '100' },
                { field: 'bankName', title: '开户行', width: '100' },
                { field: 'bankCard', title: '银行卡号', width: '140' },
                { field: 'bankUser', title: '开户名', width: '100' },
                { field: 'bankAddress', title: '开户支行', width: '100' },
                {
                    field: '_operate', title: '操作', width: '120', align: 'center', formatter: function (val, row, index) {
                        if (row.flag == 0) {
                            return '&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildTo" >取消买单</a>&nbsp;&nbsp;' +
                                '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridStatusTo" >确认付款</a>';
                        } else {
                            return "";
                        }
                    }
                }
            ]],
            url: "UEpMyBuyer/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["status"] = statusDto[data.rows[i]["flag"]];
                }
            }
            return data;
        }, function () {
            //行取消买单
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定取消买单吗？", function () {
                        utils.AjaxPost("UEpMyBuyer/SaveCancel", {id:dataId}, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                queryGrid();

                            }
                        });
                    });
                }
            });
            //行确认付款
            $(".gridStatusTo").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定已付款吗？", function () {
                        utils.AjaxPost("UEpMyBuyer/PayMoney", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                queryGrid();

                            }
                        });
                    });

                }
            });
          
        });

        //查询grid
        queryGrid = function () {
            var objs = $("#UEpMyBuyerQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })
     

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});