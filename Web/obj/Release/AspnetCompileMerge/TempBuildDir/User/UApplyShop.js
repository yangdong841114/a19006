
define(['text!UApplyShop.html', 'jquery', 'j_easyui', 'zui'], function (UApplyShop, $) {

    var controller = function (name) {
        
        
        var dto = undefined;

        //设置显示内容
        setShowContent = function (dto) {
            if (dto.isAgent == 0) {
                $("#shopName").html(dto.userId);
                $("#mmy").html(dto.regAgentmoney);
                $("#ingDiv").css("display", "none");
            } else if (dto.isAgent == 1) {
                $("#shopName").html(dto.agentName);
                $("#ingDiv").css("display", "block");
                $("#btnDiv").css("display", "none");
                $("#myDiv").css("display", "none");
            } else {
                $("#shopName").html(dto.agentName);
                $("#ingDiv").css("display", "none");
                $("#btnDiv").css("display", "none");
                $("#myDiv").css("display", "none");
            }
        }

        //加载默认密码
        utils.AjaxPostNotLoadding("UApplyShop/GetDefaultMsg", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UApplyShop);
                dto = result.result;
                
                setShowContent(dto);

                //提交申请按钮
                $("#saveBtn").on('click', function () {
                    utils.confirm("确认申请报单中心吗？", function () {
                        if (dto && dto.isAgent == 0) {
                            utils.AjaxPost("UApplyShop/ApplyShop", {}, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    dto = result.result;
                                    setShowContent(dto)
                                    utils.showSuccessMsg("申请成功，请等待管理员审核！");
                                }
                            });
                        } else {
                            utils.showErrMsg("申请失败，无法确认当前登录用户");
                        }
                    })
                });
            }
        });


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#regContent').dialog("destroy");
        };
    };

    return controller;
});