
define(['text!UArticle.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UArticle, $) {

    var controller = function (name) {

        appView.html(UArticle);

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            title:'公告列表',
            rownumbers:false,
            showFooter: true,
            columns: [[
                {
                    field: '_downTitle', title: '主题', width: '80%', formatter: function (val, row, index) {
                        return '<a href="#UArticleDetail/' + row.id + '"  class="titleOpen" >' + row.title + '</a>';
                    }
                },
             { field: 'addTime', title: '发布日期', width: '20%' },
            ]],
            url: "UArticle/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        })



        controller.onRouteChange = function () {
        };
    };

    return controller;
});