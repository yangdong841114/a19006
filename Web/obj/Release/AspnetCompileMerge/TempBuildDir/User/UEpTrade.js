
define(['text!UEpTrade.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UEpTrade, $) {

    var controller = function (name) {

        appView.html(UEpTrade);

        var batchGrid = null;
        //导出
        if (LoginUser && LoginUser == "system") {
            document.getElementById("export").style.display = "block";

            //初始化批量购买表格
            batchGrid = utils.newGrid("dialogGird", {
                title: '勾选的挂卖记录',
                showFooter: false,
                pagination:false,
                columns: [[
                 { field: 'number', title: '挂卖单号', width: '170' },
                 { field: 'userId', title: '卖家编号', width: '100' },
                 {
                     field: 'buyNum', title: '购买数量', width: '100', formatter: function (val, row, index) {
                         return '<input type="text" class="form-control" id="buy' + row.id + '"  style="width:98%;color:red;height:23px;" value="' + row.buyNum + '"/>';
                     }
                 },
                 //{ field: 'buyNum', title: '购买数量', width: '90' },
                 { field: 'waitNum', title: '待出售数量', width: '90' },
                 { field: 'addTime', title: '挂卖日期', width: '130' },
                ]],
            });
        }

        

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            if (ipt.id == "batchBuyBtn") {
                if (!LoginUser || LoginUser != "system") {
                    $("#batchBuyBtn").attr("display", "none");
                } else {
                    $(ipt).linkbutton({});
                }
            } else {
                $(ipt).linkbutton({});
            }
            
        });

        //初始化弹出框
        $('#dlg').dialog({
            title: '我要购买',
            width: 500,
            height: 220,
            cache: false,
            modal: true
        });

        $('#dlg2').dialog({
            title: '批量购买',
            width: 644,
            height: 475,
            cache: false,
            modal: true
        });

        //清除错误样式
        $(".UEpTradeAdd").each(function () {
            var dom = $(this);
            dom.on("focus", function () {
                dom.removeClass("inputError");
            })
        });

        $("#buyNum").bind("blur", function () {
            var val = $("#buyNum").val();
            if (!isNaN(val) && val > 0) {
                $("#payMoney").val(val);
            }
        });

        //保存按钮
        $("#saveBtn").bind("click", function () {
            var checked = true;
            var val = $("#buyNum").val();
            if (val ==0 || isNaN(val) || val < 0) {
                $("#buyNum").addClass("inputError");
                checked = false;
            }

            if (checked) {
                var data = { buyNum: val, sid: $("#sid").val() };
                utils.AjaxPost("UEpTrade/SaveRecord", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("操作成功");
                        //关闭弹出框
                        $('#dlg').dialog("close");
                        //重刷grid
                        queryGrid();

                    }
                });
            }
            return false;
        })

        //批量购买保存按钮
        $("#saveBatchBtn").bind("click", function () {

            var rows = batchGrid.datagrid('getRows');
            if (!rows || rows.length == 0) {
                utils.showErrMsg("您还没有勾选任何挂卖记录");
            } else {
                var checked = true
                var data = {};
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    var val = $("#buy" + row.id).val();
                    if (val == 0 || isNaN(val) || val < 0) {
                        utils.showErrMsg("第"+(i+1)+"行的购买数量格式错误");
                        checked = false;
                        return;
                    } else if (row.waitNum < val) {
                        utils.showErrMsg("第" + (i + 1) + "行的购买数量超出待售数量");
                        checked = false;
                        return;
                    }
                    data["list[" + i + "].sid"] = row.id;
                    data["list[" + i + "].buyNum"] = val;
                    data["list[" + i + "].snumber"] = row.number;
                }
                if (checked) {
                    utils.AjaxPost("UEpTrade/SaveRecordBatch", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg("批量购买成功");
                            //关闭弹出框
                            $('#dlg2').dialog("close");
                            //重刷grid
                            queryGrid();

                        }
                    });
                }
            }
        })

        //我要购买按钮
        $("#buyBtn").bind("click", function () {
            var rows = grid.datagrid("getSelections");
            if (rows == null || rows.length==0) { utils.showErrMsg("请选择需要购买的挂卖记录！"); }
            else {
                var obj = rows[0];
                //清空表单数据
                $('#editModalForm').form('reset');
                $("#payMoney").val("");
                $("#sid").val(obj.id);
                $('#dlg').dialog({ title: '购买【'+obj.number+'】' }).dialog("open");
            }
        });

        //批量购买按钮
        $("#batchBuyBtn").bind("click", function () {
            var rows = grid.datagrid("getSelections");
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要购买的挂卖记录！"); }
            else {
                for (var i = 0; i < rows.length; i++) {
                    rows[i]["buyNum"] = rows[i].waitNum;
                }
                batchGrid.datagrid('loadData', rows);
                
                $('#dlg2').dialog("open");
            }
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            title:'挂卖记录',
            showFooter: true,
            singleSelect:false,
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'number', title: '挂卖单号', width: '18%' },
             { field: 'userId', title: '卖家编号', width: '15%' },
             { field: 'waitNum', title: '待出售数量', width: '12%' },
             { field: 'scNum', title: '已出售数量', width: '12%' },
             { field: 'saleNum', title: '挂卖数量', width: '12%' },
             { field: 'addTime', title: '挂卖日期', width: '15%' },
            ]],
            url: "UEpTrade/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        })



        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
            $('#dlg2').dialog("destroy");
        };
    };

    return controller;
});