﻿//      var toolbarOptions = [
//['bold', 'italic', 'underline', 'strike'],        // toggled buttons
//['blockquote', 'code-block'],

//[{ 'header': 1 }, { 'header': 2 }],               // custom button values
//[{ 'list': 'ordered'}, { 'list': 'bullet' }],
//[{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
//[{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
//[{ 'direction': 'rtl' }],                         // text direction

//[{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
//[{ 'header': [1, 2, 3, 4, 5, 6, false] }],

//[{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
//[{ 'font': [] }],
//[{ 'align': [] }],

//['clean']                                         // remove formatting button
//      ];
var quillHelp = {
    toolbarOptions: [['bold', 'italic', 'underline', 'strike'], [{ 'size': ['small', false, 'large', 'huge'] }], [{ 'color': [] }, { 'background': [] }], ['link', 'image'],
          [{ 'align': [] }]],
    quill:null,
    BindQuill:function (obj){
        quillHelp.quill = new Quill("#"+obj.bindId, {
            modules: {
                toolbar: quillHelp.toolbarOptions
            },
            theme: 'snow'
        });
        //quill = new Quill('#editor', {
        //    theme: 'snow'
        //});
        quillHelp.quill.container.firstChild.innerHTML = $("#" + obj.valId).val();
    },
    GetQuillHtml: function () {
        if (quillHelp.quill != null && quillHelp.quill != undefined) {
            return quillHelp.quill.container.firstChild.innerHTML;
        }
        return "";
    }
};
