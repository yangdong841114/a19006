
define(['text!Wealthscheme.html', 'jquery'], function (Wealthscheme, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "财富方案");
        

        utils.AjaxPostNotLoadding("Wealthscheme/InitView", {}, function (result) {
            if (result.status == "success") {
                appView.html(Wealthscheme);
                var cont = "";
                if (result.result) {
                    cont = result.result.content;
                    $("#title").val(result.result.title);
                }

                //初始化编辑器
                $("#editor").height(document.body.offsetHeight - 270);
                var editor = new Quill("#editor", {
                    modules: {
                        toolbar: utils.getEditorToolbar()
                    },
                    theme: 'snow'
                });
                utils.setEditorHtml(editor, cont)

                $("#saveBtn").on("click", function () {
                    if ($("#title").val() == 0) {
                        utils.showErrMsg("请输入主题");
                    }
                    else if (editor.getText()==0) {
                        utils.showErrMsg("请输入内容");
                    } else {
                        utils.AjaxPost("Wealthscheme/SaveByUeditor", { content: utils.getEditorHtml(editor), title: $("#title").val() }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("保存成功");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }
                })

            }
        });

        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});