
define(['text!ShopMemberList.html', 'jquery', 'j_easyui'], function (ShopMemberList, $) {

    var controller = function (shopId) {
        //设置标题
        $("#center").panel("setTitle", "会员列表");
        appView.html(ShopMemberList);

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '100' }]],
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'userName', title: '会员名称',width:'100'},
             { field: 'uLevel', title: '会员级别', width: '80' },
             { field: 'regMoney', title: '注册金额', width: '80' },
             { field: 'bankName', title: '开户行', width: '100' },
             { field: 'bankCard', title: '开户帐号', width:'130'},
             { field: 'bankUser', title: '开户名', width: '100' },
             { field: 'code', title: '身份证号', width: '140' },
             { field: 'phone', title: '联系电话', width: '100' },
             { field: 'address', title: '联系地址', width: '150' },
             { field: 'qq', title: 'QQ', width: '100' },
             { field: 'reName', title: '推荐人编号', width: '80' },
             { field: 'shopName', title: '报单中心', width: '80' },
             { field: 'passTime', title: '开通日期', width: '130' },
             { field: 'byopen', title: '操作人', width: '80' },
             { field: 'isLock', title: '是否冻结', width: '70' },
             { field: 'empty', title: '是否空单', width: '70' },
             { field: 'isPay', title: '是否开通', width: '70' },
             //{
             //    field: '_operate', title: '操作', width: '160', align: 'center', formatter: function (val, row, index) {
             //        return '<a href="javascript:void(0);" name="opera" data-options="text:\'编辑\',memberId:' + row.id + '" class="easyui-linkbutton gridFildEdit" ></a>&nbsp;' +
             //        '<a href="javascript:void(0);" name="opera" data-options="text:\'进入前台\',memberId:' + row.id + '" class="easyui-linkbutton gridFildTo" ></a>&nbsp;' +
             //        '<a href="javascript:void(0);" name="opera" data-options="text:\'发邮件\',memberId:' + row.id + '" class="easyui-linkbutton gridFildEmail" ></a>';
             //}
             //}
            ]],
            url: 'ShopPassed/GetShopMemberList?shopId='+shopId
            
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["passTime"] = utils.changeDateFormat(data.rows[i]["passTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    data.rows[i].empty = data.rows[i].empty == 0 ? "否" : "是";
                    data.rows[i].isLock = data.rows[i].isLock == 0 ? "否" : "是";
                    data.rows[i].isPay = data.rows[i].isPay == 0 ? "否" : "是";
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            if (objs == null) {
                objs = {};
            }
            objs["shopId"] = shopId;
            var url = grid.datagrid("options").url;
            if (!url || url == 0) {
                grid.datagrid("options").url = "ShopPassed/GetShopMemberList";
            }
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        
        

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});