
define(['text!MemberInfo.html', 'jquery', 'j_easyui', 'zui'], function (MemberInfo, $) {

    var controller = function (memberId) {
        //设置标题
        $("#center").panel("setTitle","编辑会员资料");
        
        //初始化数据
        utils.AjaxPostNotLoadding("MemberInfo/GetModel", { memberId: memberId }, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                appView.html(MemberInfo);

                //初始化input text
                $(".easyui-textbox").each(function (i, ipt) {
                    $(ipt).textbox({ labelAlign: 'right', height: '28px', labelWidth: '150px' });
                });

                $(".easyui-passwordbox").each(function (i, ipt) {
                    $(ipt).passwordbox({ labelAlign: 'right', height: '28px', labelWidth: '150px' });
                });

                //初始化下拉框
                $("#uLevel").combobox({ labelAlign: 'right', height: '28px', labelWidth: '150px', data: cacheList["ulevel"], valueField: 'id', textField: 'name', editable: false });
                $("#treePlace").combobox({ labelAlign: 'right', height: '28px', labelWidth: '150px', editable: false });
                $("#bankName").combobox({ labelAlign: 'right', height: '28px', labelWidth: '150px', data: cacheList["ulevel"], valueField: 'name', textField: 'name', editable: false });
                $("#province").combobox({ labelAlign: 'right', height: '28px', labelWidth: '150px', data: areaData, valueField: 'value', textField: 'value', editable: false });
                $("#city").combobox({ labelAlign: 'right', height: '28px', labelWidth: '150px', data: [], valueField: 'value', textField: 'value', editable: false });
                $("#area").combobox({ labelAlign: 'right', height: '28px', labelWidth: '150px', data: [], valueField: 'value', textField: 'value', editable: false });

                $("#province").combobox({
                    onSelect: function (record) {
                        $("#city").combobox("loadData", record.childs);
                        $("#city").combobox("setValue", record.childs[0].value);
                    }
                });

                $("#city").combobox({
                    onSelect: function (record) {
                        var childs = record.childs && record.childs.length > 0 ? record.childs : [];
                        $("#area").combobox("clear");
                        $("#area").combobox("loadData", childs);
                        $("#area").combobox("setValue", childs.length>0?childs[0].value:"");
                    }
                });

                //初始化按钮
                $(".easyui-linkbutton").each(function (i, ipt) {
                    $(ipt).linkbutton({});
                });

                $("#MemberInfoForm").form("load", result.result);

                //保存按钮
                $('#saveBtn').bind('click', function () {
                    $('#MemberInfoForm').form('submit', {
                        url: 'MemberInfo/UpdateMember',
                        onSubmit: function () {
                            var ok = $(this).form('validate');
                            if (ok) {
                                $.messager.progress({
                                    title: '请稍后',
                                    msg: '正在处理中...',
                                    interval: 100,
                                    text: ''
                                });
                            }
                            return ok;
                        },
                        success: function (result) {
                            $.messager.progress("close");
                            var result = eval('(' + result + ')');
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //$('#RegisterForm').form('reset');
                            }
                        }
                    });
                    return false;
                });
            }
        });

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});