
define(['text!messageCenter.html', 'jquery'], function (messageCenter, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("信息管理");
        appView.html(messageCenter);

        var items = menuItem[parentId];
        if (items && items.length > 0) {
            var banHtml = "";
            for (var i = 0; i < items.length; i++) {
                var node = items[i];
                var url = (node.curl + "").replace("Admin/", "");
                if (node.isShow == 0) {
                    banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                }
            }
            $("#messageCenteritemCont").html(banHtml);
        }

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});