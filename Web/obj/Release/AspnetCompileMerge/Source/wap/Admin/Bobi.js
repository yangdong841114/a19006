
define(['text!Bobi.html', 'jquery'], function (Bobi, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("拨比查询")

        utils.AjaxPostNotLoadding("/Admin/Bobi/GetTotalBobi", {}, function (result) {
            if (result.status == 'success') {
                appView.html(Bobi);
                var dto = result.result;
                if (dto) {
                    $("#income").html(dto.income);
                    $("#outlay").html(dto.outlay);
                    $("#profit").html(dto.profit);
                    $("#bili").html(dto.bili);
                }

                //查询参数
                this.param = utils.getPageData();

                var dropload = $('#BobiDataList').dropload({
                    scrollArea: window,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData("/Admin/Bobi/GetBoBiListPage", param, me,
                            function (rows, footers) {
                                var html = "";
                                for (var i = 0; i < rows.length; i++) {
                                    rows[i]["jstime"] = utils.changeDateFormat(rows[i]["jstime"], 'date');

                                    var dto = rows[i];
                                    html += '<li>' +
                                            '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.jstime + '</time><span class="sum">+' + dto.profit + '</span>';
                                    html += '日拨比：<span class="sum">' + dto.bili + '</span><i class="fa fa-angle-right"></i></div>' +
                                    '<div class="allinfo">' +
                                    '<dl><dt>结算日期</dt><dd>' + dto.jstime + '</dd></dl><dl><dt>本日收入</dt><dd>' + dto.income + '</dd></dl>' +
                                    '<dl><dt>本日支出</dt><dd>' + dto.outlay + '</dd></dl><dl><dt>本日盈利</dt><dd>' + dto.profit + '</dd></dl>' +
                                    '<dl><dt>日拨出比率</dt><dd>' + dto.bili + '</dd></dl>' +
                                    '</div></li>';
                                }
                                $("#BobiItemList").append(html);
                            }, function () {
                                $("#BobiItemList").append('<p class="dropload-noData">暂无数据</p>');
                            });
                    }
                });

                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    document.getElementById("checkAllBtn").checked = false;
                    $("#BobiItemList").empty();
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }

                //查询按钮
                $("#searchBtn").on("click", function () {
                    searchMethod();
                })
            }
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});