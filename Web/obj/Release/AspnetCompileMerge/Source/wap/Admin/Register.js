
define(['text!Register.html', 'jquery'], function (Register, $) {

    var controller = function (agm) {
        //设置标题
        $("#title").html("会员注册");
        appView.html(Register);
        var pmap = null;


        //初始化默认数据
        setDefaultData = function (map) {
            if (map.loginPass && map.loginPass != null) {
                $("#password").val(map.loginPass);
                $("#passwordRe").val(map.loginPass);
                $("#dfpass1").html(map.loginPass);
            }
            if (map.passOpen && map.passOpen != null) {
                $("#passOpen").val(map.passOpen);
                $("#passOpenRe").val(map.passOpen);
                $("#dfpass2").html(map.passOpen);
            }
            if (map.threePass && map.threePass != null) {
                $("#threepass").val(map.threePass);
                $("#threepassRe").val(map.threePass);
                $("#dfpass3").html(map.threePass);
            }
            if (map.fatherName && map.fatherName != null) {
                $("#fatherName").val(map.fatherName);
            }
            if (map.defaultReName && map.defaultReName != null) {
                $("#reName").val(map.defaultReName);
            }
            if (map.zcxy && map.zcxy != null) {
                $("#regContent").html(map.zcxy);
            }
            if (map.pk && map.pk != null) {
                $("#pk").html(map.pk);
            }
            if (map.yk && map.yk != null) {
                $("#yk").html(map.yk);
            }
            if (map.jk && map.jk != null) {
                $("#jk").html(map.jk);
            }
            if (map.userIdPrefix && map.userIdPrefix != null) {
                $("#qzUserId").html(map.userIdPrefix);
            }
        }

        var data = { uid: 0 };
        var treePlace = undefined;
        //系谱图参数， userId+treeplace,最后一位为系谱图区域
        if (agm) {
            var fuid = agm.substring(0, agm.length - 1);
            treePlace = agm.substring(agm.length - 1, agm.length);
            treePlace = treePlace == 0 ? -1 : treePlace;
            data = { uid: fuid };
        }

        //第一步到第二步
        step1To2 = function () {
            if ($("#userId").val() == 0) {
                utils.showErrMsg("请输入会员编号");
                $("#userId").focus();
            } else if ($("#password").val() == 0) {
                utils.showErrMsg("请输入登陆密码");
                $("#password").focus();
            } else if ($("#passOpen").val() == 0) {
                utils.showErrMsg("请输入安全密码");
                $("#passOpen").focus();
            } else if ($("#threepass").val() == 0) {
                utils.showErrMsg("请输入交易密码");
                $("#threepass").focus();
            } else if ($("#passwordRe").val() != $("#password").val()) {
                utils.showErrMsg("确认登录密码与登录密码不一致");
                $("#passwordRe").focus();
            } else if ($("#passOpenRe").val() != $("#passOpen").val()) {
                utils.showErrMsg("确认安全密码与安全密码不一致");
                $("#passOpenRe").focus();
            } else if ($("#threepassRe").val() != $("#threepass").val()) {
                utils.showErrMsg("确认交易密码与交易密码不一致");
                $("#threepassRe").focus();
            } else {
                $("#setpTab1").css("display", "none");
                $("#setpTab2").css("display", "block");
            }
        }

        //第二步返回第一步
        back2To1 = function () {
            $("#setpTab2").css("display", "none");
            $("#setpTab1").css("display", "block");
        }

        //第二步到第三步
        step2To3 = function () {
            if ($("#fatherName").val() == 0) {
                utils.showErrMsg("请输入接点人编号");
                $("#fatherName").focus();
            } else if ($("#reName").val() == 0) {
                utils.showErrMsg("请输入推荐人编号");
                $("#reName").focus();
            } else if ($("#shopName").val() == 0) {
                utils.showErrMsg("请输入报单中心编号");
                $("#shopName").focus();
            } else {
                $("#setpTab2").css("display", "none");
                $("#setpTab3").css("display", "block");
            }
        }

        //第三步返回第二步
        back3To2 = function () {
            $("#setpTab3").css("display", "none");
            $("#setpTab2").css("display", "block");
        }

        //打开注册协议
        openRegisterWin = function (index) {
            $("#zcxydiv").css("display", "block");
            $("#setpTab3").css("display", "none");
        }

        //同意并注册
        agreeRegister = function (ind) {
            $("#zcxydiv").css("display", "none");
            $("#setpTab3").css("display", "block");
            if (ind == 1) {
                $("#read")[0].checked = true;
            }
        }

        var indexChecked = [0, 0, 0];

        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //选择左右区
        checkedTreePlace = function (index) {
            $("#treePlace1").removeClass("active");
            $("#treePlace2").removeClass("active");
            if (index == -1) {
                $("#treePlace1").addClass("active");
            } else {
                $("#treePlace2").addClass("active");
            }
        }

        //查询参数
        this.param = utils.getPageData();

        var productInit = false;

        var re = /^[0-9]+$/;

        //减少数量
        subNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (re.test(val) && parseInt(val) > 0) {
                $("#buyNum" + id).val(parseInt(val) - 1);
                $("#buyNum" + id).attr("dataVal", parseInt(val) - 1);
                calTotalMoney();
            }
        }

        //增加数量
        addNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (re.test(val)) {
                $("#buyNum" + id).val(parseInt(val) + 1);
                $("#buyNum" + id).attr("dataVal", parseInt(val) + 1);
                calTotalMoney();
            }
        }

        //录入数量
        changeBuyNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (!re.test(val)) {
                $("#buyNum" + id).val($("#buyNum" + id).attr("dataVal"));
            } else {
                calTotalMoney();
            }
        }

        //计算总金额
        calTotalMoney = function () {
            var total = 0;
            $(".productBox").each(function () {
                if (this.checked) {
                    var id = $(this).attr("dataId");
                    var num = $("#buyNum" + id).val();
                    if (re.test(num)) {
                        num = parseInt(num);
                        var price = parseFloat($(this).attr("dataPrice"));
                        total = total + (num * price);
                    }
                }
            });
            $("#totalMoney").html(total);
        }

        //商品勾选的上一步
        prevProduct = function () {
            $("#productSelectDiv").css("display", "none");
            $("#setpTab3").css("display", "block");
        }


        //下一步，商品勾选
        netProduct = function () {
            var checked = true;
            //数据校验
            $("#setpTab3 input").each(function (index, ele) {
                var jdom = $(this);
                if (jdom.attr("emptymsg") && jdom.val() == 0) {
                    utils.showErrMsg(jdom.attr("emptymsg"));
                    jdom.focus();
                    checked = false;
                }
                if (!checked) { return false; }
            });
            if (checked) {
                //手机号码验证
                var ptext = /^1(3|4|5|7|8)\d{9}$/;
                if (!ptext.test($("#phone").val())) {
                    utils.showErrMsg("手机号码格式错误");
                    $("#phone").focus();
                } else if ($("#read")[0].checked == false) {
                    utils.showErrMsg("请勾选注册协议");
                } else {
                    $("#setpTab3").css("display", "none");
                    $("#productSelectDiv").css("display", "block");
                    scrollTo(0, 0);
                    if (!productInit) {
                        var dropload = $('#productData').dropload({
                            scrollArea: window,
                            domDown: { domNoData: '<p class="dropload-noData"></p>' },
                            loadDownFn: function (me) {
                                utils.LoadPageData("/Admin/Register/GetProductListPage", param, me,
                                    function (rows, footers) {
                                        var html = "";
                                        for (var i = 0; i < rows.length; i++) {
                                            var dto = rows[i];
                                            html += '<dl><dt style="vertical-align:middle;"><table style="float:left;"><tr>' +
                                                  '<td><input type="checkbox" onclick="calTotalMoney()" dataName="' + dto.productName + '" dataId="' + dto.id + '" dataPrice="'
                                                  + dto.price + '" class="productBox" style="float:left;width:2rem;height:2rem;" />&nbsp;</td>' +
                                                  '<td><img src="' + dto.imgUrl + '" /></td></tr></table></dt>' +
                                                  '<dd><div class="cartcomminfo"><dl><dt></dt><dd><span>' + dto.productCode + '</span></dd></dl>' +
                                                  '<h2>' + dto.productName + '</h2><h3>¥' + dto.price + '</h3><dl><dt></dt>' +
                                                  '<dd><button class="btnminus" onclick=\'subNum("' + dto.id + '")\'><i class="fa fa-minus"></i></button>' +
                                                  '<input type="number" onblur=\'changeBuyNum("' + dto.id + '")\' dataVal="0" class="txtamount" id="buyNum' + dto.id + '" value="0" />' +
                                                  '<button class="btnplus" onclick=\'addNum("' + dto.id + '")\'><i class="fa fa-plus"></i></button></dd>' +
                                                  '</dl></div></dd></dl>';
                                        }
                                        $("#productDataList").append(html);
                                    }, function () {
                                        $("#productDataList").append('<p class="dropload-noData">暂无数据</p>');
                                    });
                            }
                        });
                        //提交注册按钮
                        $("#saveBtn").bind("click", function () {
                            var fs = {};
                            $("#setpTab1 input").each(function () {
                                fs[this.id] = this.value;
                            });
                            $("#setpTab2 input").each(function () {
                                fs[this.id] = this.value;
                            });
                            $("#setpTab3 input").each(function () {
                                fs[this.id] = this.value;
                            });
                            if ($("#read")[0].checked) { fs["read"] = "read"; }
                            var checked2 = true;
                            var i = 0;
                            $(".productBox").each(function () {
                                if (this.checked) {
                                    var id = $(this).attr("dataId");
                                    var num = $("#buyNum" + id).val();
                                    var name = $(this).attr("dataName");
                                    if (num == 0) {
                                        utils.showErrMsg("请录入商品【" + name + "】的数量");
                                        checked2 = false;
                                        return false;
                                    } else if (!re.test(num)) {
                                        utils.showErrMsg("商品【" + name + "】的的购买数量格式错误");
                                        checked2 = false;
                                        return false;
                                    } else {
                                        fs["orderItems[" + i + "].productId"] = id;
                                        fs["orderItems[" + i + "].num"] = num;
                                        fs["orderItems[" + i + "].productName"] = name;
                                        i++;
                                    }
                                }
                            });
                            if (checked2) {
                                if (parseFloat($("#totalMoney").html()) < parseFloat($("#pk").html())) {
                                    utils.showErrMsg("购买商品的金额不能成为普卡会员");
                                } else {
                                    if ($("#treePlace1").hasClass("active")) { fs["treePlace"] = -1; }
                                    else { fs["treePlace"] = 1; }
                                    fs["sourceMachine"] = "app";
                                    fs["userId"] = $("#qzUserId").html() + "" + fs["userId"];
                                    utils.AjaxPost("/Admin/Register/RegisterMember", fs, function (result) {
                                        if (result.status == "fail") {
                                            utils.showErrMsg(result.msg);
                                        } else {
                                            var mm = result.result;
                                            regModel = mm;
                                            location.href = "#regSuccess";
                                        }
                                    });
                                }
                            }
                        });
                        productInit = true;
                    }
                }// end - else
            }
        }

        //加载默认数据
        utils.AjaxPostNotLoadding("/Admin/Register/GetDefaultData", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                pmap = result.map;
                setDefaultData(pmap);

                //从系谱图注册的默认区域
                if (treePlace) {
                    checkedTreePlace(treePlace);
                }

                ////初始化银行帐号下拉框
                //var bankLit = cacheList["UserBank"];
                //utils.InitMobileSelect('#bankName', '开户行', bankLit, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                //    $("#bankName").val(data[0].name);
                //});

                //省市区选择
                var proSet = utils.InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                //输入框取消按钮
                utils.CancelBtnBind(function (dom) {
                    var prev = dom.prev();
                    if (prev[0].id == "reName" || prev[0].id == "shopName" || prev[0].id == "fatherName") {
                        $("#" + prev[0].id + "" + prev.attr("checkflag")).empty();
                    }
                });
 

                //绑定获离开焦点事件
                $("input").each(function (index, ele) {
                    var dom = $(this);
                    if (dom.attr("checkflag")) {
                        var flag = dom.attr("checkflag");
                        var domId = dom[0].id;
                        dom.bind("blur", function (event) {
                            if (dom.val() && dom.val() != 0) {
                                var userId = dom.val();
                                utils.AjaxPostNotLoadding("/Admin/Register/CheckUserId", { userId: userId, flag: flag }, function (result) {
                                    if (result.status == "fail") {
                                        $("#" + domId + "" + flag).html(result.msg);
                                    } else {
                                        $("#" + domId + "" + flag).html(result.msg);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#regContent').dialog("destroy");
        };
    };

    return controller;
});