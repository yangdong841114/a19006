
define(['text!financeCenter.html', 'jquery'], function (financeCenter, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("财务管理");
        appView.html(financeCenter);

        var items = menuItem[parentId];
        if (items && items.length > 0) {
            var banHtml = "";
            var epHtml = "";
            for (var i = 0; i < items.length; i++) {
                var node = items[i];
                var url = (node.curl + "").replace("Admin/", "");
                if (node.isShow == 0) {
                    if (node.id == "1030311" || node.id == "1030312" || node.id == "1030314") {
                        epHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                    } else {
                        banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                    }
                }
            }
            $("#financeCenteritemCont").html(banHtml);
            $("#epCenteritemCont").html(epHtml);
        }

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});