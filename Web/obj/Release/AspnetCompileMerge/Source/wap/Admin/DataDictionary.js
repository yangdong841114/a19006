
define(['text!DataDictionary.html', 'jquery'], function (DataDictionary, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("数据字典")

        //可编辑的记录列表
        var recordList = {}
        var dropload = null;
        var addParentList = null;

        

        //查询参数
        this.param = utils.getPageData();

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#DataDictionaryitemList").empty();
            param["parentId"] = $("#parentId2").val();
            param["name"] = $("#name2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //清空数据按钮
        clearData = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    dom.prev().val("");
                });
            });
        }

        //编辑按钮
        editMethod = function (id) {
            $("#prompTitle").html("编辑数据");
            $("#prompCont").empty();
            var dto = recordList[id];
            var html = '<dl><dt>数据类型</dt><dd><input type="hidden" id="id" value="' + id + '" />' +
                       '<p>' + dto.parentName + '</p></dd></dl>' +
                       '<dl><dt>类型编码</dt><dd><p>' + dto.type + '</p>' +
                       '</dd></dl><dl><dt>名称</dt><dd><input type="text" required="required" class="entrytxt" id="name" value="' + dto.name + '" placeholder="请填写名称" /><span class="erase">' +
                       '<i class="fa fa-times-circle-o"></i></span></dd></dl><dl><dt>描述</dt><dd>' +
                       '<input type="text" class="entrytxt" required="required" id="remark" value="' + dto.remark + '" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
            $("#prompCont").html(html);
            clearData();
            $("#sureBtn").html("确定提交")
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#name").val() == 0) {
                    utils.showErrMsg("请填写名称");
                } else {
                    saveOrUpdate();
                }
            })
            utils.showOrHiddenPromp();
        }

        //更新或保存数据
        saveOrUpdate = function (action) {
            var data = {};
            if ("add" == action) {
                data = { parentId: $("#parentId").val() == "-1" ? "0" : $("#parentId").val(), type: $("#type").val(), name: $("#name").val(), remark: $("#remark").val() };
            } else {
                data = { id: $("#id").val(), name: $("#name").val(), remark: $("#remark").val() };
            }
            utils.AjaxPost("/Admin/DataDictionary/SaveOrUpdate", data, function (result) {
                utils.showOrHiddenPromp();
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg(result.msg);
                    searchMethod();
                }
            });
        }



        $.ajax({
            url: "/Admin/DataDictionary/GetRootList",
            type: "POST",
            success: function (result) {
                if (result.status == "success") {

                    appView.html(DataDictionary);

                    //清空查询条件按钮
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //查询按钮
                    $("#searchBtn").on("click", function () {
                        searchMethod();
                    })

                    //根节点类型
                    var rootObj = {};
                    var list = result.list;
                    addParentList = $.extend(true, [], list)
                    addParentList.splice(0, 0, { id: "-1", name: '新建根节点' });

                    //初始化查询区select
                    if (list && list.length > 0) {
                        for (var i = 0; i < list.length; i++) {
                            rootObj[list[i].id] = list[i].name;
                            rootObj[list[i].id + "obj"] = list[i];
                        }
                        utils.InitMobileSelect('#parentId2Name', '数据类型', list, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                            $("#parentId2Name").val(data[0].name);
                            $("#parentId2").val(data[0].id);
                        });
                    }

                    //绑定展开搜索更多
                    utils.bindSearchmoreClick();

                    //上拉加载
                    dropload = $('#DataDictionarydatalist').dropload({
                        scrollArea: window,
                        domDown: { domNoData: '<p class="dropload-noData"></p>' },
                        loadDownFn: function (me) {
                            utils.LoadPageData("/Admin/DataDictionary/GetListChildren", param, me,
                                function (rows, footers) {
                                    var html = "";
                                    for (var i = 0; i < rows.length; i++) {
                                        rows[i]["parentName"] = rootObj[rows[i].parentId];

                                        var dto = rows[i];
                                        recordList[dto.id] = dto;
                                        html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style="overflow: hidden"><time>' + dto.id +
                                                '</time><span class="sum">' + dto.parentName + '</span><span class="sum">' + dto.name + '</span>';
                                        html += '<i class="fa fa-angle-right"></i></div>' +
                                        '<div class="allinfo">' +
                                        '<div class="btnbox"><ul class="tga1">' +
                                        '<li><button class="seditbtn" onclick=\'editMethod(' + dto.id + ')\'>编辑</button></li>' +
                                        '</ul></div>' +
                                        '<dl><dt>ID</dt><dd>' + dto.id + '</dd></dl><dl><dt>类型编码</dt><dd>' + dto.type + '</dd></dl>' +
                                        '<dl><dt>数据类型</dt><dd>' + dto.parentName + '</dd></dl><dl><dt>名称</dt><dd>' + dto.name + '</dd></dl>' +
                                        '<dl><dt>描述</dt><dd>' + dto.remark + '</dd></dl>' +
                                        '</div></li>';
                                    }
                                    $("#DataDictionaryitemList").append(html);
                                }, function () {
                                    $("#DataDictionaryitemList").append('<p class="dropload-noData">暂无数据</p>');
                                });
                        }
                    });

                    //隐藏提示框
                    $(".hideprompt").click(function () {
                        utils.showOrHiddenPromp();
                    });

                    //添加按钮
                    $("#addDdBtn").bind("click", function () {
                        $("#prompTitle").html("添加数据");
                        $("#prompCont").empty();
                        var html = '<dl><dt>数据类型</dt><dd><input type="hidden" id="parentId" />' +
                                   '<input type="text" class="entrytxt" id="parentIdName" placeholder="请选择" readonly/><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                                   '<dl><dt>类型编码</dt><dd><input type="text" required="required" class="entrytxt" id="type" disabled="disabled"/><span class="erase" id="typeCanBtn"><i class="fa fa-times-circle-o"></i></span>' +
                                   '</dd></dl><dl><dt>名称</dt><dd><input type="text" required="required" class="entrytxt" id="name" placeholder="请填写名称" /><span class="erase">' +
                                   '<i class="fa fa-times-circle-o"></i></span></dd></dl><dl><dt>描述</dt><dd>' +
                                   '<input type="text" required="required" class="entrytxt" id="remark" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
                        $("#prompCont").html(html);

                        $("#typeCanBtn").css("display", "none");
                        utils.InitMobileSelect('#parentIdName', '数据类型', addParentList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                            $("#parentIdName").val(data[0].name);
                            var did = data[0].id;
                            $("#parentId").val(did);
                            if (did == -1) {
                                $("#type").val("");
                                $("#type")[0].disabled = false;
                                $("#typeCanBtn").css("display", "block");
                            } else {
                                var rec = rootObj[did + "obj"];
                                $("#type")[0].disabled = true;
                                $("#type").val(rec.type)
                                $("#typeCanBtn").css("display", "none");
                            }

                        });
                        clearData();
                        $("#sureBtn").html("确定提交")
                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () {
                            if ($("#parentId").val() == 0) {
                                utils.showErrMsg("请选择数据类型");
                            } else if ($("#type").val() == 0) {
                                utils.showErrMsg("请填写类型编码");
                            } else if ($("#name").val() == 0) {
                                utils.showErrMsg("请填写名称");
                            } else {
                                saveOrUpdate("add");
                            }
                        })
                        utils.showOrHiddenPromp();
                    })

                   

                }
            }
        });

        controller.onRouteChange = function () {
            //销毁模态窗口
        };
    };

    return controller;
});