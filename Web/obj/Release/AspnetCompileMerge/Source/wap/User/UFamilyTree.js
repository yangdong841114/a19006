
define(['text!UFamilyTree.html', 'jquery'], function (UFamilyTree, $) {

    var controller = function (name) {
        $("#title").html("系谱图");
        
        //获取样式
        getTitleClass = function (u) {
            if (u.isPay != 1) {
                return 'level0'
            } else if (u.uLevel == 3) {
                return 'level1';
            } else if (u.uLevel == 4) {
                return 'level4';
            } else if (u.uLevel == 5) {
                return 'level3';
            }
        }

        //跳转用户的系谱图
        jumpUserId = function (us) {
            utils.AjaxPostNotLoadding("/User/UFamilyTree/GetAppFamilyTreeByCondition", { userId: us }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    var map = result.map;
                    if (!map || map.length == 0) {
                        utils.showErrMsg("未找到系谱图内容");
                    } else {
                        bindTree(map);
                    }
                }
            });
        }

        bindTree = function (map) {
            for (var i = 1; i < 8; i++) {
                var html = ""
                $("#kk" + i).attr('class', '')

                if (map[i + ""]) {
                    $("#kk" + i).addClass('userpedigree');
                    var dto = map[i + ""];
                    if (dto.id > 0) {
                        html = '<a href="javascript:jumpUserId(\'' + dto.userId + '\');"><span>' + dto.userId + '(' + dto.userName + ')</span></a>';
                        html += '<div class="pedigreedata">' +
                           '<table><tr><td>' + dto.encash0 + '</td><th>总</th><td>' + dto.encash1 + '</td></tr><tr><td>' + dto.new0 + '</td><th>新</th><td>' + dto.new1 + '</td>' +
                           '</tr><tr><td>' + dto.spare0 + '</td><th>余</th><td>' + dto.spare1 + '</td></tr></table></div>';
                        $("#kk" + i).addClass(getTitleClass(dto));
                    } else {
                        if (dto.isPay == 1) {
                            html = '<a href="#UserRegister/' + dto.fatherID + '' + dto.treePlace + '"><span>空位注册</span></a>';
                            html += '<div class="pedigreedata">' +
                           '<table><tr><td>' + dto.encash0 + '</td><th>总</th><td>' + dto.encash1 + '</td></tr><tr><td>' + dto.new0 + '</td><th>新</th><td>' + dto.new1 + '</td>' +
                           '</tr><tr><td>' + dto.spare0 + '</td><th>余</th><td>' + dto.spare1 + '</td></tr></table></div>';
                        } else {
                            html = '<a href="javascript:void(0);"><span>空位</span></a><div class="pedigreedata">' +
                           '<table><tr><td>0</td><th>总</th><td>0</td></tr><tr><td>0</td><th>新</th><td>0</td>' +
                           '</tr><tr><td>0</td><th>余</th><td>0</td></tr></table></div>';
                           
                        }
                    }
                    $("#kk" + i).html(html);
                }
            }
        }


        //初始化加载数据
        utils.AjaxPostNotLoadding("/User/UFamilyTree/GetAppFamilyTree", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UFamilyTree);
                var map = result.map;
                bindTree(map);

                //查询按钮
                $("#queryBtn").on("click", function () {
                    utils.AjaxPost("/User/UFamilyTree/GetAppFamilyTreeByCondition", {userId:$("#userId").val()}, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            bindTree(result.map);
                        }
                    });
                })


                //我的系谱图按钮
                $("#selfBtn").on("click", function () {
                    utils.AjaxPost("/User/UFamilyTree/GetAppFamilyTree", {}, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            $("#userId").val("");
                            bindTree(result.map);
                        }
                    });
                })
            }

        });

        controller.onRouteChange = function () {
            //销毁模态窗口
        };
    };

    return controller;
});
