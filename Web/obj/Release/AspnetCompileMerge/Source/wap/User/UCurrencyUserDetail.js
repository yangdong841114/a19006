
define(['text!UCurrencyUserDetail.html', 'jquery'], function (UCurrencyUserDetail, $) {

    var controller = function (agm) {
        if (agm) {
            $("#title").html("奖金详情");
            appView.html(UCurrencyUserDetail);

            //清空查询条件按钮
            $("#clearQueryBtn").bind("click", function () {
                utils.clearQueryParam();
            })

            //奖金类型选择框
            var bonusClassList = $.extend(true, [], cacheList["BonusClass"]);
            bonusClassList.splice(0, 0, {id:0,name:'全部'});
            utils.InitMobileSelect('#catName', '奖金类型', bonusClassList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                $("#catName").val(data[0].name);
                $("#cat").val(data[0].id);
            })

            //查询参数
            this.param = utils.getPageData();

            var dropload = $('#UCurrencyUserDetailDataList').dropload({
                scrollArea: window,
                domDown: { domNoData: '<p class="dropload-noData"></p>' },
                loadDownFn: function (me) {
                    utils.LoadPageData("/User/UCurrency/GetDetailListPage?addDate=" + agm, param, me,
                        function (rows, footers) {
                            var html = "";
                            for (var i = 0; i < rows.length; i++) {
                                rows[i]["jstime"] = utils.changeDateFormat(rows[i]["jstime"]);
                                rows[i]["ff"] = rows[i]["ff"] == 1 ? "已发" : "未发";
                                rows[i]["yf"] = rows[i]["yf"].toFixed(2);
                                rows[i]["fee1"] = rows[i]["fee1"].toFixed(2);
                                rows[i]["fee2"] = rows[i]["fee2"].toFixed(2);
                                rows[i]["fee3"] = rows[i]["fee3"].toFixed(2);
                                rows[i]["sf"] = rows[i]["sf"].toFixed(2);
                                var dto = rows[i];
                                html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.catName + '</time><span class="sum">+' + dto.yf + '</span>' +
                                      '实发：<span class="sum">¥' + dto.sf + '</span><i class="fa fa-angle-right"></i></div>' +
                                      '<div class="allinfo">' +
                                      '<dl><dt>奖金名称</dt><dd>' + dto.catName + '</dd></dl>' +
                                      '<dl><dt>应发金额</dt><dd>' + dto.yf + '</dd></dl><dl><dt>所得税</dt><dd>' + dto.fee1 + '</dd></dl>' +
                                      '<dl><dt>管理费</dt><dd>' + dto.fee2 + '</dd></dl><dl><dt>复消账户</dt><dd>' + dto.fee3 + '</dd></dl>' +
                                      '<dl><dt>实发</dt><dd>' + dto.sf + '</dd></dl><dl><dt>结算日期</dt><dd>' + dto.jstime + '</dd></dl>' +
                                      '<dl><dt>发放状态</dt><dd>' + dto.ff + '</dd></dl><dl><dt>业务摘要</dt><dd>' + dto.mulx + '</dd></dl>' +
                                      '</div></li>';
                            }
                            $("#UCurrencyUserDetailItemList").append(html);
                        }, function () {
                            $("#UCurrencyUserDetailItemList").append('<p class="dropload-noData">暂无数据</p>');
                        });
                }
            });

            //查询按钮
            $("#searchBtn").on("click", function () {
                param.page = 1;
                $("#UCurrencyUserDetailItemList").empty();
                param["cat"] = $("#cat").val();
                dropload.unlock();
                dropload.noData(false);
                dropload.resetload();
            })

        }

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});