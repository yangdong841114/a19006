
define(['text!UMTransfer.html', 'jquery'], function (UMTransfer, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期选择框
                    utils.initCalendar(["startTime", "endTime"]);
                    //绑定展开搜索更多
                    utils.bindSearchmoreClick();

                    //清空查询条件按钮
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })
                    //查询条件转账类型选择框
                    var transfer = $.extend(true, [], cacheList["MTransfer"]);
                    transfer.splice(0, 0, { id: 0, name: '全部' });
                    utils.InitMobileSelect('#typeName2', '转账类型', transfer, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                        $("#typeName2").val(data[0].name);
                        $("#typeId2").val(data[0].id);
                    })

                    //查询按钮
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加载数据
                searchMethod();
            }
        }

        //设置标题
        $("#title").html("账户转账");
        appView.html(UMTransfer);


        var dto = null;

        //转账明细
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UMTransferdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UMTransfer/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["typeId"] = cacheMap["MTransfer"][rows[i].typeId];
                     
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.typeId + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>转账日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>转账类型</dt><dd>' + dto.typeId + '</dd></dl><dl><dt>转出账户</dt><dd>' + dto.fromUserId + '</dd></dl>' +
                                  '<dl><dt>会员姓名</dt><dd>' + dto.fromUserName + '</dd></dl><dl><dt>转入账户</dt><dd>' + dto.toUserId + '</dd></dl>' +
                                  '<dl><dt>会员姓名</dt><dd>' + dto.toUserName + '</dd></dl><dl><dt>转账金额</dt><dd>' + dto.epoints + '</dd></dl>' +
                                  '</div></li>';
                        }
                        $("#UMTransferitemList").append(html);
                    }, function () {
                        $("#UMTransferitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UMTransferitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["typeId"] = $("#typeId2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //设置表单默认数据
        setDefaultValue = function (dto) {
            $("#agentDz").html(dto.account.agentDz);
            $("#agentJj").html(dto.account.agentJj);
            $("#toUserId").val("");
            $("#epoints").val("");
            $("#typeName").val("");
            $("#typeId").val("");
            $("#toUserName").empty();
        }

        //加载会员信息
        utils.AjaxPostNotLoadding("/User/UMTransfer/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMTransfer);
                dto = result.result;
                
                //转账类型选择框
                utils.InitMobileSelect('#typeName', '转账类型', cacheList["MTransfer"], { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                    $("#typeName").val(data[0].name);
                    $("#typeId").val(data[0].id);
                })

                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "typeName") {
                            $("#typeId").val("");
                        } else if (prev[0].id == "toUserId") {
                            $("#toUserName").empty();
                        }
                        dom.prev().val("");
                    });
                });

                //初始默认值
                setDefaultValue(dto);

                //会员编号离开焦点事件
                $("#toUserId").on("blur", function () {
                    $("#toUserName").empty();
                    var current = $("#toUserId");
                    var parent = current.parent();
                    if (current.val() != 0) {
                        utils.AjaxPostNotLoadding("/User/UMTransfer/GetUserName", { userId: current.val() }, function (result) {
                            if (result.status == "fail" || result.msg == "会员不存在") {
                                $("#toUserName").empty()
                            } else{
                                $("#toUserName").html(result.msg);
                            }
                        });
                    }
                })

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#typeId").val();
                    var current = $("#typeId");
                    var parent = current.parent();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    if (val == 0) {
                        utils.showErrMsg("请选择转账类型");
                    }else if (!g.test($("#epoints").val())) {
                        utils.showErrMsg("转账金额格式不正确");
                    }else if (val == 61 && $("#toUserId").val()==0) { 
                        utils.showErrMsg("请录入转账会员");
                    } else {
                        $("#zzlx").html($("#typeName").val());
                        $("#zzje").html($("#epoints").val());
                        $("#hybh").html($("#toUserId").val());
                        utils.showOrHiddenPromp();
                    }
                })

                //确认按钮
                $("#sureBtn").on('click', function () {
                    var data = { typeId: $("#typeId").val(), epoints: $("#epoints").val(), toUserId: $("#toUserId").val() };
                    utils.AjaxPost("/User/UMTransfer/SaveMTransfer", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            dto = result.result;
                            setDefaultValue(dto);
                        }
                    });
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});