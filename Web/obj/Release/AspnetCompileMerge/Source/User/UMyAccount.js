
define(['text!UMyAccount.html', 'jquery', 'j_easyui', 'zui'], function (UMyAccount, $) {

    var controller = function (name) {

        //加载信息
        utils.AjaxPostNotLoadding("UMyAccount/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMyAccount);
                var dto = result.result;
                $("#agentDz").html(dto.agentDz.toFixed(2));
                $("#agentJj").html(dto.agentJj.toFixed(2));
                $("#agentGw").html(dto.agentGw.toFixed(2));
                $("#agentFt").html(dto.agentFt.toFixed(2));
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});