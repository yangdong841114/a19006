﻿using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DAO
{
    /// <summary>
    /// 通用数据访问接口
    /// </summary>
    public  interface IBaseDao
    {
        /// <summary>
        /// 执行存储过程，返回指定参数
        /// </summary>
        /// <param name="storedProcName">存储过程名称</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="ReturnPara">返回参数名称。注意此项为存储过程返回变量，不用带@</param>
        /// <returns></returns>
        string RunProceOnlyPara(string storedProcName, IDataParameter[] parameters, string ReturnPara);

        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="dto">对象</param>
        /// <returns>自增生成的ID</returns>
        object SaveByIdentity(DtoData dto);

        /// <summary>
        /// 查询单对象
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        object ExecuteScalar(string sql);

        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="dto">对象</param>
        /// <returns>执行行数</returns>
        int Save(DtoData dto);

        /// <summary>
        /// 根据更新一条记录 
        /// </summary>
        /// <param name="dto">要保存的对象，必须有id字段为主键</param>
        /// <returns>执行的行数</returns>
        int Update(DtoData dto);

        /// <summary>
        /// 根据SQL更新
        /// </summary>
        /// <param name="sql">sql</param>
        /// <param name="param">参数</param>
        /// <returns></returns>
        int ExecuteBySql(string sql, List<DbParameterItem> param);

        /// <summary>
        /// 根据SQL更新
        /// </summary>
        /// <param name="sql">sql</param>
        /// <returns></returns>
        int ExecuteBySql(string sql);

        /// <summary>
        /// 根据ID删除记录
        /// </summary>
        /// <param name="table">表名</param>
        /// <param name="id">ID</param>
        /// <returns>执行的行数</returns>
        int Delte(string table,object id);

        /// <summary>
        /// 根据SQL条件查询记录条数
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <param name="param">参数列表</param>
        /// <param name="isJointSql">是否拼接SQL,true:根据param拼装SQL</param>
        /// <returns>记录数</returns>
        int GetCount(string sql, List<DbParameterItem> param, bool isJointSql);

        /// <summary>
        /// 根据SQL查询记录条数
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <returns>记录数</returns>
        int GetCount(string sql);
        int GetCount(string sql, List<DbParameterItem> param, bool isJointSql, string key);

        /// <summary>
        /// 根据SQL条件查询单个记录
        /// </summary>
        /// <param name="sql">sql</param>
        /// <param name="param">参数列表</param>
        /// <param name="isJointSql">是否拼接SQL,true:根据param拼装SQL</param>
        /// <returns>DataRow行数据</returns>
        DataRow GetOne(string sql, List<DbParameterItem> param, bool isJointSql);

        /// <summary>
        /// 根据SQL查询单个记录
        /// </summary>
        /// <param name="sql">sql</param>
        /// <returns>DataRow行数据</returns>
        DataRow GetOne(string sql);

        /// <summary>
        /// 根据SQL条件查询列表
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <param name="param">参数列表</param>
        /// <param name="isJointSql">是否拼接SQL,true:根据param拼装SQL</param>
        /// <returns>列表</returns>
        DataTable GetList(string sql, List<DbParameterItem> param, bool isJointSql);


        /// <summary>
        /// 根据SQL查询列表
        /// </summary>
        /// <param name="sql">查询语句</param>
        /// <returns>列表</returns>
        DataTable GetList(string sql);


        /// <summary>
        /// 根据dataset批量更新
        /// </summary>
        /// <param name="dataSet">数据源</param>
        /// <param name="tableName">表名</param>
        /// <param name="updateSql">执行SQL</param>
        /// <param name="param">参数</param>
        /// <returns></returns>
        int UpdateBatchByDataSet(DataSet dataSet, string tableName,string updateSql,List<DbParameterItem> param);

        /// <summary>
        /// 根据dataset批量插入
        /// </summary>
        /// <param name="dataSet">数据源</param>
        /// <param name="tableName">表名</param>
        /// <param name="insertSql">执行SQL</param>
        /// <param name="param">参数</param>
        /// <returns></returns>
        int InsertBatchByDataSet(DataSet dataSet, string tableName, string updateSql, List<DbParameterItem> param);

        /// <summary>
        /// 分页条件查询,不拼装SQL
        /// sql 需包含 row_number() over(order by d.parentId,d.id) rownumber 语句
        /// param 需包含strnum和endnum,分别表示起始条数，截至条数
        /// </summary>
        /// <param name="sql">select *,row_number() over(order by d.parentId,d.id) rownumber from table </param>
        /// <param name="param">param 需包含strnum和endnum,分别表示起始条数，截至条数</param>
        /// <returns></returns>
        DataTable GetPageListNotJoinSql(string sql, List<DbParameterItem> param);

        /// <summary>
        /// 分页条件查询,
        /// sql 需包含 row_number() over(order by d.parentId,d.id) rownumber 语句
        /// param 需包含strnum和endnum,分别表示起始条数，截至条数
        /// </summary>
        /// <param name="sql">select *,row_number() over(order by d.parentId,d.id) rownumber from table </param>
        /// <param name="param">param 需包含strnum和endnum,分别表示起始条数，截至条数</param>
        /// <returns></returns>
        DataTable GetPageList(string sql, List<DbParameterItem> param);
        DataTable GetPageList(string sql, List<DbParameterItem> param, string key);

    }
}
