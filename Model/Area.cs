﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 省市区
    /// </summary>
    [Serializable]
    public class Area : DtoData
    {
        public int? id { get; set; }
        public int? parentId { get; set; }
        public virtual string code { get; set; }
        public virtual string name { get; set; }
        public virtual string qcode { get; set; }
        public virtual string zipcode { get; set; }
       
    }
}
