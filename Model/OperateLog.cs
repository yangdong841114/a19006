﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 操作日志表
    /// </summary>
    [Serializable]
    public class OperateLog : Page,DtoData
    {
        //主键id
        public virtual int? oid { get; set; }
        //操作记录ID
        public virtual int? recordId { get; set; }
        //操作人ID
        public virtual int? uid { get; set; }
        //操作人编码
        public virtual string userId { get; set; }
        //IP地址
        public virtual string ipAddress { get; set; }
        //描述
        public virtual string mulx { get; set; }
        //操作时间
        public virtual DateTime? addTime { get; set; }
        //表名
        public virtual string tableName { get; set; }
        //表对应业务名称
        public virtual string recordName { get; set; }


        public virtual string userName { get; set; }
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
    }
}
