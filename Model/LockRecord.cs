﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 记录锁仓表实体
    /// </summary>
    [Serializable]
    public class LockRecord : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //订单号
        public virtual string OrderId { get; set; }
        //转出地址
        public virtual string fromAddress { get; set; }
        //HASH
        public virtual string HASH { get; set; }
        //金额
        public virtual double? pocValue { get; set; }
        //提交时间
        public virtual DateTime? addTime { get; set; }
        //到帐时间
        public virtual DateTime? getTime { get; set; }
        //-1无档次1有档次2异常用数据
        public virtual int? flag { get; set; }
        //档次ID
        public virtual int? LockType { get; set; }
        public virtual string LockTypeName { get; set; }
        //remark
        public virtual string remark { get; set; }

        public virtual double? maxSfAmounts { get; set; }

        public virtual double? maxFhAmounts { get; set; }


        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }


        //查询 条件冗余字段end--------------------------------------------------------
    }
}
