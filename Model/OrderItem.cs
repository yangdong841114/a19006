﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 订单体实体
    /// </summary>
    [Serializable]
    public class OrderItem : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //订单号
        public virtual string hid { get; set; }
        //商品ID
        public virtual int? productId { get; set; }
        //商品名称
        public virtual string productName { get; set; }
        //商品编码
        public virtual string productNumber { get; set; }
        //单价
        public virtual double? price { get; set; }
        //数量
        public virtual int? num { get; set; }
        //金额
        public virtual double? total { get; set; }
        //积分
        public virtual double? pv { get; set; }

        //商品图片
        public virtual string productImg { get; set; }
       
    }
}
