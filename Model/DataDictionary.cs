﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 数据字典
    /// </summary>
    [Serializable]
    public class DataDictionary : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //类型
        public virtual string type { get; set; }
        //上级ID
        public virtual int? parentId { get; set; }
        //名称
        public virtual string name { get; set; }
        //描述
        public virtual string remark { get; set; } 
        //创建人ID
        public virtual int? addUid { get; set; }
        //创建时间
        public virtual DateTime? addTime { get; set; }
        //修改人ID
        public virtual int? updateUid { get; set; }
        //最后修改时间
        public virtual DateTime? updateTime { get; set; }

        public virtual int? pocLockScbs { get; set; }
        public virtual double? pocLockScpoc { get; set; }
       
    }
}
