﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    [Serializable]
    public class CurrencySums : Page, DtoData
    {
        public virtual int? id { get; set; }
        public virtual DateTime? addTime { get; set; }
        public virtual double? sumPoc { get; set; }
        public virtual int? isStatus { get; set; }
    }
}
