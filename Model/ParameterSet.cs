﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 参数表实体
    /// </summary>
    [Serializable]
    public class ParameterSet : DtoData
    {
        //主键ID
        public virtual int? id { get; set; }
        //参数值
        public virtual string paramValue { get; set; }
        //单位
        public virtual string unit { get; set; }
        //参数说明
        public virtual string purpose { get; set; }
        //参数类型名称
        public virtual string typeName { get; set; }
        //参数编码，对应页面input控件的ID（在表中唯一）
        public virtual string paramCode { get; set; }
    }
}
