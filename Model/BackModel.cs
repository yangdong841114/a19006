﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 数据库备份实体
    /// </summary>
    [Serializable]
    public class BackModel : Page, DtoData
    {
        public int? id { get; set; }

        public string fname { get; set; }

        public DateTime? addTime { get; set; }
       
    }
}
