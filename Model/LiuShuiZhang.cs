﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 流水账表实体
    /// </summary>
    [Serializable]
    public class LiuShuiZhang : Page,DtoData 
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //账户类型
        public virtual int? accountId { get; set; }
        //描述
        public virtual string abst { get; set; }
        //收入
        public virtual double? income { get; set; }
        //支出
        public virtual double? outlay { get; set; }
        //余额
        public virtual double? last { get; set; }
        //写入时间
        public virtual DateTime? addtime { get; set; }
        //来源ID
        public virtual int? sourceId { get; set; }
        //来源表名
        public virtual string tableName { get; set; }
        //写入用户ID
        public virtual int? addUid { get; set; }
        //写入用户名称
        public virtual string addUser { get; set; }

        //冗余类型
        public virtual string optype { get; set; }

        public virtual double? epotins { get; set; }

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
    }
}
