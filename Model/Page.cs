﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Page
    {
        public int? page { get; set; }  //第几页

        public int? rows { get; set; }  //取多少条
    }
}
