﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 短信通知
    /// </summary>
    [Serializable]
    public class MobileNotice : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //标题
        public virtual string title { get; set; }
        public virtual int? flag { get; set; }
        //手机号
        public virtual string phone { get; set; }
        //短信消息
        public virtual string msg { get; set; }
        //编码
        public virtual string code { get; set; }
        
       
    }
}
