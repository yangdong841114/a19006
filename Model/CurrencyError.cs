﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    [Serializable]
    public class CurrencyError : Page, DtoData
    {
        public virtual int? id { get; set; }
        public virtual string userId { get; set; }
        public virtual string pocAddress { get; set; }
        public virtual double? sumSc { get; set; }
        public virtual double? sumSf { get; set; }
        public virtual double? sumFh { get; set; }
        public virtual double? maxFh { get; set; }
        public virtual DateTime? addTime { get; set; }
        public virtual string remarks { get; set; }
    }
}
