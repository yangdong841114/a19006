﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 商品表实体
    /// </summary>
    [Serializable]
    public class Product : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //商品编号
        public virtual string productCode { get; set; }
        //商品名称
        public virtual string productName { get; set; }
        //图片url
        public virtual string imgUrl { get; set; }
        //商品类型
        public virtual int? productType { get; set; }
        //单价
        public virtual double? price { get; set; }
        //复消价
        public virtual double? fxPrice { get; set; }
        //添加时间
        public virtual DateTime? addTime { get; set; }
        //商品描述
        public virtual string cont { get; set; }
        //是否审核通过  1：否，2：是
        public virtual int? isPass { get; set; }
        //通过时间
        public virtual DateTime? passTime { get; set; }
        //审核人ID
        public virtual int? auditUid { get; set; }
        //审核人编码
        public virtual string auditUser { get; set; }
        //是否上架  1：否，2：是
        public virtual int? isShelve { get; set; }
        //积分
        public virtual double? pv { get; set; }

        //是否审核1：否，2：是
        public virtual int? flag { get; set; }
        public virtual int? productTypeId { get; set; }
        public virtual string productTypeName { get; set; }
        public virtual int? productBigTypeId { get; set; }
        public virtual string productBigTypeName { get; set; }

        //是否只查询商家商品 Y：查询所有商家商品，N：查询系统商品
        public virtual string notSystem { get; set; }
        public virtual string mname { get; set; }
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

    }
}
