﻿public class RespondJson
{
    /// <summary>
    ///加密的响应结果
    /// </summary>
    public string data;

    public string encryptkey;

}

public class ResponseData
{
    public string merchantaccount;
    public string yborderid;
    public string orderid;
    public string payurl;
    public string imghexstr;
    public string sign;
    public string error_code;
    public string error_msg;
   
}