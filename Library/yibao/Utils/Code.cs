﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;


public class Code
{

    public Code() { }

    public string Encode()
    {
        return "";
    }


    /// <summary>
    /// 将字符串进行urlencode  编码格式是UTF-8
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string UrlEncode(string str)
    {
        StringBuilder sb = new StringBuilder();
        byte[] byStr = System.Text.Encoding.UTF8.GetBytes(str); //默认是System.Text.Encoding.Default.GetBytes(str)
        for (int i = 0; i < byStr.Length; i++)
        {
            sb.Append(@"%" + Convert.ToString(byStr[i], 16));
        }

        return (sb.ToString());
    }







    /// <summary>
    /// 是否是utf-8的编码格式
    /// </summary>
    /// <param name="buff"></param>
    /// <returns></returns>
    public static bool isUtf8(byte[] buff)
    {
        for (int i = 0; i < buff.Length; i++)
        {
            if ((buff[i] & 0xE0) == 0xC0)    // 110x xxxx 10xx xxxx
            {
                if ((buff[i + 1] & 0x80) != 0x80)
                {
                    return false;
                }
            }
            else if ((buff[i] & 0xF0) == 0xE0)  // 1110 xxxx 10xx xxxx 10xx xxxx
            {
                if ((buff[i + 1] & 0x80) != 0x80 || (buff[i + 2] & 0x80) != 0x80)
                {
                    return false;
                }
            }
            else if ((buff[i] & 0xF8) == 0xF0)  // 1111 0xxx 10xx xxxx 10xx xxxx 10xx xxxx
            {
                if ((buff[i + 1] & 0x80) != 0x80 || (buff[i + 2] & 0x80) != 0x80 || (buff[i + 3] & 0x80) != 0x80)
                {
                    return false;
                }
            }
        }
        return true;
    }
}
