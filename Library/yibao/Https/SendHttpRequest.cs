﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;


/// <summary>
/// 发送http的请求方法
/// </summary>
public class SendHttpRequest
{

    /// <summary>
    /// 发送http请求
    /// </summary>
    /// <param name="requestURL">请求地址</param>
    /// <param name="datastring">请求数据</param>
    /// <param name="post">是否是post</param>
    /// <param name="reqEncoding">请求编码格式</param>
    /// <param name="respEncoding">响应编码格式</param>
    /// <returns></returns>
    public static string payRequest(string requestURL, string datastring, bool  post,Encoding reqEncoding, Encoding respEncoding)
    {
        //返回结果
        string responseStr = "";

        if (post)
        {
            responseStr = HttpClass.HttpPost(requestURL, datastring,reqEncoding,respEncoding);
        }
        else
        {
            responseStr = HttpClass.HttpGet(requestURL, datastring,reqEncoding,respEncoding);
        }
        return responseStr;

    }








}