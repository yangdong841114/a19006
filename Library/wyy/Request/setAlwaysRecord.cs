﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.wyy.Request
{
    /// <summary>
    /// 设置频道为录制状态
    /// </summary>
    public class setAlwaysRecord
    {
        /// <summary>
        /// 频道ID，32位字符串
        /// </summary>
        public string cid { get; set; }
        /// <summary>
        /// 1-开启录制； 0-关闭录制
        /// </summary>
        public int needRecord { get; set; }
        /// <summary>
        /// 1-flv； 0-mp4
        /// </summary>
        public int format { get; set; }
        /// <summary>
        /// 录制切片时长(分钟)，5~120分钟
        /// </summary>
        public int duration { get; set; }
        /// <summary>
        /// 录制后文件名（只支持中文、字母和数字），格式为filename_YYYYMMDD-HHmmssYYYYMMDD-HHmmss, 文件名录制起始时间（年月日时分秒) -录制结束时间（年月日时分秒)
        /// </summary>
        public string filename { get; set; }
    }
}
