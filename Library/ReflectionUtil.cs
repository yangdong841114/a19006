﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Library
{
    public class ReflectionUtil
    {

        /// <summary>
        /// 检查是否基础字段类型，（非复合对象类型）
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static bool IsBaseDataType(System.Type type){
            if (type == typeof(int?)) { return true; }
            if (type == typeof(double?)) { return true; }
            if (type == typeof(DateTime?)) { return true; }
            if (type == typeof(int)) { return true; }
            if (type == typeof(double)) { return true; }
            if (type == typeof(DateTime)) { return true; }
            if (type == typeof(string)) { return true; }
            return false;
        }

        private static bool NotPageMember(string fieldName)
        {
            return (fieldName != "CurrentPageIndex" && fieldName != "PageSize");
        }

        /// <summary>
        /// 返回插入SQL语句
        /// </summary>
        /// <param name="tableName">要插入的表名</param>
        /// <param name="obj">要插入的对象</param>
        /// <returns>INSERT SQL</returns>
        public static string GetInsertSql(string tableName,object obj)
        {
            if (obj == null ||tableName==null) return null;

            Type t = obj.GetType();
            StringBuilder sb2 = new StringBuilder(" Values(");
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into ").Append(tableName).Append("(");
            string sql = null;
            PropertyInfo[] pis = t.GetProperties();
            bool isExists = false;  //是否存在非空字段
            if (pis != null && pis.Length > 0)
            {
                foreach (PropertyInfo p in pis)
                {
                    Type pt = p.PropertyType;           //字段类型
                    if (IsBaseDataType(pt) && NotPageMember(p.Name))
                    {
                        object o = p.GetValue(obj, null);   //字段值
                        //字段值不为空时拼接到插入SQL字符串中
                        if (o != null)
                        {
                            isExists = true;
                            sb.Append(p.Name).Append(",");
                            sb2.Append("@").Append(p.Name).Append(",");
                        }
                    }
                }
                if (isExists)
                {
                    string a = sb.ToString().Substring(0, sb.Length - 1) + ") ";
                    string b = sb2.ToString().Substring(0, sb2.Length - 1) + ") ";
                    sql = a + b;
                }
            }

            return sql;
        }

        /// <summary>
        /// 返回更新SQL语句，所有实体需以id为对象
        /// </summary>
        /// <param name="obj">要更新的对象</param>
        /// <returns>INSERT SQL</returns>
        public static string GetUpdateSql(object obj,string tableName)
        {
            if (obj == null || tableName == null) return null;

            Type t = obj.GetType();
            StringBuilder sb2 = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            sb.Append("update ").Append(tableName).Append(" set ");
            string sql = null;
            PropertyInfo[] pis = t.GetProperties();
            bool isExists = false;  //是否存在非空字段
            if (pis != null && pis.Length > 0)
            {
                foreach (PropertyInfo p in pis)
                {
                    Type pt = p.PropertyType;           //字段类型
                    if (IsBaseDataType(pt) && NotPageMember(p.Name))
                    {
                        object o = p.GetValue(obj, null);   //字段值
                        if (p.Name == "id")
                        {
                            if (o == null)
                            {
                                throw new Exception("主键ID不能为空");
                            }
                            sb2.Append(" where ").Append(p.Name).Append("=@").Append(p.Name);
                        }
                        else
                        {

                            //字段值不为空时拼接到插入SQL字符串中
                            if (o != null)
                            {
                                isExists = true;
                                sb.Append(p.Name).Append("=@").Append(p.Name).Append(",");
                            }
                        }
                    }
                }
                if (isExists)
                {
                    string a = sb.ToString().Substring(0, sb.Length - 1);
                    sql = a + sb2.ToString();
                }
            }

            return sql;
        }

        /// <summary>
        /// 获取更新参数列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static SqlParameter[] GetUpdateParamLsit(object obj)
        {
            if (obj == null) return null;
            List<SqlParameter> result = new List<SqlParameter>();
            Type t = obj.GetType();
            object id = null;
            PropertyInfo[] pis = t.GetProperties();
            if (pis != null && pis.Length > 0)
            {
                foreach (PropertyInfo p in pis)
                {
                    Type pt = p.PropertyType;           //字段类型
                    if (IsBaseDataType(pt) && NotPageMember(p.Name))
                    {
                        object o = p.GetValue(obj, null);   //字段值
                        if (p.Name == "id")
                        {
                            id = o;
                        }
                        else
                        {
                            //字段值不为空时拼接到插入SQL字符串中
                            if (o != null)
                            {
                                result.Add(new SqlParameter("@" + p.Name, o));
                            }
                        }
                    }
                }
                result.Add(new SqlParameter("@id", id));
            }
            return result.ToArray();
        }

        /// <summary>
        /// 获取参数列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static SqlParameter[] GetParamLsit( object obj)
        {
            if (obj == null) return null;

            List<SqlParameter> result = new List<SqlParameter>();

            Type t = obj.GetType();

            PropertyInfo[] pis = t.GetProperties();
            if (pis != null && pis.Length > 0)
            {
                foreach (PropertyInfo p in pis)
                {
                    Type pt = p.PropertyType;           //字段类型
                    if (IsBaseDataType(pt) && NotPageMember(p.Name))
                    {
                        object o = p.GetValue(obj, null);   //字段值
                        //字段值不为空时拼接到插入SQL字符串中
                        if (o != null)
                        {
                            result.Add(new SqlParameter("@" + p.Name, o));
                        }
                    }
                }
            }
            return result.ToArray();
        }
    }
}
