﻿using System;
using System.Text.RegularExpressions;
using System.Web;


namespace Library
{
    /**/
    /// <summary>
    /// SqlKey 的摘要说明。
    /// </summary>
    public class SqlKey
    {
        private HttpRequest request;
       // private const string StrKeyWord = @"select|insert|delete|from|count(|drop table|update|truncate|asc(|mid(|char(|xp_cmdshell|exec master|netlocalgroup administrators|:|net user|""|or|and";
      //  private const string StrRegex = @"-|;|,|/|(|)|[|]|}|{|%|@|*|!|'";
        private const string StrKeyWord = @"select|insert|delete|create|alter|from|count(|drop table|update|truncate|asc(|mid(|char(|xp_cmdshell|exec master|netlocalgroup administrators|:|net user|""|or|and|src|href|script|iframe";
        private const string StrRegex = @";|,|(|)|[|]|}|{|%|*|!|'|<|>|create|alter";
        public SqlKey(System.Web.HttpRequest _request)
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
            this.request = _request;
        }

        /**/
        /// <summary>
        /// 只读属性 SQL关键字
        /// </summary>
        public static string KeyWord
        {
            get
            {
                return StrKeyWord;
            }
        }
        /**/
        /// <summary>
        /// 只读属性过滤特殊字符
        /// </summary>
        public static string RegexString
        {
            get
            {
                return StrRegex;
            }
        }
        /**/
        /// <summary>
        /// 检查URL参数中是否带有SQL注入可能关键字。
        /// </summary>
        /// <param name="_request">当前HttpRequest对象</param>
        /// <returns>存在SQL注入关键字true存在，false不存在</returns>
        public bool CheckRequestQuery()
        {
            if (request.QueryString.Count != 0)
            {
                //若URL中参数存在，逐个比较参数。
                foreach (string i in this.request.QueryString)
                {
                    // 检查参数值是否合法。
                    if (i == "__VIEWSTATE") continue;
                    if (i == "__EVENTVALIDATION") continue;
                    if (CheckKeyWord(request.QueryString[i].ToString()))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /**/
        /// <summary>
        /// 检查提交表单中是否存在SQL注入可能关键字
        /// </summary>
        /// <param name="_request">当前HttpRequest对象</param>
        /// <returns>存在SQL注入关键字true存在，false不存在</returns>
        public bool CheckRequestForm()
        {
            if (request.Form.Count > 0)
            {

                //获取提交的表单项不为0 逐个比较参数
                foreach (string i in this.request.Form)
                {
                    if (i == "__VIEWSTATE") continue;
                    if (i == "__EVENTVALIDATION") continue;
                    //检查参数值是否合法
                    if (CheckKeyWord(request.Form[i]))
                    {
                        //存在SQL关键字
                        return true;

                    }
                }
            }
            return false;
        }

        /**/
        /// <summary>
        /// 静态方法，检查_sword是否包涵SQL关键字
        /// </summary>
        /// <param name="_sWord">被检查的字符串</param>
        /// <returns>存在SQL关键字返回true，不存在返回false</returns>
        public static bool CheckKeyWord(string _sWord)
        {
            string word = _sWord;
            string[] patten1 = StrKeyWord.Split('|');
            string[] patten2 = StrRegex.Split('|');
            foreach (string i in patten1)
            {
                if (word.ToLower().Contains(" " + i) || word.ToLower().Contains(i + " "))
                {
                    return true;
                }
            }
            foreach (string i in patten2)
            {
                if (word.ToLower().Contains(i))
                {
                    return true;
                }
            }
            return false;
        }

        /**/
        /// <summary>
        /// 反SQL注入:返回1无注入信息，否则返回错误处理
        /// </summary>
        /// <returns>返回1无注入信息，否则返回错误处理</returns>
        public string CheckMessage()
        {
            string msg = "1";
            if (CheckRequestQuery()) //CheckRequestQuery() || CheckRequestForm()
            {
                //msg = "<span style='font-size:24px;'>非法操作！<br>";
                //msg += "操作IP：" + request.ServerVariables["REMOTE_ADDR"] + "<br>";
                //msg += "操作时间：" + DateTime.Now + "<br>";
                //msg += "页面：" + request.ServerVariables["URL"].ToLower() + "<br>";
                //msg += "<a href="#" onclick="history.back()">返回上一页</a></span>";
            }
            return msg.ToString();
        }
    }

}
