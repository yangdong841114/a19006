﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 下载专区业务逻辑接口
    /// </summary>
    public interface IDownLoadFileBLL
    {
  
        /// <summary>
        /// 保存下载专区记录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        int Save(DownLoadFile model,Member current);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        PageResult<DownLoadFile> GetListPage(DownLoadFile model);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);
    }
}
