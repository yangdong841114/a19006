﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface ICurrencySumDetailsBLL : IBaseBLL<CurrencySumDetails>
    {
        PageResult<CurrencySumDetails> GetListPage(CurrencySumDetails model);
    }
}
