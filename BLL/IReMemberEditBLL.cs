﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 修改推荐关系业务逻辑接口
    /// </summary>
    public interface IReMemberEditBLL : IBaseBLL<ReMemberEdit>
    {
        /// <summary>
        /// 保存修改推荐关系记录
        /// </summary>
        /// <param name="mb">实体对象</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        ReMemberEdit SaveReMemberEdit(ReMemberEdit mb, Member current);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="fields">要查询的列</param>
        /// <returns></returns>
        PageResult<ReMemberEdit> GetListPage(ReMemberEdit model);

    }
}
