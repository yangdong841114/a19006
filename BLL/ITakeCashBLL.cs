﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 会员提现业务逻辑接口
    /// </summary>
    public interface ITakeCashBLL : IBaseBLL<TakeCash>
    {

        /// <summary>
        /// 获取已审核提现总金额
        /// </summary>
        /// <returns></returns>
        double GetAuditMoney();

        /// <summary>
        /// 获取待审核提现总金额
        /// </summary>
        /// <returns></returns>
        double GetNoAduditMoney();

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<TakeCash> GetListPage(TakeCash model);

        /// <summary>
        /// 保存会员提现申请
        /// </summary>
        /// <param name="model">保存数据</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int SaveTakeCash(TakeCash model, Member current);

        /// <summary>
        /// 取消提现申请
        /// </summary>
        /// <param name="id">提现申请ID</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int UpdateCancel(int id, Member current);


        /// <summary>
        /// 审核提现申请
        /// </summary>
        /// <param name="id">提现申请ID</param>
        /// <param name="current">当前登录用户</param>
        /// <returns></returns>
        int UpdateAudit(int id, Member current);


        /// <summary>
        /// 导出查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        DataTable GetExcelListPage(TakeCash model);

        /// <summary>
        /// 导出提现excel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        DataTable GetTakeCashExcel();
    }
}
