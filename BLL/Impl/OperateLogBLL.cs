﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class OperateLogBLL : IOperateLogBLL
    {

        private System.Type type = typeof(OperateLog);
        public IBaseDao dao { get; set; }

        public PageResult<OperateLog> GetListPage(OperateLog model, Member current)
        {
            PageResult<OperateLog> page = new PageResult<OperateLog>();
            string sql = "";
            string countSql = "";
            if (current.id == 2)
            {
                 sql = "select m.*,b.userName,row_number() over(order by m.oid desc) rownumber from OperateLog m inner join Member b on m.uid = b.id where 1=1 ";
                 countSql = "select count(1) from OperateLog m inner join Member b on m.uid = b.id where 1=1 ";
            }
            else
            {
                 sql = "select m.*,b.userName,row_number() over(order by m.oid desc) rownumber from OperateLog m inner join Member b on m.uid = b.id where 1=1 and m.recordName<>'后台修改流水' ";
                 countSql = "select count(1) from OperateLog m inner join Member b on m.uid = b.id where 1=1 and m.recordName<>'后台修改流水' ";
            }
            
            List<DbParameterItem> param = new List<DbParameterItem>();

            //如果不是超级管理员不能查询超级管理员的登录日志
            if (ConstUtil.SUPER_ADMIN != current.userId)
            {
                param.Add(new DbParameterItem("m.userId", ConstUtil.NEQ, ConstUtil.SUPER_ADMIN));
            }
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.recordName))
                {
                    param.Add(new DbParameterItem("m.recordName", ConstUtil.EQ, model.recordName));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<OperateLog> list = new List<OperateLog>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((OperateLog)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public PageResult<OperateLog> GetListType(Member current)
        {
            PageResult<OperateLog> page = new PageResult<OperateLog>();
            string sql="";
            if (current.id == 2)
            {
                 sql= " select recordName from OperateLog  group by recordName ";
            }
            else
            {
                sql = " select recordName from OperateLog where  recordName<>'后台修改流水'  group by recordName ";
            }
            List<DbParameterItem> param = new List<DbParameterItem>();

            DataTable dt = dao.GetList(sql, param, false);
            List<OperateLog> list = new List<OperateLog>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((OperateLog)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }
    }
}
