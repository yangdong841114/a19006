﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class LockRecordBLL : BaseBLL<LockRecord>, ILockRecordBLL
    {

        private System.Type type = typeof(LockRecord);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }


        public override IBaseDao GetDao()
        {
            return dao;
        }

        public void ExecSql(string sql)
        {
            dao.ExecuteBySql(sql);
        }

        public new LockRecord GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            LockRecord mb = (LockRecord)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new LockRecord GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            LockRecord mb = (LockRecord)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<LockRecord> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<LockRecord> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<LockRecord>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LockRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<LockRecord> GetList(string sql)
        {
            List<LockRecord> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<LockRecord>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LockRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }
        public PageResult<LockRecord> GetListPage(LockRecord model, string key)
        {
            dao.ExecuteBySql(@"update LockRecord set LockTypeName=DataDictionary.name from DataDictionary where LockRecord.LockType=DataDictionary.id and LockTypeName is null");
            PageResult<LockRecord> page = new PageResult<LockRecord>();
            string sql = string.Format("select t.id,replace(dbo.nylnz(t.userId,'{0}'),substring(dbo.nylnz(t.userId,'{0}'),LEN(dbo.nylnz(t.userId,'{0}'))/3,LEN(dbo.nylnz(t.userId,'{0}'))/3),'********') as userId,t.OrderId,dbo.nylnz(t.fromAddress,'{0}') as fromAddress,t.HASH,t.pocValue,t.addTime,t.getTime,t.flag,t.remark,t.LockType,t.LockTypeName,t.isEnd,t.haveSyMonth,t.haveFhMonth,t.maxSfAmounts,t.maxFhAmounts,row_number() over(order by t.id desc) rownumber from LockRecord t  where 1=1 ", key);
            string countSql = "select count(1) from LockRecord t  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE_NY, model.userId));
                }

                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("t.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("t.flag", ConstUtil.EQ, model.flag));
                }
                if (!ValidateUtils.CheckIntZero(model.LockType))
                {
                    param.Add(new DbParameterItem("t.LockType", ConstUtil.EQ, model.LockType));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.getTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("t.getTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true, key);

            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));
            param.Add(new DbParameterItem("key", null, key));

            DataTable dt = dao.GetPageList(sql, param, key);
            List<LockRecord> list = new List<LockRecord>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LockRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;

            //汇总
            dt = dao.GetList(sql, param_nopage, true);
            double all_pocValue = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    LockRecord model_row = (LockRecord)ReflectionUtil.GetModel(type, row);
                    all_pocValue += model_row.pocValue.Value;
                }
            }
            LockRecord foot_model = new LockRecord();
            list = new List<LockRecord>();
            foot_model.LockTypeName = "合计";
            foot_model.pocValue = all_pocValue;
            foot_model.HASH = "";
            list.Add(foot_model);
            page.footer = list;


            return page;
        }
        public PageResult<LockRecord> GetListPage(LockRecord model)
        {
            dao.ExecuteBySql(@"update LockRecord set LockTypeName=DataDictionary.name from DataDictionary where LockRecord.LockType=DataDictionary.id and LockTypeName is null");
            PageResult<LockRecord> page = new PageResult<LockRecord>();
            string sql = "select t.*,row_number() over(order by t.id desc) rownumber from LockRecord t  where 1=1 ";
            string countSql = "select count(1) from LockRecord t  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                }

                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("t.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("t.flag", ConstUtil.EQ, model.flag));
                }
                if (!ValidateUtils.CheckIntZero(model.LockType))
                {
                    param.Add(new DbParameterItem("t.LockType", ConstUtil.EQ, model.LockType));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.getTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("t.getTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<LockRecord> list = new List<LockRecord>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LockRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;

            //汇总
            dt = dao.GetList(sql, param_nopage, true);
            double all_pocValue = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    LockRecord model_row = (LockRecord)ReflectionUtil.GetModel(type, row);
                    all_pocValue += model_row.pocValue.Value;
                }
            }
            LockRecord foot_model = new LockRecord();
            list = new List<LockRecord>();
            foot_model.LockTypeName = "合计";
            foot_model.pocValue = all_pocValue;
            foot_model.HASH = "";
            list.Add(foot_model);
            page.footer = list;


            return page;
        }
        public void AutoCurrencyToPoc(string addTime, int id, string key)
        {
            var sql = string.Format("select * from CurrencySumDetails where id = {0}", id);
            DataTable dt_Currency = dao.GetList(sql);
            var userId = dt_Currency.Rows[0]["userId"];
            if (dt_Currency.Rows.Count > 0)
            {
                dao.ExecuteBySql(string.Format("update Currency set LockTypeName = '正常下发', apiLog = '运行时间-{0}' where addDate = '{1}' and userId = '{2}'", DateTime.Now.ToString(), addTime, userId));
                dao.ExecuteBySql(string.Format("update CurrencySumDetails set Logs = '运行时间-{0}' where id = {1}", DateTime.Now.ToString(), id));
                foreach (DataRow item in dt_Currency.Rows)
                {
                    var orderId = "A19006_" + item["id"].ToString();
                    var projectId = "-1";
                    var address = item["userId"].ToString();
                    var epoints = item["sumPoc"].ToString();
                    string poc_back = Common.JhInterface.pocGateway(orderId, projectId, address, epoints);
                    if (poc_back.IndexOf("success") == -1)
                    {
                        dao.ExecuteBySql(string.Format("update Currency set apiLog = apiLog + '-POC链上提交失败" + poc_back + DateTime.Now.ToString() + "' where addDate = '{0}' and dbo.nylnz(userId,'{1}') = '{2}'", addTime, key, address));
                        dao.ExecuteBySql(string.Format("update CurrencySumDetails set Logs = Logs + '-POC链上提交失败" + poc_back + DateTime.Now.ToString() + "' where addTime = '{0}' and userId = '{1}'", addTime, address));
                    }
                    else
                    {
                        dao.ExecuteBySql(string.Format("update Currency set apiLog = apiLog + '-POC链上提交成功" + DateTime.Now.ToString() + "' where addDate = '{0}' and dbo.nylnz(userId,'{1}') = '{2}'", addTime, key, address));
                        dao.ExecuteBySql(string.Format("update CurrencySumDetails set isStatus = 1, isAdmin = 0, Logs = Logs + '-POC链上提交成功" + DateTime.Now.ToString() + "' where addTime = '{0}' and userId = '{1}'", addTime, address));
                    }
                }
            }
        }
        public void CallbackPoc(string orderId, string setDate, string hash, string key)
        {
            var sql = string.Format("select * from CurrencySumDetails where id = '{0}'", orderId);
            var dt = dao.GetList(sql);
            foreach (DataRow item in dt.Rows)
            {
                var address = item["userId"].ToString();
                var addDate = item["addTime"].ToString();
                //dao.ExecuteBySql(string.Format("update Currency set apiLog = apiLog + '-已回调通知成功" + poc_back + DateTime.Now.ToString() + "' where addDate = '{0}' and dbo.nylnz(userId,'{1}') = '{2}'", addTime, key, address));
                dao.ExecuteBySql(string.Format("update CurrencySumDetails set isStatus = 2, Logs = Logs + '-已回调通知成功" + hash + setDate + "' where id = '{0}' ", orderId));
                dao.ExecuteBySql(string.Format("update Currency set ff = 1, fftime = '{3}', apiLog = apiLog + '-已回调通知成功" + hash + setDate + "' where addDate = '{0}' and dbo.nylnz(userId,'{1}') = '{2}'", addDate, key, address, setDate));
            }
        }
        public void AutoCurrencyToPoc(string addTime, string key)
        {
            var sql = string.Format("select * from CurrencySumDetails where addTime = '{0}'", addTime);
            DataTable dt_Currency = dao.GetList(sql);

            if (dt_Currency.Rows.Count > 0)
            {
                dao.ExecuteBySql(string.Format("update Currency set LockTypeName = '正常下发',apiLog = '运行时间-{0}' where addDate = '{1}'", DateTime.Now.ToString(), addTime));
                dao.ExecuteBySql(string.Format("update CurrencySums set isStatus = 1 where addTime = '{0}'", addTime));
                dao.ExecuteBySql(string.Format("update CurrencySumDetails set Logs = '运行时间-{0}' where addTime = '{1}'", DateTime.Now.ToString(), addTime));
                foreach (DataRow item in dt_Currency.Rows)
                {
                    var orderId = "A19006_" + item["id"].ToString();
                    var projectId = "-1";
                    var address = item["userId"].ToString();
                    var epoints = item["sumPoc"].ToString();
                    string poc_back = Common.JhInterface.pocGateway(orderId, projectId, address, epoints);
                    if (poc_back.IndexOf("success") == -1)
                    {
                        dao.ExecuteBySql(string.Format("update Currency set apiLog = apiLog + '-POC链上提交失败" + poc_back + DateTime.Now.ToString() + "' where addDate = '{0}' and dbo.nylnz(userId,'{1}') = '{2}'", addTime, key, address));
                        dao.ExecuteBySql(string.Format("update CurrencySumDetails set Logs = Logs + '-POC链上提交失败" + poc_back + DateTime.Now.ToString() + "' where addTime = '{0}' and userId = '{1}'", addTime, address));
                    }
                    else
                    {
                        dao.ExecuteBySql(string.Format("update Currency set apiLog = apiLog + '-POC链上提交成功" + DateTime.Now.ToString() + "' where addDate = '{0}' and dbo.nylnz(userId,'{1}') = '{2}'", addTime, key, address));
                        dao.ExecuteBySql(string.Format("update CurrencySumDetails set isStatus = 1, isAdmin = 2, Logs = Logs + '-POC链上提交成功" + DateTime.Now.ToString() + "' where addTime = '{0}' and userId = '{1}'", addTime, address));
                    }
                }
            }
        }
        //自动转帐奖金表中的数据自动提交POC转帐
        public void AutoCurrencyToPoc()
        {
            DataTable dt_Currency = dao.GetList("select top 1 * from Currency where ff=0 and userId in (select userId from Member) and userId in (select userId from LockRecord) order by id asc");
            double Bonus = 0;
            string Pockg = "";
            if (dt_Currency.Rows.Count > 0)
            {

                Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("pocLockMysf7", "pocLockMysf6", "pocLockMyfh6", "pocLockMysf5", "pocLockMyfh5", "pocLockMysf4", "pocLockMyfh4", "pocLockMysf3", "pocLockMyfh3", "pocLockMysf2", "pocLockMysf1", "Pockg");
                double pocLockMysf7 = Convert.ToDouble(param["pocLockMysf7"].paramValue);
                double pocLockMysf6 = Convert.ToDouble(param["pocLockMysf6"].paramValue);
                double pocLockMyfh6 = Convert.ToDouble(param["pocLockMyfh6"].paramValue);
                double pocLockMysf5 = Convert.ToDouble(param["pocLockMysf5"].paramValue);
                double pocLockMyfh5 = Convert.ToDouble(param["pocLockMyfh5"].paramValue);
                double pocLockMysf4 = Convert.ToDouble(param["pocLockMysf4"].paramValue);
                double pocLockMyfh4 = Convert.ToDouble(param["pocLockMyfh4"].paramValue);
                double pocLockMysf3 = Convert.ToDouble(param["pocLockMysf3"].paramValue);
                double pocLockMyfh3 = Convert.ToDouble(param["pocLockMyfh3"].paramValue);
                double pocLockMysf2 = Convert.ToDouble(param["pocLockMysf2"].paramValue);
                double pocLockMysf1 = Convert.ToDouble(param["pocLockMysf1"].paramValue);
                Pockg = param["Pockg"].paramValue;
                int LockType = int.Parse(dt_Currency.Rows[0]["LockType"].ToString());
                DataTable dt_LockRecord = dao.GetList("select pocValue from LockRecord where id=" + dt_Currency.Rows[0]["infoId"].ToString());
                double pocValue = double.Parse(dt_LockRecord.Rows[0]["pocValue"].ToString());

                if (LockType == 40) Bonus = pocValue * pocLockMysf7 / 100;//个体用户
                if (LockType == 41) Bonus = (pocLockMysf6 + pocLockMyfh6) * 10000;//守护者
                if (LockType == 42) Bonus = (pocLockMysf5 + pocLockMyfh5) * 10000;//忠诚节点
                if (LockType == 43) Bonus = (pocLockMysf4 + pocLockMyfh4) * 10000;//高级节点
                if (LockType == 44) Bonus = (pocLockMysf3 + pocLockMyfh3) * 10000;//超级节点
                if (LockType == 86) Bonus = (pocLockMysf2 + 0) * 10000;//POC技术团队
                if (LockType == 87) Bonus = (pocLockMysf1 + 0) * 10000;//POC创始团队

                Bonus = Bonus + 10;//编差可以十

            }
            if (dt_Currency.Rows.Count > 0 && double.Parse(dt_Currency.Rows[0]["epoints"].ToString()) <= Bonus && Pockg == "开")
            {
                dao.ExecuteBySql("update Currency set LockTypeName='正常下发',apiLog='运行时间-" + DateTime.Now.ToString() + "' where id=" + dt_Currency.Rows[0]["id"].ToString());
                string poc_back = "";
                poc_back = Common.JhInterface.pocGateway("A19006_" + dt_Currency.Rows[0]["id"].ToString(), "A19006", "自动生成", dt_Currency.Rows[0]["pocAddress"].ToString(), dt_Currency.Rows[0]["epoints"].ToString(), "POC");
                if (poc_back.IndexOf("success") == -1)
                {
                    dao.ExecuteBySql("update Currency set apiLog=apiLog+'-POC兑现网关失败" + poc_back + DateTime.Now.ToString() + "' where id=" + dt_Currency.Rows[0]["id"].ToString());
                }
                else
                {
                    string txhash = "";
                    if (poc_back.IndexOf("txhash") != -1)
                        txhash = poc_back.Substring(poc_back.IndexOf("txhash"));
                    if (txhash.Length > 100)
                        txhash = txhash.Substring(0, 95);
                    dao.ExecuteBySql("update Currency set  ff=1,  apiLog=apiLog+'-POC成功" + txhash + DateTime.Now.ToString() + "' where id=" + dt_Currency.Rows[0]["id"].ToString());
                    //ff=1等回调函数发送过来再更新。
                }
            }
        }

        public void SdBonus()
        {
            dao.ExecuteBySql("exec LockBonus 1;");
        }

        public int Save(LockRecord model, Member current)
        {
            model.addTime = DateTime.Now;
            string cf = "";
            LockRecord ca = this.GetOne("select * from LockRecord where OrderId='" + model.OrderId + "'");
            if (ca != null)
            {
                cf = " 重复：fromAddress-" + model.fromAddress + " pocValue-" + model.pocValue + " getTime-" + model.getTime.ToString();
                model = ca;
                model.remark += cf;
            }
            if (ca == null)
                model.flag = -1;//默认无标准
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("pocLockScrs1", "pocLockScsl1", "pocLockScrs2", "pocLockScsl2", "pocLockScrs3", "pocLockScsl3", "pocLockScrs4", "pocLockScsl4", "pocLockScrs5", "pocLockScsl5", "pocLockScrs6", "pocLockScsl6", "pocLockScrs7", "pocLockScsl7", "pocLockMysf7", "pocLockMysf6", "pocLockMyfh6", "pocLockMysf5", "pocLockMyfh5", "pocLockMysf4", "pocLockMyfh4", "pocLockMysf3", "pocLockMyfh3", "pocLockMysf2", "pocLockMysf1", "pocLockSyyssf1", "pocLockSyyssf2", "pocLockSyysfh3", "pocLockSyyssf3", "pocLockSyysfh4", "pocLockSyyssf4", "pocLockSyysfh5", "pocLockSyyssf5", "pocLockSyysfh6", "pocLockSyyssf6", "pocLockSyyssf7");
            int pocLockScrs1 = Convert.ToInt32(param["pocLockScrs1"].paramValue);
            double pocLockScsl1 = Convert.ToDouble(param["pocLockScsl1"].paramValue) * 10000;
            int pocLockScrs2 = Convert.ToInt32(param["pocLockScrs2"].paramValue);
            double pocLockScsl2 = Convert.ToDouble(param["pocLockScsl2"].paramValue) * 10000;
            int pocLockScrs3 = Convert.ToInt32(param["pocLockScrs3"].paramValue);
            double pocLockScsl3 = Convert.ToDouble(param["pocLockScsl3"].paramValue) * 10000;
            int pocLockScrs4 = Convert.ToInt32(param["pocLockScrs4"].paramValue);
            double pocLockScsl4 = Convert.ToDouble(param["pocLockScsl4"].paramValue) * 10000;
            int pocLockScrs5 = Convert.ToInt32(param["pocLockScrs5"].paramValue);
            double pocLockScsl5 = Convert.ToDouble(param["pocLockScsl5"].paramValue) * 10000;
            int pocLockScrs6 = Convert.ToInt32(param["pocLockScrs6"].paramValue);
            double pocLockScsl6 = Convert.ToDouble(param["pocLockScsl6"].paramValue) * 10000;
            int pocLockScrs7 = Convert.ToInt32(param["pocLockScrs7"].paramValue);
            double pocLockScsl7 = Convert.ToDouble(param["pocLockScsl7"].paramValue) * 10000;
            double pocLockMysf7 = Convert.ToDouble(param["pocLockMysf7"].paramValue);
            double pocLockMysf6 = Convert.ToDouble(param["pocLockMysf6"].paramValue);
            double pocLockMyfh6 = Convert.ToDouble(param["pocLockMyfh6"].paramValue);
            double pocLockMysf5 = Convert.ToDouble(param["pocLockMysf5"].paramValue);
            double pocLockMyfh5 = Convert.ToDouble(param["pocLockMyfh5"].paramValue);
            double pocLockMysf4 = Convert.ToDouble(param["pocLockMysf4"].paramValue);
            double pocLockMyfh4 = Convert.ToDouble(param["pocLockMyfh4"].paramValue);
            double pocLockMysf3 = Convert.ToDouble(param["pocLockMysf3"].paramValue);
            double pocLockMyfh3 = Convert.ToDouble(param["pocLockMyfh3"].paramValue);
            double pocLockMysf2 = Convert.ToDouble(param["pocLockMysf2"].paramValue);
            double pocLockMysf1 = Convert.ToDouble(param["pocLockMysf1"].paramValue);
            double pocLockSyyssf1 = Convert.ToDouble(param["pocLockSyyssf1"].paramValue);
            double pocLockSyyssf2 = Convert.ToDouble(param["pocLockSyyssf2"].paramValue);
            double pocLockSyysfh3 = Convert.ToDouble(param["pocLockSyysfh3"].paramValue);
            double pocLockSyyssf3 = Convert.ToDouble(param["pocLockSyyssf3"].paramValue);
            double pocLockSyysfh4 = Convert.ToDouble(param["pocLockSyysfh4"].paramValue);
            double pocLockSyyssf4 = Convert.ToDouble(param["pocLockSyyssf4"].paramValue);
            double pocLockSyysfh5 = Convert.ToDouble(param["pocLockSyysfh5"].paramValue);
            double pocLockSyyssf5 = Convert.ToDouble(param["pocLockSyyssf5"].paramValue);
            double pocLockSyysfh6 = Convert.ToDouble(param["pocLockSyysfh6"].paramValue);
            double pocLockSyyssf6 = Convert.ToDouble(param["pocLockSyyssf6"].paramValue);
            double pocLockSyyssf7 = Convert.ToDouble(param["pocLockSyyssf7"].paramValue);
            int newId = 0;
            if (model.id > 0)
            {
                dao.Update(model);
            }
            else
            {
                if (pocLockScsl1 == model.pocValue.Value && dao.GetList("select * from Member where pocLockScbs1>0 and id!=" + model.uid).Rows.Count < pocLockScrs1)//数量等于锁仓数量,人数(不含此会员)小于锁仓人数
                {
                    model.LockType = 87;
                    model.flag = 1;//有标准
                    //会员累计信息增加
                    dao.ExecuteBySql("update Member set pocLockScbs1=isnull(pocLockScbs1,0)+1,pocLockScpoc1=isnull(pocLockScpoc1,0)+" + model.pocValue + " where userId='" + model.userId + "'");
                    dao.ExecuteBySql("update DataDictionary set pocLockScbs=isnull(pocLockScbs,0)+1,pocLockScpoc=isnull(pocLockScpoc,0)+" + model.pocValue + " where id=" + model.LockType);
                }
                if (pocLockScsl2 == model.pocValue.Value && dao.GetList("select * from Member where pocLockScbs2>0 and id!=" + model.uid).Rows.Count < pocLockScrs2)//数量等于锁仓数量,人数(不含此会员)小于锁仓人数
                {
                    model.LockType = 86;
                    model.flag = 1;//有标准
                    //会员累计信息增加
                    dao.ExecuteBySql("update Member set pocLockScbs2=isnull(pocLockScbs2,0)+1,pocLockScpoc2=isnull(pocLockScpoc2,0)+" + model.pocValue + " where userId='" + model.userId + "'");
                    dao.ExecuteBySql("update DataDictionary set pocLockScbs=isnull(pocLockScbs,0)+1,pocLockScpoc=isnull(pocLockScpoc,0)+" + model.pocValue + " where id=" + model.LockType);
                }
                if (pocLockScsl3 == model.pocValue.Value && dao.GetList("select * from Member where pocLockScbs3>0 and id!=" + model.uid).Rows.Count < pocLockScrs3)//数量等于锁仓数量,人数(不含此会员)小于锁仓人数
                {
                    model.LockType = 44;
                    model.flag = 1;//有标准
                    //会员累计信息增加
                    dao.ExecuteBySql("update Member set pocLockScbs3=isnull(pocLockScbs3,0)+1,pocLockScpoc3=isnull(pocLockScpoc3,0)+" + model.pocValue + " where userId='" + model.userId + "'");
                    dao.ExecuteBySql("update DataDictionary set pocLockScbs=isnull(pocLockScbs,0)+1,pocLockScpoc=isnull(pocLockScpoc,0)+" + model.pocValue + " where id=" + model.LockType);
                }
                if (pocLockScsl4 == model.pocValue.Value && dao.GetList("select * from Member where pocLockScbs4>0 and id!=" + model.uid).Rows.Count < pocLockScrs4)//数量等于锁仓数量,人数(不含此会员)小于锁仓人数
                {
                    model.LockType = 43;
                    model.flag = 1;//有标准
                    //会员累计信息增加
                    dao.ExecuteBySql("update Member set pocLockScbs4=isnull(pocLockScbs4,0)+1,pocLockScpoc4=isnull(pocLockScpoc4,0)+" + model.pocValue + " where userId='" + model.userId + "'");
                    dao.ExecuteBySql("update DataDictionary set pocLockScbs=isnull(pocLockScbs,0)+1,pocLockScpoc=isnull(pocLockScpoc,0)+" + model.pocValue + " where id=" + model.LockType);
                }
                if (pocLockScsl5 == model.pocValue.Value && dao.GetList("select * from Member where pocLockScbs5>0 and id!=" + model.uid).Rows.Count < pocLockScrs5)//数量等于锁仓数量,人数(不含此会员)小于锁仓人数
                {
                    model.LockType = 42;
                    model.flag = 1;//有标准
                    //会员累计信息增加
                    dao.ExecuteBySql("update Member set pocLockScbs5=isnull(pocLockScbs5,0)+1,pocLockScpoc5=isnull(pocLockScpoc5,0)+" + model.pocValue + " where userId='" + model.userId + "'");
                    dao.ExecuteBySql("update DataDictionary set pocLockScbs=isnull(pocLockScbs,0)+1,pocLockScpoc=isnull(pocLockScpoc,0)+" + model.pocValue + " where id=" + model.LockType);
                }
                if (pocLockScsl6 == model.pocValue.Value && dao.GetList("select * from Member where pocLockScbs6>0 and id!=" + model.uid).Rows.Count < pocLockScrs6)//数量等于锁仓数量,人数(不含此会员)小于锁仓人数
                {
                    model.LockType = 41;
                    model.flag = 1;//有标准
                    //会员累计信息增加
                    dao.ExecuteBySql("update Member set pocLockScbs6=isnull(pocLockScbs6,0)+1,pocLockScpoc6=isnull(pocLockScpoc6,0)+" + model.pocValue + " where userId='" + model.userId + "'");
                    dao.ExecuteBySql("update DataDictionary set pocLockScbs=isnull(pocLockScbs,0)+1,pocLockScpoc=isnull(pocLockScpoc,0)+" + model.pocValue + " where id=" + model.LockType);
                }
                if (pocLockScsl7 == model.pocValue.Value && dao.GetList("select * from Member where pocLockScbs7>0 and id!=" + model.uid).Rows.Count < pocLockScrs7)//数量等于锁仓数量,人数(不含此会员)小于锁仓人数
                {
                    model.LockType = 40;
                    model.flag = 1;//有标准
                    //会员累计信息增加
                    dao.ExecuteBySql("update Member set pocLockScbs7=isnull(pocLockScbs7,0)+1,pocLockScpoc7=isnull(pocLockScpoc7,0)+" + model.pocValue + " where userId='" + model.userId + "'");
                    dao.ExecuteBySql("update DataDictionary set pocLockScbs=isnull(pocLockScbs,0)+1,pocLockScpoc=isnull(pocLockScpoc,0)+" + model.pocValue + " where id=" + model.LockType);
                }

                if (model.LockType == 40 && model.flag == 1)
                {
                    model.maxSfAmounts = model.pocValue.Value * pocLockMysf7 * pocLockSyyssf7 / 100;//个体用户
                    model.maxFhAmounts = 0;
                }
                if (model.LockType == 41 && model.flag == 1)
                {
                    model.maxSfAmounts = pocLockMysf6 * 10000 * pocLockSyyssf6;//守护者
                    model.maxFhAmounts = pocLockMyfh6 * 10000 * pocLockSyysfh6;
                }
                if (model.LockType == 42 && model.flag == 1)
                {
                    model.maxSfAmounts = pocLockMysf5 * 10000 * pocLockSyyssf5;//忠诚节点
                    model.maxFhAmounts = pocLockMyfh5 * 10000 * pocLockSyysfh5;
                }
                if (model.LockType == 43 && model.flag == 1)
                {
                    model.maxSfAmounts = pocLockMysf4 * 10000 * pocLockSyyssf4;//高级节点
                    model.maxFhAmounts = pocLockMyfh4 * 10000 * pocLockSyysfh4;
                }
                if (model.LockType == 44 && model.flag == 1)
                {
                    model.maxSfAmounts = pocLockMysf3 * 10000 * pocLockSyyssf3;//超级节点
                    model.maxFhAmounts = pocLockMyfh3 * 10000 * pocLockSyysfh3;
                }
                if (model.LockType == 86 && model.flag == 1)
                {
                    model.maxSfAmounts = pocLockMysf2 * 10000 * pocLockSyyssf2;//POC技术团队
                    model.maxFhAmounts = 0;
                }
                if (model.LockType == 87 && model.flag == 1)
                {
                    model.maxSfAmounts = pocLockMysf1 * 10000 * pocLockSyyssf1;//POC创始团队
                    model.maxFhAmounts = 0;
                }
                object o = dao.SaveByIdentity(model);
                newId = Convert.ToInt32(o);
            }
            return newId;
        }




    }
}
