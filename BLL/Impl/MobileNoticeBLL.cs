﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using System.Configuration;

namespace BLL.Impl
{
    public class MobileNoticeBLL :  IMobileNoticeBLL
    {
        private System.Type type = typeof(MobileNotice);
        public IBaseDao dao { get; set; }

        public MobileNotice GetModel(string code)
        {
            string sql = "select * from MobileNotice ";
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("code", ConstUtil.EQ, code))
                .Result();
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            MobileNotice mb = (MobileNotice)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public int UpdateList(List<MobileNotice> list)
        {
            DataTable dt = dao.GetList("select * from MobileNotice");
            if(list==null || list.Count==0){
                throw new ValidateException("要保存的内容为空");
            }
            foreach(MobileNotice ps in list){
                if(ValidateUtils.CheckNull(ps.msg)){
                    throw new ValidateException(ps.msg + "：的值为空");
                }
                DataRow[] rows = dt.Select("code='"+ps.code+"'");
                if(rows==null || rows.Length==0){
                    throw new ValidateException(ps.code+"没有找到数据");
                }

                rows[0]["flag"] = ps.flag;
                rows[0]["phone"] = ps.phone;
                rows[0]["msg"] = ps.msg;
            }
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("flag", DbType.Int32, null))
                .Add(new DbParameterItem("phone", DbType.String, null))
                .Add(new DbParameterItem("msg", DbType.String, null))
                .Add(new DbParameterItem("code", DbType.String, null))
                .Result();
            string sql = "update MobileNotice set flag=@flag,phone=@phone,msg=@msg where code=@code";
            DataSet set = new DataSet();
            set.Tables.Add(dt);
            dt.TableName = "MobileNotice";
            return dao.UpdateBatchByDataSet(set, "MobileNotice", sql, param);
        }

        public Dictionary<string, MobileNotice> GetToDictionary()
        {
            Dictionary<string, MobileNotice> rr = new Dictionary<string, MobileNotice>();
            string sql = "select * from MobileNotice ";

            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    MobileNotice pr = (MobileNotice)ReflectionUtil.GetModel(type, row);
                    rr.Add(pr.code,pr);
                }
            }
            return rr;
        }

        public string SendMessage(string phone, string msg)
        {
            if (!ValidateUtils.CheckNull(phone))
            {
                string uid = ConfigurationManager.AppSettings["winicLink"].Split(';')[0].Trim().Replace("uid=", "");
                string pwd = ConfigurationManager.AppSettings["winicLink"].Split(';')[1].Trim().Replace("pwd=", "");
                org.winic.service2.Service1 ss = new org.winic.service2.Service1();
                string result = NewSendMessages(uid, pwd, phone, msg, null);
            }
            return "success";
        }
        public string NewSendMessages(string uid, string pwd, string phone, string msg, string otime)
        {
            var result = Common.HttpSend.PostData("http://service.winic.org:8003/service.asmx/SendMessages?", "uid=" + uid + "&pwd=" + pwd + "&tos=" + phone + "&msg=" + msg + "&otime=");
            return "success";
        }

        public string SendMessage(List<string> phones, string msg)
        {
            if (phones != null && phones.Count > 0 && !ValidateUtils.CheckNull(msg))
            {
                StringBuilder sb = new StringBuilder();
                foreach(string phone in phones){
                    sb.Append(phone).Append(",");
                }
                string phs = sb.ToString().Substring(0, sb.Length - 1);
                string uid = ConfigurationManager.AppSettings["winicLink"].Split(';')[0].Trim().Replace("uid=", "");
                string pwd = ConfigurationManager.AppSettings["winicLink"].Split(';')[1].Trim().Replace("pwd=", "");
                org.winic.service2.Service1 ss = new org.winic.service2.Service1();
                string result = ss.SendMessages(uid, pwd, phs, msg, null);
            }
            return "success";
        }
    }
}
