﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace BLL.Impl
{
    public class MemberBLL : BaseBLL<Member>, IMemberBLL
    {
        private string allField = "[id],[userId],[userName],[nickName],[phone],[address],[email],[code]" +
                                  ",[qq],[province],[city],[area],[isLock],[isAgent],[agentIslock],[agentName],[applyAgentTime]" +
                                  ",[openAgentTime],[regAgentmoney],[shopid],[shopName],[uLevel],[rLevel],[treePlace]" +
                                  ",[fatherID],[fatherName],[pLevel],[pPath],[spare0],[spare1],[encash0],[encash1]" +
                                  ",[new0],[new1],[reId],[reName],[rePath],[reLevel],[reCount],[reTreeCount],[regMoney]" +
                                  ",[regNum],[isPay],[addTime],[passTime],[byopenId],[byopen],[empty],bankName,bankCard,bankAddress,bankUser";
        private System.Type type = typeof(Member);
        public IBaseDao dao { get; set; }

        public INewsBLL newsBLL { get; set; }

        public IParamSetBLL psBLL { get; set; }

        public ILoginHistoryBLL lhBll { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public IOrderBLL orderBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new Member GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            Member mb = (Member)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new Member GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Member mb = (Member)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public List<Member> GetMemberList(string sql, List<Common.DbParameterItem> param, bool isJoinSql)
        {
            List<Member> list = null;
            DataTable dt = dao.GetList(sql, param, isJoinSql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Member>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<Member> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<Member> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Member>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<Member> GetList(string sql)
        {
            List<Member> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Member>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public DataTable GetDataTable(string sql)
        {
            return dao.GetList(sql);
        }

        public int DeleteById(int id)
        {
            //调用存储过程
            string pro = "delMember";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@uid",SqlDbType.Int,4),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = id;
            p[1].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                return 1;
            }
            else
            {
                throw new ValidateException(result);
            }
        }

        public string OpenMember(int uid, int flag, Member current)
        {
            //调用存储过程
            string pro = "openMember";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@uid",SqlDbType.Int,4),
                  new SqlParameter("@empty",SqlDbType.Int,4),
                  new SqlParameter("@ipAddress",SqlDbType.VarChar,20),
                  new SqlParameter("@addUid",SqlDbType.Int,4),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = uid;
            p[1].Value = flag;
            p[2].Value = current.ipAddress;
            p[3].Value = current.id;
            p[4].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                //是否需要短信通知
                MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_OPEN_MEMBER);
                if (bf.flag == 1)
                {
                    Member m = this.GetModelById(uid);
                    //发送短信
                    noticeBLL.SendMessage(m.phone, m.userId + bf.msg);
                }
                return result;
            }
            else
            {
                throw new ValidateException(result);
            }
        }

        public ResponseDtoData loginUser(Model.Member m, int isMan)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (m == null) { response.msg = "登录信息不能为空"; }
            else if (ValidateUtils.CheckNull(m.userId) || ValidateUtils.CheckNull(m.password)) { response.msg = "用户名或密码不能为空"; }
            else
            {
                //提交的密码加密
                m.password = DESEncrypt.EncryptDES(m.password, ConstUtil.SALT);
                //查询用户
                string sql = "select *,(case when loginLockTime is not null and loginLockTime>getDate() then 1 else 0 end) isLoginLock," +
                             "datediff(minute,getDate(),isnull(loginLockTime,getDate())) remainingMinutes from member where isAdmin = " + isMan;
                List<DbParameterItem> pdb = ParamUtil.Get().Add(new DbParameterItem("userId", ConstUtil.EQ, m.userId)).Result();
                Member mb = this.GetOne(sql, pdb);

                if (mb == null)
                {
                    response.msg = "账户名或密码错误";
                }
                else
                {
                    //登录日志
                    //状态，0：成功，1：失败（帐号不存在），2：失败（密码验证失败），3：（失败）帐号未开通，4：（失败）帐号被冻结，5：（失败）帐号被登录锁定
                    LoginHistory log = new LoginHistory();
                    log.uid = mb.id;
                    log.userId = m.userId;
                    log.loginIp = m.ipAddress;
                    log.isMan = isMan;
                    log.flag = 0;
                    log.mulx = "登录成功";
                    log.loginTime = DateTime.Now;

                    if (mb.isLoginLock == 1) //如果账户被锁定，直接返回
                    {
                        response.msg = "账户已被锁定，请" + mb.remainingMinutes + "分钟后尝试";
                        log = null;
                    }
                    else if (m.password != mb.password)
                    {
                        log.flag = 2;
                        log.mulx = "密码验证失败";
                        response.msg = "账户名或密码错误";

                        //登录参数
                        List<string> list = new List<string>();
                        list.Add("loginFailTimes");
                        list.Add("loginLockTime");
                        Dictionary<string, ParameterSet> pd = psBLL.GetToDictionary(list);
                        int loginFailTimes = Convert.ToInt32(pd["loginFailTimes"].paramValue); //连续错误次数
                        int loginLockTime = Convert.ToInt32(pd["loginLockTime"].paramValue);   //锁定时长（分钟）
                        int last = (loginFailTimes - (mb.loginFailTimes.Value + 1)); //剩余次数

                        //一般情况，增加用户登录失败次数
                        string usql = "update Member set loginFailTimes=loginFailTimes+1 where id = " + mb.id;

                        //剩余2次时提醒
                        if (last <= 2 && last > 0)
                        {
                            response.msg = "密码错误，再失败" + last + "次后账户将被锁定";
                        }
                        //达到指定连续错误次数,锁定帐号
                        else if (last <= 0)
                        {
                            response.msg = "密码错误，账户已被锁定，请" + loginLockTime + "分钟后尝试";
                            //用户登录失败次数清0，锁定住用户
                            usql = "update Member set loginFailTimes = 0,loginLockTime = DATEADD(mi," + loginLockTime + ",getDate()) where id = " + mb.id;
                        }

                        dao.ExecuteBySql(usql);
                        log.mulx = response.msg;
                    }
                    else if (mb.isPay == 0)
                    {
                        log.flag = 3;
                        log.mulx = "验证通过，帐号未开通";
                        response.msg = "帐号不可用";
                    }
                    else if (mb.isLock == 1)
                    {
                        log.flag = 4;
                        log.mulx = "验证通过，帐号被冻结";
                        response.msg = "帐号不可用";
                    }
                    else
                    {
                        //返回的用户对象不应含有密码、安全问题信息
                        mb.password = null;
                        mb.threepass = null;
                        mb.passOpen = null;
                        mb.question = null;
                        mb.answer = null;

                        //清空登录失败次数
                        string usql = "update Member set loginFailTimes=0,loginLockTime = null where id = " + mb.id;
                        dao.ExecuteBySql(usql);
                        response.result = mb;
                        response.status = "success";
                    }
                    if (log != null)
                    {
                        //保存登录日志
                        lhBll.Save(log);
                    }
                }
            }
            return response;
        }

        public Member GetModelByIdNoPassWord(int memberId)
        {
            Member mm = GetModelById(memberId);
            if (mm != null)
            {
                mm.password = null;
                mm.threepass = null;
                mm.passOpen = null;
                mm.question = null;
                mm.answer = null;
            }
            return mm;
        }

        public Member GetModelAndAccountNoPass(int memberId)
        {
            Member mm = GetModelByIdNoPassWord(memberId);
            if (mm != null)
            {
                mm.account = accountBLL.GetModel(mm.id.Value);
            }
            return mm;
        }

        public Member GetModelById(int memberId)
        {
            List<DbParameterItem> pdb = ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.EQ, memberId)).Result();
            Member mm = this.GetOne("select * from member ", pdb);
            return mm;
        }

        public Member GetModelByUserId(string userId)
        {
            List<DbParameterItem> pdb = ParamUtil.Get().Add(new DbParameterItem("userId", ConstUtil.EQ, userId)).Result();
            return this.GetOne("select * from member ", pdb);
        }

        public Member GetModelByUserId(string userId, int isAdmin)
        {
            List<DbParameterItem> pdb = ParamUtil.Get()
                .Add(new DbParameterItem("userId", ConstUtil.EQ, userId))
                .Add(new DbParameterItem("isAdmin", ConstUtil.EQ, isAdmin)).Result();
            return this.GetOne("select * from member ", pdb);
        }

        public Member SaveMemberNoVerify(Member mb)
        {
            object o = this.SaveByIdentity(mb);
            int id = Convert.ToInt32(o);
            mb.id = id;
            return mb;
        }

        public Member SaveMember(Member mb)
        {
            string err = null;

            string reg = @"^1(3|4|5|7|8)\d{9}$";


            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "会员编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.password)) { err = "登录密码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.passOpen)) { err = "安全密码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.threepass)) { err = "交易密码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.fatherName)) { err = "接点人编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.reName)) { err = "推荐人编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.shopName)) { err = "报单中心不能为空"; }
            else if (ValidateUtils.CheckIntZero(mb.treePlace)) { err = "放置区域不能为空"; }
            else if (mb.orderItems == null || mb.orderItems.Count == 0) { err = "注册购买的商品不能为空"; }
            if (mb.sourceMachine != "app")
            {
                if (!Regex.IsMatch(mb.phone, reg)) { throw new ValidateException("手机号码格式错误"); }
                if (ValidateUtils.CheckNull(mb.bankUser)) { err = "开户名不能为空"; }
                else if (ValidateUtils.CheckNull(mb.bankCard)) { err = "银行卡号不能为空"; }
                else if (ValidateUtils.CheckNull(mb.bankAddress)) { err = "开户支行名称不能为空"; }
                else if (mb.bankName == null || mb.bankName == "0") { err = "请选择开户行"; }

            }
            if (err != null)
            {
                throw new ValidateException(err);
            }

            //检查用户名是否存在
            Member m = this.GetModelByUserId(mb.userId, 0);
            if (m != null) { throw new ValidateException("会员编码：" + m.userId + " 已经存在"); }

            //检查推荐人是否存在
            Member re = this.GetModelByUserId(mb.reName, 0);
            if (re == null) { throw new ValidateException("推荐人编码：" + mb.reName + " 不存在"); }
            if (re.isPay != 1) { throw new ValidateException("推荐人：" + mb.reName + " 未开通，不能成为推荐人 "); }
            mb.reId = re.id;
            mb.rePath = re.rePath + re.id + ",";
            mb.reLevel = re.reLevel + 1;

            //检查报单中心是否存在
            m = this.GetModelByUserId(mb.shopName, 0);
            if (m == null) { throw new ValidateException("报单中心：" + mb.shopName + " 不存在"); }
            if (m.agentIslock != 0) { throw new ValidateException("报单中心：" + mb.shopName + " 已冻结"); }
            mb.shopid = m.id;

            //检查接点人是否存在
            m = this.GetModelByUserId(mb.fatherName, 0);
            if (m == null) { throw new ValidateException("接点人编码：" + mb.fatherName + " 不存在"); }
            mb.pLevel = m.pLevel + 1;
            mb.fatherID = m.id;
            mb.pPath = m.pPath + m.id + ",";

            //放置区域是否占用
            mb.treePlace = mb.treePlace.Value == -1 ? 0 : mb.treePlace.Value;
            string placeName = mb.treePlace == 0 ? "左区" : "右区";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("fatherId", ConstUtil.EQ, mb.fatherID))
                                                         .Add(new DbParameterItem("treePlace", ConstUtil.EQ, mb.treePlace))
                                                         .Result();
            int count = this.GetCount("select count(1) from member ", param);
            if (count > 0) { throw new ValidateException("接点人编码：" + mb.fatherName + "下，" + placeName + "已被使用"); }

            //注册会员需放在推荐人扇下
            if (!m.pPath.Contains("," + re.id.Value + ",") && m.id != re.id)
            {
                throw new ValidateException("注册新会员需放在推荐人伞下");
            }

            //密码加密
            mb.password = DESEncrypt.EncryptDES(mb.password, ConstUtil.SALT);
            mb.passOpen = DESEncrypt.EncryptDES(mb.passOpen, ConstUtil.SALT);
            mb.threepass = DESEncrypt.EncryptDES(mb.threepass, ConstUtil.SALT);

            //获取每个级别注册金额
            Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("regPrice", "upUlevel1", "upUlevel2", "upUlevel3");
            double price = Convert.ToDouble(ps["regPrice"].paramValue); //每单注册金额
            int pknum = Convert.ToInt32(ps["upUlevel1"].paramValue);    //普卡注册单数
            int yknum = Convert.ToInt32(ps["upUlevel2"].paramValue);    //银卡注册单数
            int jknum = Convert.ToInt32(ps["upUlevel3"].paramValue);    //金卡注册单数
            double pkMoney = price * pknum;     //普卡注册金额
            double ykMoney = price * yknum;     //银卡注册金额
            double jkMoney = price * jknum;     //金卡注册金额


            //-------------------------------------------------------------------生成订单----------------------------------------------------------------//


            int totalNum = 0;       //总购买数量
            double totalMoney = 0;  //总购买金额

            //检查购买商品数量
            StringBuilder sb = new StringBuilder();
            foreach (OrderItem it in mb.orderItems)
            {
                sb.Append(it.productId).Append(",");
                if (ValidateUtils.CheckIntZero(it.num) || it.num < 0) { throw new ValidateException("您购买的商品【" + it.productName + "】还未录入购买数量"); }
                totalNum += it.num.Value;
            }

            //检查商品
            string sql = "select * from Product where id in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";
            DataTable proDt = dao.GetList(sql);
            if (proDt == null || proDt.Rows.Count == 0) { throw new ValidateException("您购买的商品为空"); }
            Dictionary<int, DataRow> prodi = new Dictionary<int, DataRow>();
            foreach (DataRow row in proDt.Rows)
            {
                prodi.Add(Convert.ToInt32(row["id"]), row);
            }
            List<OrderItem> items = new List<OrderItem>();
            foreach (OrderItem it in mb.orderItems)
            {
                if (!prodi.ContainsKey(it.productId.Value)) { throw new ValidateException("您购买的商品【" + it.productName + "】不存在"); }
                DataRow row = prodi[it.productId.Value];
                double proPrice = Convert.ToDouble(row["price"]); //商品单价
                int num = it.num.Value; //用户传入的购买数量
                double rowTotal = num * proPrice; //行金额
                OrderItem item = new OrderItem();
                item.productId = it.productId;
                item.productName = row["productName"].ToString();
                item.productNumber = row["productCode"].ToString();
                item.productName = row["productName"].ToString();
                item.price = proPrice;
                item.num = num;
                item.total = rowTotal;
                totalMoney += rowTotal;
                items.Add(item);
            }
            //订单信息
            OrderHeader oh = new OrderHeader();
            oh.status = 1;
            oh.typeId = 1;
            oh.payType = "现金";
            oh.userId = mb.userId;
            oh.phone = mb.phone;
            oh.receiptName = mb.userName;
            oh.provinceName = mb.province;
            oh.cityName = mb.city;
            oh.areaName = mb.area;
            oh.address = mb.address;
            oh.productNum = totalNum;
            oh.productMoney = totalMoney;
            oh.items = items;
            mb.regMoney = totalMoney;
            if (totalMoney < pkMoney) { throw new ValidateException("购买商品的金额不能成为普卡会员"); }

            mb.orderItems = null;

            //-------------------------------------------------------------------生成订单----------------------------------------------------------------//

            //判断什么会员等级
            if (mb.regMoney.Value >= pkMoney && mb.regMoney.Value < ykMoney)
            {
                mb.regMoney = pkMoney;
                mb.regNum = pknum;
                mb.uLevel = ConstUtil.ULEVEL_PK;
            }
            else if (mb.regMoney.Value >= ykMoney && mb.regMoney.Value < jkMoney)
            {
                mb.regMoney = ykMoney;
                mb.regNum = yknum;
                mb.uLevel = ConstUtil.ULEVEL_YK;
            }
            if (mb.regMoney.Value >= jkMoney)
            {
                mb.regMoney = jkMoney;
                mb.regNum = jknum;
                mb.uLevel = ConstUtil.ULEVEL_JK;
            }

            //其他默认信息
            mb.addTime = DateTime.Now;      //注册时间
            mb.loginFailTimes = 0;          //默认登录失败次数

            //保存会员
            mb.sourceMachine = null;
            object o = this.SaveByIdentity(mb);
            int id = Convert.ToInt32(o);
            mb.id = id;

            //默认保存一条账户记录
            MemberAccount account = new MemberAccount();
            account.id = id;
            accountBLL.SaveByIdentity(account);

            //保存注册订单
            oh.uid = id;
            oh.orderDate = DateTime.Now;
            string hid = orderBLL.SaveRegisterOrder(oh);
            sql = "update Member set orderId = '" + hid + "' where id = " + id;
            dao.ExecuteBySql(sql);

            if (id <= 0)
            {
                throw new ValidateException("注册会员失败，请联系管理员");
            }
            return mb;
        }
        public PageResult<Member> GetMemberListPage(Member model, string fields, string key)
        {
            PageResult<Member> page = new PageResult<Member>();
            fields = fields == null ? allField : fields;
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from Member m where id>3 ";
            string countSql = "select count(1) from Member where id>3 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.isAgent != null)
                {
                    param.Add(new DbParameterItem("isAgent", ConstUtil.EQ, model.isAgent));
                }
                if (model.uLevel != null)
                {
                    param.Add(new DbParameterItem("uLevel", ConstUtil.EQ, model.uLevel));
                }
                if (model.empty != null)
                {
                    param.Add(new DbParameterItem("empty", ConstUtil.EQ, model.empty));
                }
                if (!ValidateUtils.CheckIntZero(model.shopid))
                {
                    param.Add(new DbParameterItem("shopId", ConstUtil.EQ, model.shopid));
                }
                if (model.isPay != null)
                {
                    param.Add(new DbParameterItem("isPay", ConstUtil.EQ, model.isPay));
                }
                if (!ValidateUtils.CheckNull(model.reName))
                {
                    param.Add(new DbParameterItem("reName", ConstUtil.LIKE, model.reName));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE_NY, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckNull(model.phone))
                {
                    param.Add(new DbParameterItem("phone", ConstUtil.LIKE, model.phone));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if (model.passStartTime != null)
                {
                    param.Add(new DbParameterItem("passTime", ConstUtil.DATESRT_LGT_DAY, model.passStartTime));
                }
                if (model.passEndTime != null)
                {
                    param.Add(new DbParameterItem("passTime", ConstUtil.DATESRT_EGT_DAY, model.passEndTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true, key);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param, key);
            List<Member> list = new List<Member>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }
        public PageResult<Member> GetMemberListPage(Member model, string fields)
        {
            PageResult<Member> page = new PageResult<Member>();
            fields = fields == null ? allField : fields;
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from Member m where id>3 ";
            string countSql = "select count(1) from Member where id>3 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.isAgent != null)
                {
                    param.Add(new DbParameterItem("isAgent", ConstUtil.EQ, model.isAgent));
                }
                if (model.uLevel != null)
                {
                    param.Add(new DbParameterItem("uLevel", ConstUtil.EQ, model.uLevel));
                }
                if (model.empty != null)
                {
                    param.Add(new DbParameterItem("empty", ConstUtil.EQ, model.empty));
                }
                if (!ValidateUtils.CheckIntZero(model.shopid))
                {
                    param.Add(new DbParameterItem("shopId", ConstUtil.EQ, model.shopid));
                }
                if (model.isPay != null)
                {
                    param.Add(new DbParameterItem("isPay", ConstUtil.EQ, model.isPay));
                }
                if (!ValidateUtils.CheckNull(model.reName))
                {
                    param.Add(new DbParameterItem("reName", ConstUtil.LIKE, model.reName));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem(string.Format("dbo.nylnz(userId,'@9TIfezps$y#x1aKQooT')"), ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckNull(model.phone))
                {
                    param.Add(new DbParameterItem("phone", ConstUtil.LIKE, model.phone));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if (model.passStartTime != null)
                {
                    param.Add(new DbParameterItem("passTime", ConstUtil.DATESRT_LGT_DAY, model.passStartTime));
                }
                if (model.passEndTime != null)
                {
                    param.Add(new DbParameterItem("passTime", ConstUtil.DATESRT_EGT_DAY, model.passEndTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Member> list = new List<Member>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public PageResult<Member> GetAdminListPage(Member model, Member current)
        {
            PageResult<Member> page = new PageResult<Member>();
            string sql = "select m.id,m.userId,m.userName,m.addTime,t.nickName,row_number() over(order by m.id desc) rownumber from Member m left join " +
                         " (select memberId, nickName=stuff((select ','+l.roleName from RoleMember a inner join Role l on a.roleId = l.id where A.memberId=r.memberId for xml path('')), 1, 1, '') " +
                         " from RoleMember r group by memberId) t  on m.id = t.memberId" +
                         " where m.isAdmin = 1 ";
            string countSql = "select count(1) from Member m where m.isAdmin = 1 ";
            //非超级管理员不能查询超级管理员信息
            if (current.userId != ConstUtil.SUPER_ADMIN)
            {
                sql += " and m.userId<>'" + ConstUtil.SUPER_ADMIN + "'";
                countSql += " and m.userId<>'" + ConstUtil.SUPER_ADMIN + "'";
            }

            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.isPay != null)
                {
                    param.Add(new DbParameterItem("m.isPay", ConstUtil.EQ, model.isPay));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckNull(model.phone))
                {
                    param.Add(new DbParameterItem("m.phone", ConstUtil.LIKE, model.phone));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Member> list = new List<Member>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Member)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int UpdateLock(List<int> list, int isLock)
        {
            if (list == null || list.Count == 0)
            {
                throw new ValidateException("要更新的内容为空");
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                sb.Append(list[i]).Append(",");
            }
            if (isLock == 0 || isLock == 1)
            {
                string sql = "update member set isLock = @isLock where id in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";
                List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("isLock", null, isLock))
                               .Result();
                return dao.ExecuteBySql(sql, param);
            }
            else
            {
                //获取参数
                Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("loginPass", "passOpen", "threePass");
                string loginPass = ps["loginPass"].paramValue;
                string passOpen = ps["passOpen"].paramValue;
                string threePass = ps["threePass"].paramValue;

                string sql = "update member set password = @password,passOpen = @passOpen,threepass = @threepass  where id in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";

                List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("password", null, DESEncrypt.EncryptDES(loginPass, ConstUtil.SALT)))
                    .Add(new DbParameterItem("passOpen", null, DESEncrypt.EncryptDES(passOpen, ConstUtil.SALT)))
                    .Add(new DbParameterItem("threepass", null, DESEncrypt.EncryptDES(threePass, ConstUtil.SALT)))
                               .Result();
                return dao.ExecuteBySql(sql, param);
            }

        }

        public List<Member> GetDirectTreeList(string userId)
        {
            string sql = "select m.id,m.userId,m.userName,m.reId,m.reName,m.uLevel," +
                         "m.userId+'（'+isnull(m.userName,'')+'）：'+isnull(d.name,'')+'：'+CAST(isnull(m.reCount,0) as varchar(10)) dtreeName " +
                         "from Member m inner join DataDictionary d on m.uLevel = d.id where isAdmin = 0";
            if (!ValidateUtils.CheckNull(userId))
            {
                Member mm = GetModelByUserId(userId);
                if (mm == null) return null;
                sql += " and (CHARINDEX(','+CAST(" + mm.id + " AS VARCHAR)+',',m.rePath)>0 or m.id = " + mm.id + ")";
            }

            return this.GetList(sql);
        }

        public Member GetDirectTree(int uid, Member current)
        {
            string sql = "select m.id,m.userId,m.userName,m.reId,m.reName,m.uLevel,m.reCount," +
                         "m.userId+'（'+isnull(m.userName,'')+'）：'+isnull(d.name,'')+'：'+CAST(isnull(m.reCount,0) as varchar(10)) dtreeName  " +
                         "from Member m inner join DataDictionary d on m.uLevel = d.id where isAdmin = 0";

            //只能查询当前登录会员及下级节点的会员
            //userId非空则判断userId是否是当前登录会员或下级节点，是：则按userId查询，不是：返回空列表
            if (uid > 0)
            {
                Member mm = GetModelById(uid);
                if (mm == null) { sql += " and 1=2"; }
                else
                {
                    Member cr = GetModelById(current.id.Value);
                    if (cr.isAdmin.Value == 1 || mm.id.Value == cr.id.Value || mm.rePath.Contains("," + cr.id + ","))
                    {
                        sql += " and (m.id=" + uid + " or m.reId=" + uid + ")";
                    }
                    else
                    {
                        sql += " and 1=2";
                    }
                }
            }
            //userId为空则查询当前登录会员直推图
            else
            {
                sql += " and (m.id=" + uid + " or m.reId=" + uid + ")";// +" (CHARINDEX(','+CAST(" + current.id + " AS VARCHAR)+',',m.rePath)>0 or m.id = " + current.id + ")";
            }
            List<Member> list = this.GetList(sql);
            List<Member> reList = new List<Member>();
            Member result = null;
            for (int i = 0; i < list.Count; i++)
            {
                Member m = list[i];
                if (m.id.Value == uid)
                {
                    result = m;
                }
                else
                {
                    reList.Add(m);
                }
            }
            result.reList = reList;
            return result;
        }


        public List<Member> GetDirectTreeList(string userId, Member current)
        {
            string sql = "select m.id,m.userId,m.userName,m.reId,m.reName,m.uLevel," +
                         "m.userId+'（'+isnull(m.userName,'')+'）：'+isnull(d.name,'')+'：'+CAST(isnull(m.reCount,0) as varchar(10)) dtreeName  " +
                         "from Member m inner join DataDictionary d on m.uLevel = d.id where isAdmin = 0";

            //只能查询当前登录会员及下级节点的会员
            //userId非空则判断userId是否是当前登录会员或下级节点，是：则按userId查询，不是：返回空列表
            if (!ValidateUtils.CheckNull(userId))
            {
                Member mm = GetModelByUserId(userId);
                if (mm == null) { sql += " and 1=2"; }
                else
                {
                    Member cr = GetModelById(current.id.Value);
                    if (mm.id.Value == cr.id.Value || mm.rePath.Contains("," + cr.id + ","))
                    {
                        sql += " and (CHARINDEX(','+CAST(" + mm.id + " AS VARCHAR)+',',m.rePath)>0 or m.id = " + mm.id + ")";
                    }
                    else
                    {
                        sql += " and 1=2";
                    }

                }
            }
            //userId为空则查询当前登录会员直推图
            else
            {
                sql += " and (CHARINDEX(','+CAST(" + current.id + " AS VARCHAR)+',',m.rePath)>0 or m.id = " + current.id + ")";
            }
            return this.GetList(sql);
        }

        public int SaveAdmin(Member mb)
        {
            string err = null;

            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "管理员编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.password)) { err = "登录密码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.passOpen)) { err = "安全密码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.threepass)) { err = "交易密码不能为空"; }
            if (err != null)
            {
                throw new ValidateException(err);
            }

            //检查用户名是否存在
            Member m = this.GetModelByUserId(mb.userId);
            if (m != null) { throw new ValidateException("会员编码：" + m.userId + " 已经存在"); }

            //密码加密
            mb.password = DESEncrypt.EncryptDES(mb.password, ConstUtil.SALT);
            mb.passOpen = DESEncrypt.EncryptDES(mb.passOpen, ConstUtil.SALT);
            mb.threepass = DESEncrypt.EncryptDES(mb.threepass, ConstUtil.SALT);

            //其他默认信息
            mb.addTime = DateTime.Now;      //注册时间
            mb.loginFailTimes = 0;          //默认登录失败次数
            mb.isAdmin = 1; //后台会员
            mb.isPay = 1;
            mb.isLock = 0;

            //保存会员
            object o = this.SaveByIdentity(mb);
            int id = Convert.ToInt32(o);

            if (id <= 0)
            {
                throw new ValidateException("添加管理员失败，请联系管理员");
            }
            return id;
        }



        public int UpdatePassword(int memberId, string oldPass, string newPass, int flag)
        {
            if (ValidateUtils.CheckNull(newPass)) { throw new ValidateException("修改后的密码不能为空"); }
            Member m = GetModelById(memberId);
            oldPass = DESEncrypt.EncryptDES(oldPass, ConstUtil.SALT);
            newPass = DESEncrypt.EncryptDES(newPass, ConstUtil.SALT);
            Member update = new Member();
            update.id = memberId;
            if (flag == 1)
            {
                if (oldPass != m.password) { throw new ValidateException("旧登录密码错误"); }
                else
                {
                    update.password = newPass;
                }
            }
            else if (flag == 2)
            {
                if (oldPass != m.passOpen) { throw new ValidateException("旧安全密码错误"); }
                else
                {
                    update.passOpen = newPass;
                }
            }
            else if (flag == 3)
            {
                if (oldPass != m.threepass) { throw new ValidateException("旧交易密码错误"); }
                else
                {
                    update.threepass = newPass;
                }
            }
            else
            {
                throw new ValidateException("参数错误");
            }
            return this.Update(update);
        }

        public object[] GetRegMoneyAndNum(int ulevel)
        {
            //会员级别ID与参数注册单数对应关系
            //key:会员级别ID，value:参数paramCode
            Dictionary<int, string> ulelveDi = new Dictionary<int, string>();
            ulelveDi.Add(3, "upUlevel1");
            ulelveDi.Add(4, "upUlevel2");
            ulelveDi.Add(5, "upUlevel3");
            //查询参数
            List<string> list = new List<string>();
            list.Add("upUlevel1");
            list.Add("upUlevel2");
            list.Add("upUlevel3");
            list.Add("regPrice");
            Dictionary<string, ParameterSet> di = psBLL.GetToDictionary(list);
            //单价
            double price = Convert.ToDouble(di["regPrice"].paramValue);
            //会员级别所需单数
            int num = Convert.ToInt32(di[ulelveDi[ulevel]].paramValue);

            object[] objs = { price * num, num };
            return objs;
        }

        public string GetLeftUserId(int uid)
        {
            //调用存储过程 getLeftUid
            string pro = "getLeftUid";
            IDataParameter[] p = new IDataParameter[]{
                new SqlParameter("@uid",SqlDbType.Int,10),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = uid;
            p[1].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            return result;
        }

        public int UpdateForgetPass(Member m)
        {
            if (m == null) { throw new ValidateException("密码为空"); }
            if (ValidateUtils.CheckNull(m.password)) { throw new ValidateException("登录密码不能为空"); }
            if (ValidateUtils.CheckNull(m.passOpen)) { throw new ValidateException("安全密码不能为空"); }
            if (ValidateUtils.CheckNull(m.threepass)) { throw new ValidateException("交易密码不能为空"); }
            if (ValidateUtils.CheckIntZero(m.id)) { throw new ValidateException("修改会员不能为空"); }

            m.password = DESEncrypt.EncryptDES(m.password, ConstUtil.SALT);
            m.passOpen = DESEncrypt.EncryptDES(m.passOpen, ConstUtil.SALT);
            m.threepass = DESEncrypt.EncryptDES(m.threepass, ConstUtil.SALT);
            return dao.Update(m);
        }

        public int UpdateMember(Member model, Member current)
        {
            if (model == null || ValidateUtils.CheckIntZero(model.id)) { throw new ValidateException("修改会员不能为空"); }
            if (!ValidateUtils.CheckNull(model.password)) { model.password = DESEncrypt.EncryptDES(model.password, ConstUtil.SALT); }
            if (!ValidateUtils.CheckNull(model.passOpen)) { model.passOpen = DESEncrypt.EncryptDES(model.passOpen, ConstUtil.SALT); }
            if (!ValidateUtils.CheckNull(model.threepass)) { model.threepass = DESEncrypt.EncryptDES(model.threepass, ConstUtil.SALT); }
            if (model.bankCard.Length < 16 || model.bankCard.Length > 19) { throw new ValidateException("银行卡号位数必须为16-19位"); }
            Member old = this.GetModelById(model.id.Value);
            int c = dao.Update(model);
            if (old.bankCard != model.bankCard || old.bankUser != model.bankUser)
            {
                //发短信
                //发送给管理员短信
                noticeBLL.SendMessage(model.phone, "您正在修改收款银行信息，如非本人操作，请及时与管理员联系，" +
                    "以免给您造成不必要的损失！");

                //保存操作日志
                OperateLog log = new OperateLog();
                log.recordId = model.id;
                log.uid = current.id;
                log.userId = current.userId;
                log.ipAddress = current.ipAddress;
                log.mulx = "修改后的：银行卡号：" + model.bankCard + "，开户名：" + model.bankName;
                log.tableName = "Member";
                log.recordName = "修改银行收款信息";
                this.SaveOperateLog(log);
            }
            return c;

        }

        public Dictionary<string, object> GetRegisterDefaultMsg(int uid)
        {
            Dictionary<string, object> di = new Dictionary<string, object>();
            if (uid > 0)
            {
                Member m = GetModelById(uid);
                if (m != null)
                {
                    //默认接点人
                    di.Add("fatherName", m.userId);
                }
            }

            //注册协议
            News n = newsBLL.GetModelByTypeId(ConstUtil.NEWS_ZCXY);
            if (n != null)
            {
                di.Add("zcxy", n.content);
            }

            Dictionary<string, ParameterSet> ps = psBLL.GetDictionaryByCodes("loginPass", "passOpen", "threePass", "regPrice", "upUlevel1", "upUlevel2", "upUlevel3",
                "userIdPrefix");
            double price = Convert.ToDouble(ps["regPrice"].paramValue);             //每单注册金额
            di.Add("loginPass", ps["loginPass"].paramValue);                        //默认登录密码
            di.Add("passOpen", ps["passOpen"].paramValue);                          //默认安全密码
            di.Add("threePass", ps["threePass"].paramValue);                        //默认交易密码
            di.Add("pk", Convert.ToInt32(ps["upUlevel1"].paramValue) * price);      //普卡注册金额
            di.Add("yk", Convert.ToInt32(ps["upUlevel2"].paramValue) * price);      //银卡注册金额
            di.Add("jk", Convert.ToInt32(ps["upUlevel3"].paramValue) * price);      //金卡注册金额
            di.Add("userIdPrefix", ps["userIdPrefix"].paramValue);                  //用户名前缀
            return di;
        }

        public ResponseDtoData CheckUserId(string userId, int flag)
        {
            if (Common.SqlChecker.CheckKeyWord(userId) != "") { throw new ValidateException("含SQL注入字符" + Common.SqlChecker.CheckKeyWord(userId)); }
            ResponseDtoData response = new ResponseDtoData("fail");
            if (userId == null || userId == "")
            {
                response.msg = "编号为空";
            }
            else
            {
                Member m = GetModelByUserId(userId, 0);
                //检查用户编码是否已经存在
                if (flag == 1)
                {
                    if (m != null) { response.msg = "用户已经存在"; }
                    else { response.status = "success"; response.msg = "OK"; }
                }
                //检查接点人编码是否存在
                else if (flag == 2)
                {
                    if (m != null) { response.status = "success"; response.msg = m.userName; }
                    else { response.msg = "接点人不存在"; }
                }
                //检查推荐人编码是否存在
                else if (flag == 3)
                {
                    if (m != null) { response.status = "success"; response.msg = m.userName; }
                    else { response.msg = "推荐人不存在"; }
                }
                //检查报单中心编码是否存在
                else if (flag == 4)
                {
                    if (m != null && m.isAgent.Value == 2) { response.status = "success"; response.msg = m.userName; }
                    else { response.msg = "报单中心不存在"; }
                }
            }
            return response;
        }

        public string GetParentLinkString(int type, int uid)
        {
            Member m = this.GetModelById(uid);
            if (m == null || m.id == 1) { return ""; }
            string path = type == 1 ? m.pPath : m.rePath;
            List<Member> list = this.GetList("select userId,userName from Member where CHARINDEX(','+CAST(id AS VARCHAR)+',','" + path + "')>0 ");
            StringBuilder sb = new StringBuilder("");
            if (list != null && list.Count > 0)
            {
                foreach (Member mm in list)
                {
                    sb.Append(mm.userId).Append("（").Append(mm.userName).Append("）").Append("/ ");
                }
            }
            return sb.Length > 0 ? sb.ToString().Substring(0, sb.Length - 2) : "";
        }


        public int SaveChangeSecurityPass(Member m)
        {
            if (m.oldAnswer != null && Common.SqlChecker.CheckKeyWord(m.oldAnswer) != "") { throw new ValidateException("含SQL注入字符" + Common.SqlChecker.CheckKeyWord(m.oldAnswer)); }
            if (m.question != null && Common.SqlChecker.CheckKeyWord(m.question) != "") { throw new ValidateException("含SQL注入字符" + Common.SqlChecker.CheckKeyWord(m.question)); }
            if (string.IsNullOrEmpty(m.answer)) { throw new ValidateException("密保答案不能为空"); }


            if (m.fristAnswer != "Y")
            {
                if (string.IsNullOrEmpty(m.oldAnswer))
                {
                    throw new ValidateException("旧密保答案不能为空");
                }
                else
                {
                    Member oldMember = GetModelById(m.id.Value);
                    string oldAnswer = DESEncrypt.EncryptDES(m.oldAnswer, ConstUtil.SALT);
                    if (oldAnswer != oldMember.answer) { throw new ValidateException("旧密保答案错误"); }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(m.question)) { throw new ValidateException("密保问题不能为空"); }
            }
            if (!string.IsNullOrEmpty(m.question))
            {
                m.question = DESEncrypt.EncryptDES(m.question, ConstUtil.SALT);
            }
            m.answer = DESEncrypt.EncryptDES(m.answer, ConstUtil.SALT);
            m.oldAnswer = null;
            m.fristAnswer = null;
            return dao.Update(m);
        }

        public DataTable GetMemberExcelList(int type)
        {
            string sql = "";
            if (type == 0) //未开通
            {
                sql = "select userId,userName,addTime,fatherName,reName,shopName,uLevel,regMoney,phone from Member where 1=1 and id>3";
            }
            else if (type == 1)  //已开通
            {
                sql = "select userId,userName,addTime,fatherName,reName,shopName,uLevel,regMoney,phone,byopen,passTime from Member where 1=1 and id>3";
            }
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (type != null)
            {
                if (!ValidateUtils.CheckNull(type.ToString()))
                {
                    param.Add(new DbParameterItem("isPay", ConstUtil.EQ, type));
                }
            }
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }
    }
}
