﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ShoppingCartBLL : BaseBLL<ShoppingCart>, IShoppingCartBLL
    {

        private System.Type type = typeof(ShoppingCart);
        public IBaseDao dao { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new ShoppingCart GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            ShoppingCart mb = (ShoppingCart)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new ShoppingCart GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            ShoppingCart mb = (ShoppingCart)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<ShoppingCart> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<ShoppingCart> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<ShoppingCart>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ShoppingCart)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<ShoppingCart> GetList(string sql)
        {
            List<ShoppingCart> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<ShoppingCart>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ShoppingCart)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        /// <summary>
        /// 根据会员ID，产品id找记录
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        private ShoppingCart GetByProAndUser(int productId, int uid)
        {
            string sql = "select * from ShoppingCart where productId = " + productId + " and uid=" + uid;
            return this.GetOne(sql);
        }

        public List<ShoppingCart> GetListShoppingCart(ShoppingCart model)
        {
            if (model == null) { throw new ValidateException("查询条件不能为空"); }
            if (ValidateUtils.CheckIntZero(model.uid)) { throw new ValidateException("会员不能为空"); }
            string sql = "select s.*,p.productName,p.productCode,p.fxPrice price,p.imgUrl,p.fxPrice*s.num totalPrice from ShoppingCart s inner join "+
                         "Product p on s.productId = p.id where s.uid=" + model.uid;
            List<ShoppingCart> list = this.GetList(sql);
            return list;
        }

        public int SaveShoppingCart(ShoppingCart model)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (ValidateUtils.CheckIntZero(model.uid)) { throw new ValidateException("会员不能为空"); }
            if (ValidateUtils.CheckIntZero(model.productId)) { throw new ValidateException("商品不能为空"); }
            if (ValidateUtils.CheckIntZero(model.num)) { throw new ValidateException("数量不能为空"); }
            //检查会员是否已有该商品
            ShoppingCart cart = GetByProAndUser(model.productId.Value, model.uid.Value);
            if (cart != null)
            {
                //已经存在记录的情况下，直接累加数量
                cart.num = cart.num + model.num;
                return UpdateShoppingCart(cart);
            }
            else
            {
                model.addTime = DateTime.Now;
                model.updateTime = model.addTime;
                model.isCheck = 1;
                return dao.Save(model);
            }

        }


        public int GetCartCount(int uid)
        {
            string sql = "select count(1) from ShoppingCart where uid=" + uid;
            return dao.GetCount(sql);
        }

        public int UpdateShoppingCart(ShoppingCart model)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (ValidateUtils.CheckIntZero(model.id)) { throw new ValidateException("找不到购物车记录"); }
            if (ValidateUtils.CheckIntZero(model.num)) { throw new ValidateException("数量不能为空"); }
            model.updateTime = DateTime.Now;
            string sql = "update ShoppingCart set num=" + model.num + " where id=" + model.id;
            return dao.ExecuteBySql(sql);
        }


        public int Delete(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到购物车记录"); }
            string sql = "delete from ShoppingCart where id= "+id;
            return dao.ExecuteBySql(sql);
        }

        public int DeleteByUid(int uid)
        {
            string sql = "delete from ShoppingCart where uid= " + uid;
            return dao.ExecuteBySql(sql);
        }

        public int SaveChecked(int id, int isChecked)
        {
            isChecked = isChecked == 1 ? 1 : 2;
            string sql = "update ShoppingCart set isCheck=" + isChecked + " where id= " + id;
            return dao.ExecuteBySql(sql);
        }

        public int SaveCheckAll(int uid, int isChecked)
        {
            isChecked = isChecked == 1 ? 1 : 2;
            string sql = "update ShoppingCart set isCheck=" + isChecked + " where uid= " + uid;
            return dao.ExecuteBySql(sql);
        }
    }
}
