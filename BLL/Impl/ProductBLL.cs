﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ProductBLL : BaseBLL<Product>, IProductBLL
    {

        private System.Type type = typeof(Product);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new Product GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            Product mb = (Product)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new Product GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Product mb = (Product)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<Product> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<Product> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Product>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Product)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<Product> GetList(string sql)
        {
            List<Product> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Product>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Product)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }


        public PageResult<Product> GetMerchantListPage(Product model, string fields)
        {
            PageResult<Product> page = new PageResult<Product>();
            string sql = "select " + fields + ",c.name mname,row_number() over(order by m.id desc) rownumber from Product m left join Merchant c on m.uid = c.uid where 1=1 ";
            string countSql = "select count(1) from Product m left join Merchant c on m.uid = c.uid where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                //只查询商家商品
                param.Add(new DbParameterItem("m.uid", ConstUtil.NEQ, 1));
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isShelve))
                {
                    param.Add(new DbParameterItem("m.isShelve", ConstUtil.EQ, model.isShelve));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (!ValidateUtils.CheckNull(model.mname))
                {
                    param.Add(new DbParameterItem("c.name", ConstUtil.LIKE, model.mname));
                }
                if (!ValidateUtils.CheckNull(model.productCode))
                {
                    param.Add(new DbParameterItem("m.productCode", ConstUtil.LIKE, model.productCode));
                }
                if (!ValidateUtils.CheckNull(model.productName))
                {
                    param.Add(new DbParameterItem("m.productName", ConstUtil.LIKE, model.productName));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if (!ValidateUtils.CheckIntZero(model.productTypeId))
                {
                    sql += " and (m.productTypeId=" + model.productTypeId + " or m.productBigTypeId=" + model.productTypeId + ")";
                    countSql += " and (m.productTypeId=" + model.productTypeId + " or m.productBigTypeId=" + model.productTypeId + ")";
                }
                if (!ValidateUtils.CheckIntZero(model.productBigTypeId))
                {
                    param.Add(new DbParameterItem("m.productBigTypeId", ConstUtil.EQ, model.productBigTypeId));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Product> list = new List<Product>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Product)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public PageResult<Product> GetListPage(Product model, string fields)
        {
            PageResult<Product> page = new PageResult<Product>();
            string sql = "select " + fields + ",c.name mname,row_number() over(order by m.id desc) rownumber from Product m left join Merchant c on m.uid = c.uid where 1=1 ";
            string countSql = "select count(1) from Product m left join Merchant c on m.uid = c.uid where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (model.uid != null && model.uid != 0)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isShelve))
                {
                    param.Add(new DbParameterItem("m.isShelve", ConstUtil.EQ, model.isShelve));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (!ValidateUtils.CheckNull(model.mname))
                {
                    param.Add(new DbParameterItem("c.name", ConstUtil.LIKE, model.mname));
                }
                if (!ValidateUtils.CheckNull(model.productCode))
                {
                    param.Add(new DbParameterItem("m.productCode", ConstUtil.LIKE, model.productCode));
                }
                if (!ValidateUtils.CheckNull(model.productName))
                {
                    param.Add(new DbParameterItem("m.productName", ConstUtil.LIKE, model.productName));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if (!ValidateUtils.CheckIntZero(model.productTypeId))
                {
                    sql += " and (m.productTypeId=" + model.productTypeId + " or m.productBigTypeId=" + model.productTypeId + ")";
                    countSql += " and (m.productTypeId=" + model.productTypeId + " or m.productBigTypeId=" + model.productTypeId + ")";
                }
                if (!ValidateUtils.CheckIntZero(model.productBigTypeId))
                {
                    param.Add(new DbParameterItem("m.productBigTypeId", ConstUtil.EQ, model.productBigTypeId));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Product> list = new List<Product>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Product)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public PageResult<Product> GetListPageAll(Product model, string fields)
        {
            PageResult<Product> page = new PageResult<Product>();
            string sql = "select " + fields + ",c.name mname,row_number() over(order by m.id desc) rownumber from Product m left join Merchant c on m.uid = c.uid where 1=1 ";
            string countSql = "select count(1) from Product m left join Merchant c on m.uid = c.uid where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (model.uid != null && model.uid != 0)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isShelve))
                {
                    param.Add(new DbParameterItem("m.isShelve", ConstUtil.EQ, model.isShelve));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (!ValidateUtils.CheckNull(model.mname))
                {
                    param.Add(new DbParameterItem("c.name", ConstUtil.LIKE, model.mname));
                }
                if (!ValidateUtils.CheckNull(model.productCode))
                {
                    param.Add(new DbParameterItem("m.productCode", ConstUtil.LIKE, model.productCode));
                }
                if (!ValidateUtils.CheckNull(model.productName))
                {
                    param.Add(new DbParameterItem("m.productName", ConstUtil.LIKE, model.productName));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            model.rows = page.total;
            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = page.total;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Product> list = new List<Product>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Product)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public bool isExistsProductCode(string productCode, int id)
        {
            if (ValidateUtils.CheckNull(productCode)) { throw new ValidateException("商品编码不能为空"); }
            //检查商品编码是否存在
            string sql = null;
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("productCode", null, productCode)).Result();
            if (id > 0)
            {
                sql = "select count(1) from Product where productCode = @productCode and id<>" + id;
            }
            else
            {
                sql = "select count(1) from Product where productCode = @productCode";
            }
            int count = dao.GetCount(sql, param, false);
            return count > 0 ? true : false;
        }

        public Product GetModel(int id)
        {
            return this.GetOne("select * from Product where id = " + id);
        }

        public Product SaveProduct(Product mb, Member current)
        {

            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
            else if (ValidateUtils.CheckNull(mb.productCode)) { throw new ValidateException("商品编码不能为空"); }
            else if (ValidateUtils.CheckNull(mb.productName)) { throw new ValidateException("商品名称不能为空"); }
            else if (mb.price == null || mb.price < 0) { throw new ValidateException("现价不能为空或负数"); }
            else if (mb.fxPrice == null || mb.fxPrice < 0) { throw new ValidateException("原价不能为空或负数"); }
            else if (ValidateUtils.CheckNull(mb.cont)) { throw new ValidateException("商品描述不能为空"); }
            else if (ValidateUtils.CheckNull(mb.imgUrl)) { throw new ValidateException("请上传商品图片"); }


            //设置默认值
            if (current.isAdmin == 1)
            {
                mb.uid = 1;
                mb.userId = "system";
                mb.flag = 2;
                mb.isShelve = 2;
            }
            else
            {
                mb.uid = current.id;
                mb.userId = current.userId;
                mb.flag = 1;
                mb.isShelve = 1;
            }
            mb.productType = 0;
            mb.isPass = 2;
            mb.addTime = DateTime.Now;
            mb.passTime = mb.addTime;
            mb.auditUid = current.id;
            mb.auditUser = current.userId;
            mb.pv = 0;

            //保存商品
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            return mb;
        }

        public Product UpdateProduct(Product model, Member current)
        {
            //非空校验
            if (model == null) { throw new ValidateException("更新内容为空"); }
            else if (ValidateUtils.CheckIntZero(model.id)) { throw new ValidateException("找不到更新的商品"); }
            else if (ValidateUtils.CheckNull(model.productCode)) { throw new ValidateException("商品编码不能为空"); }
            else if (ValidateUtils.CheckNull(model.productName)) { throw new ValidateException("商品名称不能为空"); }
            else if (model.price == null || model.price < 0) { throw new ValidateException("现价不能为空或负数"); }
            else if (model.fxPrice == null || model.fxPrice < 0) { throw new ValidateException("原价不能为空或负数"); }
            else if (ValidateUtils.CheckNull(model.cont)) { throw new ValidateException("商品描述不能为空"); }
            else if (ValidateUtils.CheckNull(model.imgUrl)) { throw new ValidateException("请上传商品图片"); }
            //model.passTime = DateTime.Now;
            //model.auditUid = current.id;
            //model.auditUser = current.userId;
            //更新
            int c = dao.Update(model);
            return model;
        }

        public int SaveShelve(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到上架的商品"); }
            string sql = "update Product set isShelve = 2 where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int SaveCancelShelve(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到下架的商品"); }
            string sql = "update Product set isShelve = 1 where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int Delete(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到删除的商品"); }
            string sql = "select isShelve from Product where id = " + id;
            object o = dao.ExecuteScalar(sql);
            if (o == null) { throw new ValidateException("找不到删除的商品"); }
            int isShelve = Convert.ToInt32(o);
            if (isShelve == 2) { throw new ValidateException("改商品还未下架，请下架后再删除"); }
            sql = "delete from Product where id=" + id;
            DataTable dt_OrderItem = dao.GetList("select * from OrderItem where productId=" + id);
            if (dt_OrderItem.Rows.Count > 0) { throw new ValidateException("此商品有出售记录不能删除"); }
            return dao.ExecuteBySql(sql);
        }

        public int SaveAuditProduct(int id, Member current)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到删除的商品"); }
            string sql = "select flag from Product where id = " + id;
            object o = dao.ExecuteScalar(sql);
            if (o == null) { throw new ValidateException("找不到要审核的商品"); }
            int flag = Convert.ToInt32(o);
            if (flag != 1) { throw new ValidateException("商品不是待审核状态"); }
            sql = "update Product set flag=2,isShelve = 2,auditUid=" + current.id + ",auditUser='" + current.userId + "',passTime = getDate() where id=" + id;
            return dao.ExecuteBySql(sql);
        }
    }
}
