﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class LogMessageBLL : ILogMessageBLL
    {

        private System.Type type = typeof(LogMessage);
        public IBaseDao dao { get; set; }



        public PageResult<LogMessage> GetListPage(LogMessage model)
        {
            PageResult<LogMessage> page = new PageResult<LogMessage>();
            string sql = "select m.*,row_number() over(order by m.id desc) rownumber from LogMessage m where 1=1 ";
            string countSql = "select count(1) from LogMessage m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.classUrl))
                {
                    param.Add(new DbParameterItem("classUrl", ConstUtil.LIKE, model.classUrl));
                }
                if (!ValidateUtils.CheckNull(model.method))
                {
                    param.Add(new DbParameterItem("method", ConstUtil.LIKE, model.method));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<LogMessage> list = new List<LogMessage>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LogMessage)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Delete(int id)
        {
            return dao.Delte("LogMessage", id);
        }

        public void RemoveAll()
        {
            dao.ExecuteBySql("truncate table LogMessage");
        }

    }
}
