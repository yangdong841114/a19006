﻿using Common;
using DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL.Impl
{
    public class CurrencySumsBLL : BaseBLL<CurrencySums>, ICurrencySumsBLL
    {
        private System.Type type = typeof(CurrencySums);
        public IBaseDao dao { get; set; }
        public override IBaseDao GetDao()
        {
            return dao;
        }
        public PageResult<CurrencySums> GetListPage(CurrencySums model)
        {
            PageResult<CurrencySums> page = new PageResult<CurrencySums>();
            string sql = "select t.*,row_number() over(order by t.id desc) rownumber from CurrencySums t  where 1=1 ";
            string countSql = "select count(1) from CurrencySums t  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            ////查询条件
            //if (model != null)
            //{
            //    if (!ValidateUtils.CheckNull(model.addTime))
            //    {
            //        param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
            //    }
            //    if (!ValidateUtils.CheckNull(model.pocAddress))
            //    {
            //        param.Add(new DbParameterItem("t.pocAddress", ConstUtil.LIKE, model.pocAddress));
            //    }
            //}
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<CurrencySums> list = new List<CurrencySums>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((CurrencySums)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }
    }
}
