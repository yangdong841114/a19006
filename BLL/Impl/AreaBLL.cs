﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class AreaBLL : IAreaBLL
    {

        private System.Type type = typeof(Area);
        public IBaseDao dao { get; set; }

        public List<Area> GetList()
        {
            string sql = "select * from Area ";
            DataTable dt = dao.GetList(sql);
            List<Area> list = new List<Area>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Area)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public Dictionary<string, List<TreeModel>> GetTreeModelList()
        {
            string sql = "select * from Area ";
            DataTable dt = dao.GetList(sql);
            //List<TreeModel> list = new List<TreeModel>();
            Dictionary<string, List<TreeModel>> di1 = new Dictionary<string, List<TreeModel>>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataRow row = dt.Rows[i];
                    TreeModel model = new TreeModel();
                    model.id = Convert.ToInt32(row["id"]);
                    model.value = row["name"].ToString();
                    string parentId = Convert.ToInt32(row["parentId"]).ToString();

                    if (!di1.ContainsKey(parentId))
                    {
                        List<TreeModel> chlids = new List<TreeModel>();
                        chlids.Add(model);
                        di1.Add(parentId, chlids);
                    }
                    else
                    {
                        List<TreeModel> chlids = di1[parentId];
                        chlids.Add(model);
                        di1[parentId] = chlids;
                    }

                    //if(parentId == 0){
                    //    list.Add(model);
                    //}
                }
            }
            return di1;
            //for(int i=0;i<list.Count;i++){
            //    TreeModel mm = list[i];
            //    if (di1.ContainsKey(mm.id.Value))
            //    {
            //        mm.childs = di1[mm.id.Value];
            //        for (int j = 0; j < mm.childs.Count; j++)
            //        {
            //            if (di1.ContainsKey(mm.childs[j].id.Value))
            //            {
            //                mm.childs[j].childs = di1[mm.childs[j].id.Value];
            //            }
            //            else
            //            {
            //                mm.childs[j].childs = new List<TreeModel>();
            //            }
            //        }
            //    }
            //    else
            //    {
            //        mm.childs = new List<TreeModel>();
            //    }
            //}

        }

        public Area Save(Area model)
        {
            object o = dao.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);
            model.id = newId;
            return model;
        }

        public Area Update(Area model)
        {
            dao.Update(model);
            return model;
        }

        public string Delete(int id)
        {
            List<DbParameterItem> param = new List<DbParameterItem>();
            param.Add(new DbParameterItem("@id", ConstUtil.EQ, id));
            dao.ExecuteBySql("delete from area where parentId in (select id from area where parentId = @id)", param);
            dao.ExecuteBySql("delete from area where parentId in (select id from area where id = @id)", param);
            dao.ExecuteBySql("delete from area where id = @id", param);
            return "删除成功";
        }
    }
}
