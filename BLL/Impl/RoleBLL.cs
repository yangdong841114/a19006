﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class RoleBLL : IRoleBLL
    {

        private System.Type type = typeof(Role);
        public IBaseDao dao { get; set; }

        public int Update(Role model)
        {
            if (model == null) { throw new ValidateException("更新内容为空"); }
            else if (ValidateUtils.CheckIntZero(model.id)) { throw new ValidateException("角色ID为空"); }
            return dao.Update(model);
        }

        public int Save(Role model)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            else if (ValidateUtils.CheckIntZero(model.roleType)) { throw new ValidateException("角色类型为空"); }
            else if (ValidateUtils.CheckNull(model.roleName)) { throw new ValidateException("角色名称为空"); }
            object o = dao.SaveByIdentity(model);
            return Convert.ToInt32(o);
        }

        public string Delete(int id)
        {
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.LIKE_ED, id)).Result();

            //检查角色是否存在
            if (ValidateUtils.CheckIntZero(id)) { return "删除的角色不能为空"; }
            string sql = "select count(1) from role where id = @id";
            int exists = dao.GetCount(sql, param, false);
            if (exists > 0)
            {
                //检查角色时候被分配给帐号
                sql = "select COUNT(1) from RoleMember r where r.roleId = @id";
                int count = dao.GetCount(sql, param, false);
                if (count > 0)
                {
                    return "该角色已被分配给会员使用，不能删除";
                }
                else
                {
                    sql = "delete from Role where id = @id";
                    int c = dao.ExecuteBySql(sql, param);
                    return "success";
                }
            }
            else
            {
                return "删除失败，该角色不存在";
            }
        }

        public string DeleteRm(List<int> list)
        {
            if (list == null || list.Count == 0) { return "要删除的记录为空"; }
            StringBuilder sb = new StringBuilder();
            foreach (int rmid in list)
            {
                sb.Append(rmid).Append(",");
            }
            string ids = sb.ToString().Substring(0, sb.Length - 1);
            string sql = "delete from RoleMember where rmId in (" + ids + ")";
            dao.ExecuteBySql(sql);
            return "success";
        }

        public PageResult<Role> GetRoleMemberPage(Role model, Member current)
        {
            PageResult<Role> page = new PageResult<Role>();
            string sql = "select rm.rmId,rm.memberId,rm.roleId id,r.roleName,r.roleType,m.userId,m.userName,row_number() over(order by r.id desc,m.id desc) rownumber from "+
                         " RoleMember rm inner join Role r on rm.roleId = r.id inner join Member m on rm.memberId = m.id  ";
            string countSql = "select count(1) from RoleMember rm inner join Role r on rm.roleId = r.id inner join Member m on rm.memberId = m.id  ";
            //如果当前登录是超级管理员则可以编辑、查看超级管理员的角色，否则对其他用户不可见
            if (current.userId == ConstUtil.SUPER_ADMIN)
            {
                sql += " where 1=1 ";
                countSql += " where 1=1 ";
            }
            else
            {
                sql += " where r.id>1 ";
                countSql += " where r.id>1 ";
            }

            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.id))
                {
                    param.Add(new DbParameterItem("rm.roleId", ConstUtil.EQ, model.id));
                }
                if (!ValidateUtils.CheckNull(model.roleName))
                {
                    param.Add(new DbParameterItem("r.roleName", ConstUtil.LIKE, model.roleName));
                }
                if (!ValidateUtils.CheckIntZero(model.roleType))
                {
                    param.Add(new DbParameterItem("r.roleType", ConstUtil.EQ, model.roleType));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Role> list = new List<Role>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Role)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public PageResult<Role> GetListPage(Role model, Member current)
        {
            PageResult<Role> page = new PageResult<Role>();
            string sql = "select m.*,row_number() over(order by m.id desc) rownumber from Role m where 1=1 and id<>2";
            string countSql = "select count(1) from Role m where 1=1 and id<>2 ";
            //如果当前登录是超级管理员则可以编辑、查看超级管理员的角色，否则对其他用户不可见
            if (current.userId == ConstUtil.SUPER_ADMIN)
            {
                sql += " and 1=1 ";
                countSql += " and 1=1 ";
            }
            else
            {
                sql += " and id>1 ";
                countSql += " and id>1 ";
            }
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.roleName))
                {
                    param.Add(new DbParameterItem("roleName", ConstUtil.LIKE, model.roleName));
                }
                if (!ValidateUtils.CheckIntZero(model.roleType))
                {
                    param.Add(new DbParameterItem("m.roleType", ConstUtil.EQ, model.roleType));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Role> list = new List<Role>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Role)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public PageResult<Role> GetNotRoleMemberPage(Role model,Member current)
        {
            PageResult<Role> page = new PageResult<Role>();
            string sql = "select m.id,m.userId,m.userName,row_number() over(order by m.id desc) rownumber from Member m where m.isAdmin = " + model.isAdmin + " and " +
                         "not exists(select 1 from RoleMember r where m.id = r.memberId and r.roleId=" + model.id + ") ";
            string countSql = "select count(1) from Member m where m.isAdmin = " + model.isAdmin + " and not exists(select 1 from RoleMember r where m.id = r.memberId and r.roleId=" + model.id + ") ";
            List<DbParameterItem> param = new List<DbParameterItem>();

            //如果不是超级管理员不能查询超级管理员
            if (ConstUtil.SUPER_ADMIN != current.userId)
            {
                param.Add(new DbParameterItem("m.userId", ConstUtil.NEQ, ConstUtil.SUPER_ADMIN));
            }

            //查询条件
            if (model == null || ValidateUtils.CheckIntZero(model.id))
            {
                throw new ValidateException("查询条件为空");
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Role> list = new List<Role>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Role)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public int SaveRm(List<Role> list)
        {
            if (list == null || list.Count == 0) { throw new ValidateException("要保存的内容为空");}
            DataTable dt = new DataTable("RoleMember");
            DataColumn dc1 = new DataColumn("memberId", Type.GetType("System.Int32"));
            DataColumn dc2 = new DataColumn("roleId", Type.GetType("System.Int32"));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            foreach (Role r in list)
            {
                if (ValidateUtils.CheckIntZero(r.id) || ValidateUtils.CheckIntZero(r.memberId)) continue;
                DataRow row = dt.NewRow();
                row["memberId"] = r.memberId;
                row["roleId"] = r.id;
                dt.Rows.Add(row);
            }
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("memberId", DbType.Int32, null))
                .Add(new DbParameterItem("roleId", DbType.Int32, null))
                .Result();
            string sql = "insert into RoleMember(memberId,roleId) values(@memberId,@roleId)";
            DataSet set = new DataSet();
            set.Tables.Add(dt);
            return dao.InsertBatchByDataSet(set, "RoleMember", sql, param);
        }

        public int SaveRoleResorce(List<Role> list)
        {
            if (list == null || list.Count == 0) { throw new ValidateException("要保存的内容为空"); }
            //先删除已有菜单
            int roleId = list[0].id.Value;
            string sql = "delete from RoleResource where roleId = " + roleId;
            dao.ExecuteBySql(sql);

            DataTable dt = new DataTable("RoleResource");
            DataColumn dc1 = new DataColumn("resourceId", Type.GetType("System.String"));
            DataColumn dc2 = new DataColumn("roleId", Type.GetType("System.Int32"));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            foreach (Role r in list)
            {
                if (ValidateUtils.CheckIntZero(r.id) || ValidateUtils.CheckNull(r.resourceId)) continue;
                DataRow row = dt.NewRow();
                row["resourceId"] = r.resourceId;
                row["roleId"] = r.id;
                dt.Rows.Add(row);
            }
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("resourceId", DbType.String, null))
                .Add(new DbParameterItem("roleId", DbType.Int32, null))
                .Result();
            sql = "insert into RoleResource(resourceId,roleId) values(@resourceId,@roleId)";
            DataSet set = new DataSet();
            set.Tables.Add(dt);
            return dao.InsertBatchByDataSet(set, "RoleResource", sql, param);
        }
    }
}
