﻿using Common;
using DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL.Impl
{
    public class CurrencySumDetailsBLL : BaseBLL<CurrencySumDetails>, ICurrencySumDetailsBLL
    {
        private System.Type type = typeof(CurrencySumDetails);
        public IBaseDao dao { get; set; }
        public override IBaseDao GetDao()
        {
            return dao;
        }
        public PageResult<CurrencySumDetails> GetListPage(CurrencySumDetails model)
        {
            PageResult<CurrencySumDetails> page = new PageResult<CurrencySumDetails>();
            string sql = "select t.id,replace(t.userId,substring(t.userId,LEN(t.userId)/3,LEN(t.userId)/3),'********') as userId,t.sumPoc,t.addTime,t.isStatus,case when t.isStatus = 0 and t.sumPoc >= (select paramValue from ParameterSet where paramCode = 'maxPoc') then 1 else 0 end as isAdmin,logs,row_number() over(order by t.id desc) rownumber from CurrencySumDetails t where 1=1 ";
            string countSql = "select count(1) from CurrencySumDetails t  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            param.Add(new DbParameterItem("t.addTime", ConstUtil.EQ, model.addTime));
            ////查询条件
            if (model != null)
            {
                //    if (!ValidateUtils.CheckNull(model.addTime))
                //    {
                //        param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                //    }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<CurrencySumDetails> list = new List<CurrencySumDetails>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((CurrencySumDetails)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }
    }
}
