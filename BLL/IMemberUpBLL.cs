﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 会员晋升业务逻辑接口
    /// </summary>
    public interface IMemberUpBLL : IBaseBLL<MemberUp>
    {
        /// <summary>
        /// 保存会员晋升记录(修改会员级别)
        /// </summary>
        /// <param name="mb">实体对象</param>
        /// <param name="current">当前会员</param>
        /// <param name="isBackRound">0:后台提交，1:前台提交（需记录流水账）</param>
        /// <returns></returns>
        MemberUp SaveMemberUp(MemberUp mb, Member current, int isBackRound);

        /// <summary>
        /// 修改荣誉级别
        /// </summary>
        /// <param name="mb">实体对象</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        MemberUp SaveRlevelUp(MemberUp mb, Member current);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="fields">要查询的列</param>
        /// <returns></returns>
        PageResult<MemberUp> GetListPage(MemberUp model);

        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="list">记录列表</param>
        /// <param name="current">当前用户</param>
        /// <returns></returns>
        int UpdateAudit(List<MemberUp> list,Member current);
    }
}
