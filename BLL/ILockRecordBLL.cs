﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 锁仓业务逻辑接口
    /// </summary>
    public interface ILockRecordBLL : IBaseBLL<LockRecord>
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<LockRecord> GetListPage(LockRecord model);
        PageResult<LockRecord> GetListPage(LockRecord model, string key);
        void ExecSql(string sql);
        int Save(LockRecord model, Member current);
        void AutoCurrencyToPoc();
        void AutoCurrencyToPoc(string date, string key);
        void CallbackPoc(string orderId, string setDate, string hash, string key);
        void AutoCurrencyToPoc(string addTime, int userId, string key);
        void SdBonus();





    }
}
