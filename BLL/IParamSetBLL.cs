﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IParamSetBLL : IBaseBLL<ParameterSet>
    {
        /// <summary>
        /// 获取所有参数
        /// </summary>
        /// <returns>Dictionary<string, ParameterSet> key:</returns>
        Dictionary<string, ParameterSet> GetAllToDictionary();

        /// <summary>
        /// 批量更新参数值
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        int UpdateList(List<ParameterSet> list);

        /// <summary>
        /// /// 获取ParameterSet键值对列表
        /// key:paramCode  value:ParameterSet实体
        /// </summary>
        /// <param name="paramCodeList">paramCode列表,此参数应由后台程序定义，不接受用户前端输入，故不做SQL注入处理</param>
        /// <returns></returns>
        Dictionary<string, ParameterSet> GetToDictionary(List<string> paramCodeList);

        Dictionary<string, ParameterSet> GetDictionaryByCodes(params string[] paramCode);
    }
}
