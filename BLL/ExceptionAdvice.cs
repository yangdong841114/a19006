﻿using AopAlliance.Intercept;
using Common;
using DAO;
using Model;
using Spring.Aop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 异常通知在抛出非ValidateException异常时，进行持久化记录
    /// </summary>
    public class ExceptionAdvice : IThrowsAdvice 
    {
        public IBaseDao dao { get; set; }

        public void AfterThrowing(MethodInfo method, Object[] args, Object target, Exception ex)
        {
            if (ex.GetType() != typeof(ValidateException))
            {
                LogMessage log = new LogMessage();
                log.addTime = DateTime.Now;
                log.classUrl = target.ToString();
                log.method = method.Name; ;
                log.message = ex.Message;
                log.stackMsg = ex.StackTrace;
                dao.Save(log);
            }
        }

    }
}
