﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IResourceBLL : IBaseBLL<Resource>
    {
        /// <summary>
        /// 根据角色ID获取分配的菜单
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        List<Resource> GetRoleResource(int roleId);

        /// <summary>
        /// 根据用户ID查询资源列表
        /// </summary>
        /// <param name="uid">用户ID</param>
        /// <returns></returns>
        List<Resource> GetListByUser(int uid);

        /// <summary>
        /// 根据用户ID、上级ID查询资源列表,不包含上级节点
        /// </summary>
        /// <param name="uid">用户ID</param>
        /// <param name="parentId">上级ID</param>
        /// <returns></returns>
        List<Resource> GetListByUserAndParent(int uid,string parentId);

        /// <summary>
        /// 根据用户ID、上级ID查询资源列表，包含上级节点
        /// </summary>
        /// <param name="uid">用户ID</param>
        /// <param name="parentId">上级ID</param>
        /// <returns></returns>
        List<Resource> GetConstantParentListByUserAndParent(int uid, string parentId);

        /// <summary>
        /// 根据用户ID、上级ID查询下级第一次子节点列表
        /// </summary>
        /// <param name="uid">用户ID</param>
        /// <param name="parentId">上级ID</param>
        /// <returns></returns>
        List<Resource> GetListByPartentId(int uid, string parentId);

        /// <summary>
        /// 保存菜单资源
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Resource SaveResource(Resource model);

        /// <summary>
        /// 更新菜单资源
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Resource UpdateResource(Resource model);

        /// <summary>
        /// 根据ID删除节点及节点下级
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        string DelteResource(string id);
    }
}
