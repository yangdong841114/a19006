﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface ICurrencySumsBLL : IBaseBLL<CurrencySums>
    {
        PageResult<CurrencySums> GetListPage(CurrencySums model);
    }
}
