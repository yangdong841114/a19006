﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 邮件中心业务逻辑接口
    /// </summary>
    public interface IEmailBoxBLL 
    {

        /// <summary>
        /// 分页查询发件箱
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        PageResult<EmailBox> GetSendListPage(EmailBox model);

        /// <summary>
        /// 分页查询收件箱
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        PageResult<EmailBox> GetReceiveListPage(EmailBox model);

        /// <summary>
        /// 查询回复的邮件列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<EmailBox> GetReceiveList(EmailBox model);

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="model"></param>
        /// <returns>收件人ID</returns>
        int SaveSend(EmailBox model, Member current);

        /// <summary>
        /// 发送给所有会员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string SaveAllSend(EmailBox model);

        /// <summary>
        /// 回复邮件
        /// </summary>
        /// <param name="model"></param>
        /// <returns>收件人ID</returns>
        int SaveReceive(EmailBox model, Member current);

        /// <summary>
        /// 删除发件箱邮件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteSend(int id);

        /// <summary>
        /// 删除发件箱邮件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteReceive(int id);

        /// <summary>
        /// 更新阅读状态
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int UpdateRead(int id);
    }
}
